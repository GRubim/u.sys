const superTrim = str => {
	str = str.trim();
	while (str.indexOf('  ') !== -1) str = str.replace('  ', ' ');
	return str;
};
const getData = () => {
	const data = {};
	page.find('[name]').each((i, e) => {
		e = $(e);
		const name = e.attr('name');
		data[name] = e.val();
	});
	return data;
};
page.find('input[name="nome"]').focus();
page.find('[name="submit"]').bind('click', function(){
	const data = getData();
	const {senha, senha2} = data;
	const nome = superTrim(data.nome);
	const login = superTrim(data.login);
	if (!nome && !login) {
		errorMsg('Insira o nome de usuário e o login');
		return;
	}
	if (!nome) {
		errorMsg('Insira o nome de usuário');
		return;
	}
	if (!login) {
		errorMsg('Insira o login');
		return;
	}
	if (senha !== senha2) {
		errorMsg('As senhas não conferem');
		return;
	}
	ocupyWorkspace(new Promise(done => {
		$.post({url: '/usuario/add', data,
			success: id => {
				if (id) {
					sendMsg('Usuário cadastrado');
					killPage(page);
				} else {
					errorMsg('Falha ao realizar cadastro');
				}
				done();
			},
			error: err => {
				errorMsg('Erro interno');
				done();
			}
		})
	}));
});
page.find('[name="cancel"]').bind('click', function(){
	killPage(page);
});
loaded();
