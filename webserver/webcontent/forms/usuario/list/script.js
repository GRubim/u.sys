const itemToRow = item => {
	const row = $new('div').addClass('row').html(
		'<div class="cell"><div class="content"></div></div>' +
		'<div class="cell"><div class="content"></div></div>'
	);
	const contents = row.find('.content');
	contents.first().append(domTxt(item.nome));
	contents.last().append(domTxt(item.login));
	return row;
};
initPaginationTable(page.find('.pagination-table'), {
	url: '/usuario/list',
	cols: [
		{title: 'Nome', width: '1w'},
		{title: 'Login', width: '150px'}
	],
	itemToRow,
	success: loaded,
	error: () => {
		errorMsg('Erro interno');
		loaded();
	}
});
page.find('[target="sair"]').bind('click', () => {
	killPage(page);
});
