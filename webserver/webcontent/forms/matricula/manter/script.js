const ESTADO_MATRICULADO = 1;
const ESTADO_TRANCADO = 2;
const ESTADO_CANCELADO = 3;
const ESTADO_INTERESSADO = 4;
const ESTADO_INTERESSE_INATIVO = 5;

const {idMatricula, callback} = args;
const decodeEstado = code => {
	switch (code) {
		case 1: return 'Matriculado';
		case 2: return 'Trancado';
		case 3: return 'Cancelado';
		case 4: return 'Interessado';
		case 5: return 'Interesse inativo';
	}
};
const select = page.find('select');
const addEstado = code => select.append(`<option value="${ code }">${ decodeEstado(code) }</option>`);
const inputIdCurso = page.find('[name="idCurso"]');
const inputIdOferta = page.find('[name="idOfertaCurso"]');
const inputCurso = page.find('[name="curso"]');
const inputModalidade = page.find('[name="modalidade"]');
const agendarButton = page.find('[target="agendar"]');

let dataLoaded = null;
let buttonLoaded = null;
const asyncCallEnded = () => {
	if (dataLoaded === false || buttonLoaded === false) {
		killPage(page);
		loaded();
	}
	if (dataLoaded && buttonLoaded) {
		loaded();
	}
};

$.get({
	url: '/getMatriculaById',
	data: {id: idMatricula},
	success: matricula => {
		const {id, estado, idOfertaCurso, nomeAluno, idCurso, nomeCurso, modalidade, nomeSite, nomeUsuario} = matricula;
		addEstado(estado);
		select.val(estado);
		if (estado === 1) {
			addEstado(2);
			addEstado(3);
		} else {
			addEstado(1);
			if (estado === 2) {
				addEstado(3);
			} else if (estado === 4) {
				addEstado(5);
			} else if (estado === 5) {
				addEstado(4);
			}
		}
		select.val(estado);
		inputCurso.val(nomeCurso);
		inputIdCurso.val(idCurso);
		inputIdOferta.val(idOfertaCurso);
		inputModalidade.val(modalidade);
		page.find('[name="aluno"]').val(nomeAluno);
		page.find('[name="colaborador"]').val(nomeSite ? nomeSite + ' (Site parceiro)' : nomeUsuario + ' (Usuário)');
		dataLoaded = true;
		asyncCallEnded();
	},
	error: err => {
		dataLoaded = false;
		errorMsg('Erro interno');
		asyncCallEnded();
	}
});

const updateButton = () => new Promise((done, fail) => {
	$.get({
		url: '/agendamentoAva/podeAgendar',
		data: {idMatricula},
		success: res => {
			if (res) {
				agendarButton.removeAttr('disabled');
			} else {
				agendarButton.prop('disabled', true);
			}
			done();
		},
		error: err => {
			errorMsg('Erro interno');
			fail();
		}
	});
});

updateButton()
	.then(() => {
		buttonLoaded = true;
		asyncCallEnded();
	})
	.catch(() => {
		buttonLoaded = false;
		asyncCallEnded();
	});

page.find('[name="id"]').val(idMatricula);
page.find('.cancel input[type="button"]').bind('click', function(){
	killPage(page);
	if (callback) {
		callback(false);
	}
});
page.find('[name="alterarModalidade"]').bind('click', function(){
	openForm('ofertaCurso/selecionar', {
		idCurso: inputIdCurso.val(),
		callback: oferta => {
			inputIdOferta.val(oferta.id);
			inputCurso.val(oferta.nome);
			inputModalidade.val(oferta.modalidade);
		}
	});
});
page.find('[name="salvar"]').bind('click', function(){
	ocupyWorkspace(new Promise(done => {
		$.post({
			url: './updateMatricula',
			data: getData(page),
			success: updated => {
				if (updated) {
					if (callback) {
						killPage(page);
						done();
						callback();
					} else {
						sendMsg('Matrícula atualizada');
					}
					done();
					return;
				}
				errorMsg('Falha ao salvar matrícula');
				done();
			},
			error: err => {
				errorMsg('Erro interno');
				done();
			}
		});
	}));
});
agendarButton.bind('click', () => {
	if (agendarButton.is('[disabled]')) return;
	ocupyWorkspace(new Promise(done => {
		$.post({
			url: '/agendamentoAva/add',
			data: {idMatricula},
			success: id => {
				sendMsg('Agendamento AVA criado');
				agendarButton.prop("disabled", true);
				done();
			},
			error: err => {
				errorMsg('Erro interno');
				done();
			}
		});
	}));
});