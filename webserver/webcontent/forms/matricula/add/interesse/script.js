const {aluno, callback, usuario} = args;
const dataToStr = data => {
	const fillZ = (val, n) => {
		val = val.toString();
		while (val.length < n) {
			val = '0' + val;
		}
		return val;
	}
	return fillZ(data.getDate(), 2)
		+ '/' + fillZ(data.getMonth() + 1, 2)
		+ '/' + fillZ(data.getFullYear(), 4);
};
const parseData = str => {
	const array = str.trim().split('/');
	if (array.length !== 3) return false;
	let d = parseInt(array[0], 10);
	let m = parseInt(array[1], 10);
	let y = parseInt(array[2], 10);
	if (!d || !m || !y)  return false;
	if (d < 1 || d > 31) return false;
	if (m < 1 || m > 12) return false;
	if (y < 1) return false;
	let data = new Date(y, m - 1, d);
	if (data.getFullYear() !== y)     return false;
	if (data.getMonth()    !== m - 1) return false;
	if (data.getDate()     !== d)     return false;
	return data;
};
const getData = () => {
	const data = {};
	page.find('input[name]').each(function(){
		const item = $(this);
		data[item.attr('name')] = item.val() || null;
	});
	return data;
};
if (aluno) {
	page.find('[name="idAluno"]').val(aluno.id);
	page.find('[name="aluno"]').val(aluno.nome);
}
page.find('[name="alterarAluno"]').bind('click', function(){
	openForm('aluno/selecionar', {callback: aluno => {
		if (aluno) {
			page.find('[name="aluno"]').val(aluno.nome);
			page.find('[name="idAluno"]').val(aluno.id);
		}
	}});
});
page.find('[name="alterarCurso"]').bind('click', function(){
	openForm('ofertaCurso/selecionar', {callback: curso => {
		if (curso) {
			const {id, nome, modalidade} = curso;
			page.find('[name="curso"]').val(nome);
			page.find('[name="idOfertaCurso"]').val(id);
			page.find('[name="modalidade"]').val(modalidade);
		}
	}});
});
page.find('[name="alterarColaborador"]').bind('click', function(){
	openForm('colaborador/selecionar', {callback: res => {
		if (res) {
			const {idUsuario, idSiteParceiro, nome} = res;
			const str = nome + (idUsuario ? ' (Usuário)' : ' (Site parceiro)');
			page.find('[name="colaborador"]').val(str);
			page.find('[name="idUsuario"]').val(idUsuario);
			page.find('[name="idSiteParceiro"]').val(idSiteParceiro);
		}
	}});
});
page.find('.cancel input[type="button"]').bind('click', function(){
	killPage(page);
	if (callback) callback();
});
page.find('input[name="concluir"]').bind('click', function(){
	const data = getData();
	const {idAluno, idUsuario, idOfertaCurso, idSiteParceiro} = data;
	if (!idAluno) return errorMsg('Selecione um aluno');
	if (!idOfertaCurso) return errorMsg('Selecione um curso');
	if (!idUsuario && !idSiteParceiro) return errorMsg('Selecione um colaborador');
	ocupyWorkspace(new Promise(done => {
		$.post({
			url: '/addMatricula',
			data,
			success: res => {
				done();
				if (res) {
					sendMsg('Interesse registrado');
					if (callback) {
						killPage(page);
						callback(res);
					}
				} else {
					errorMsg('Falha ao registrar interesse');
				}
			},
			error: res => {
				errorMsg('Erro interno');
				done();
			}
		});
	}));
});
page.find('[name="data"]').val(dataToStr(new Date()));
page.find('[name="idUsuario"]').val(usuario.id);
page.find('[name="colaborador"]').val(usuario.nome + ' (Usuário)');
loaded();
