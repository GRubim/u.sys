const {aluno, callback, usuario} = args;
if (aluno) {
	page.find('[name="idAluno"]').val(aluno.id);
	page.find('[name="aluno"]').val(aluno.nome);
}
page.find('[name="alterarAluno"]').bind('click', function(){
	openForm('aluno/selecionar', {callback: aluno => {
		if (aluno) {
			page.find('[name="aluno"]').val(aluno.nome);
			page.find('[name="idAluno"]').val(aluno.id);
		}
	}});
});
page.find('[name="alterarCurso"]').bind('click', function(){
	openForm('ofertaCurso/selecionar', {callback: curso => {
		if (curso) {
			const {id, nome, modalidade} = curso;
			page.find('[name="curso"]').val(nome);
			page.find('[name="idOfertaCurso"]').val(id);
			page.find('[name="modalidade"]').val(modalidade);
		}
	}});
});
page.find('[name="alterarColaborador"]').bind('click', function(){
	openForm('colaborador/selecionar', {callback: res => {
		if (res) {
			const {idUsuario, idSiteParceiro, nome} = res;
			const str = nome + (idUsuario ? ' (Usuário)' : ' (Site parceiro)');
			page.find('[name="colaborador"]').val(str);
			page.find('[name="idUsuario"]').val(idUsuario);
			page.find('[name="idSiteParceiro"]').val(idSiteParceiro);
		}
	}});
});
page.find('.cancel input[type="button"]').bind('click', function(){
	killPage(page);
	if (callback) callback();
});
page.find('input[name="concluir"]').bind('click', function(){
	const data = getData(page);
	const {idAluno, idUsuario, idOfertaCurso, idSiteParceiro} = data;
	if (!idAluno) return errorMsg('Selecione um aluno');
	if (!idOfertaCurso) return errorMsg('Selecione um curso');
	if (!idUsuario && !idSiteParceiro) return errorMsg('Selecione um colaborador');
	ocupyWorkspace(new Promise(done => {
		$.post({
			url: '/addMatricula',
			data,
			success: res => {
				done();
				if (res) {
					sendMsg('Matrícula criada');
					if (callback) {
						killPage(page);
						callback(res);
					}
				} else {
					errorMsg('Falha ao registrar matrícula');
				}
			},
			error: res => {
				errorMsg('Erro interno');
				done();
			}
		});
	}));
});
page.find('[name="idUsuario"]').val(usuario.id);
page.find('[name="colaborador"]').val(usuario.nome + ' (Usuário)');
loaded();
