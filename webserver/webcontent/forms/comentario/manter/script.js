const {idAluno, usuario} = args;
const idUsuario = usuario.id;
const chatContent = page.find('.chat-content');
const empty = chatContent.find('.empty');
const inputTexto = page.find('[name="texto"]');
const timeToText = date => {
	const y = date.getFullYear();
	const M = date.getMonth() + 1;
	const d = date.getDate();
	const h = date.getHours();
	const m = date.getMinutes();
	return `- ${d<10?'0'+d:d}/${M<10?'0'+M:M}/${y} - ${h<10?'0'+h:h}:${m<10?'0'+m:m}`;
};
const setComentarios = array => {
	chatContent.html('');
	if (!array.length) chatContent.append(empty);
	array.forEach(comment => {
		const {id, usuario, texto, horario} = comment;
		const div = $new('div').addClass('comment');
		let editButton = '';
		if (comment.idUsuario == idUsuario) {
			editButton = '<div class="edit-button"><span class="fas fa-edit"></span></div>';
		}
		div[0].innerHTML =
			'<div class="head">' +
				editButton +
				'<span class="username"></span> <span class="time"></span>' +
			'</div>' +
			'<div class="text"></div>' +
			'<input type="hidden" name="id" value="' + id + '">';
		div.find('.username').html(domTxt(usuario));
		const text = div.find('.text');
		texto.split('\n').forEach(line => {
			text.append($new('div').html(domTxt(line)));
		});
		div.find('.time').html(domTxt(timeToText(new Date(horario))));
		chatContent.append(div);
	});
};
const updateComentarios = callback => {
	$.get({
		url: '/comentario/list',
		data: {idAluno},
		success: array => {
			setComentarios(array);
			callback();
		},
		error: err => {
			errorMsg('Erro ao atualizar comentários');
			callback();
		}
	});
};
page.find('[target="sair"]').bind('click', () => killPage(page));
page.find('[target="enviar"]').bind('click', () => {
	const texto = inputTexto.val().trim();
	if (!texto) return;
	inputTexto.val('');
	ocupyWorkspace(new Promise(done => {
		$.post({
			url: '/comentario/add',
			data: {idAluno, texto},
			success: res => {
				updateComentarios(done);
			},
			error: err => {
				errorMsg('Erro ao enviar comentário');
				done();
			}
		})
	}));
});
page.on('click', 'div.edit-button span', function(){
	const {id} = getData($(this).closest('.comment'));
	openForm('comentario/editar', {id, callback: updated => {
		if (updated) {
			ocupyWorkspace(new Promise(updateComentarios));
		}
	}});
});
inputTexto.focus();
updateComentarios(loaded);
