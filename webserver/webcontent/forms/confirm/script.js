const {title, text, callback} = args;
if (title) {
	page.find('.page-title').html(domTxt(title));
} else {
	page.find('.page-title').remove();
}
text.split('\n').forEach(line => {
	page.find('.plain-text').append($new('div').html(domTxt(line)));
});
page.find('[target="yes"]').bind('click', () => {
	killPage(page);
	callback(true);
});
page.find('[target="no"]').bind('click', () => {
	killPage(page);
	callback(false);
});
loaded();
