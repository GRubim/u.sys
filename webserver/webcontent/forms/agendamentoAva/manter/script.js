const {id, usuario, callback} = args;
const close = () => {
	killPage(page);
	if (callback) callback();
};
page.find('.cancel').bind('click', () => {
	close();
});
const fillForm = agendamento => {
	const data = Util.dateTimeBR(agendamento.data);
	const {nomeCurso, nomeAluno, modalidade} = agendamento;
	page.find('[name="data"]').val(data);
	page.find('[name="nomeAluno"]').val(nomeAluno);
	page.find('[name="nomeCurso"]').val(nomeCurso);
	page.find('[name="modalidade"]').val(modalidade);
};
$.get({
	url: '/agendamentoAva/get',
	data: { id },
	success: agendamento => {
		if (!agendamento) {
			errorMsg('Agendamento não encontrado');
			close();
			loaded();
			return;
		}
		fillForm(agendamento);
		loaded();
	},
	error: error => {
		errorMsg('Erro interno');
		close();
		loaded();
	}
})
page.find('[target="remove"]').bind('click', () => {
	ocupyWorkspace(new Promise(done => {
		$.post({
			url: '/agendamentoAva/remove',
			data: {id},
			success: removed => {
				sendMsg('Agendamento AVA excluído');
				close();
				done();
			},
			error: error => {
				errorMsg('Erro ao excluir');
				done();
			}
		})
	}));
});
page.find('[target="comments"]').bind('click', function() {
	openForm('comentarioAva/manter', { idAgendamento: id, usuario });
});