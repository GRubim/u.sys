const {usuario} = args;
const itemToRow = item => {
	let html = '';
	const attrs = [];
	const addAttr = attr => {
		attrs.push(attr);
		html += '<div class="cell"><div class="content"></div></div>';
	};
	addAttr(item.nomeAluno);
	addAttr(item.nomeCurso);
	html +=
		'<div class="cell open">' +
			'<div class="content">' +
				'<span class="fas fa-edit"></span>' +
			'</div>' +
		'</div>';
	const row = $new('div').addClass('row').html(html);
	row.find('.open .content').css({
		'text-align': 'right',
		'margin-right': '18px',
		'cursor': 'pointer'
	});
	const input = $new('input').attr({
		'type': 'hidden',
		'name': 'id'
	});
	input.val(item.id);
	row.append(input);
	const contents = row.find('.content');
	attrs.forEach((attr, i) => {
		const content = contents.eq(i);
		content.append(domTxt(attr));
		content.parent().attr('title', attr);
	});
	return row;
};
const {reload} = initPaginationTable(page.find('.pagination-table'), {
	url: '/agendamentoAva/list',
	cols: [
		{title: 'Aluno', width: '1w'},
		{title: 'Curso', width: '150px'},
		{title: '', width: '50px'},
	],
	itemToRow,
	success: loaded,
	error: () => {
		errorMsg('Erro interno');
		loaded();
	}
});
const inputIdAluno = page.find('[name="idAluno"]');
const inputNomeAluno = page.find('[name="nomeAluno"]');
const reloadList = () => {
	let idAluno = inputIdAluno.val() || null;
	ocupyWorkspace(new Promise(done => {
		reload({data: {idAluno}})
			.then(done)
			.catch(() => {
				errorMsg('Erro ao atualizar lista');
				done();
			});
	}));
}
page.find('[target="sair"]').bind('click', () => {
	killPage(page);
});
page.on('click', '.open', function() {
	const id = $(this).closest('.row').find('[name="id"]').val();
	openForm('agendamentoAva/manter', {id, callback: reloadList, usuario});
});
page.on('click', '[target="search"]', () => {
	openForm('aluno/selecionar', {
		callback: aluno => {
			if (!aluno) {
				inputIdAluno.val('');
				inputNomeAluno.val('');
			} else {
				inputIdAluno.val(aluno.id);
				inputNomeAluno.val(aluno.nome);
			}
			reloadList();
		}
	});
});