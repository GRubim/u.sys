const inputNome = page.find('[name="nome"]').focus();
const select = page.find('select');
page.find('[name="cancel"]').bind('click', ()=>{
	killPage(page);
});
page.find('[name="submit"]').bind('click', ()=>{
	let nome = inputNome.val().trim();
	while (nome.indexOf('  ') !== -1) {
		nome = nome.replace('  ', ' ');
	}
	if (!nome) {
		return errorMsg('Insira o nome do curso');
	}
	ocupyWorkspace(new Promise((done, fail) => {
		$.post({
			url: '/addCurso',
			data: {nome, modalidades: select.val()},
			success: id => {
				if (!id) {
					errorMsg('Falha ao registrar curso');
				} else {
					sendMsg('Curso cadastrado com sucesso');
					inputNome.val('').focus();
				}
				done();
			},
			error: err => {
				errorMsg('Erro interno');
				done();
			}
		});
	}));
});
loaded();