const {id} = args;
const inputNome = page.find('[name="nome"]');
page.find('[target="sair"]').bind('click', ()=>{
	killPage(page);
});
page.find('[target="salvar"]').bind('click', ()=>{
	ocupyWorkspace(new Promise(done => {
		$.post({
			url: '/curso/update',
			data: {id, nome: inputNome.val()},
			success: updated => {
				if (updated) {
					sendMsg('Alterações salvas');
				} else {
					errorMsg('Falha ao salvar alterações');
				}
				done();
			},
			error: error => {
				errorMsg('Erro interno');
				done();
			}
		})
	}));
});
$.get({
	url: '/curso/get',
	data: {id},
	success: curso => {
		if (!curso) {
			errorMsg('Curso inexistente');
			killPage(page);
		} else {
			inputNome.val(curso.nome);
		}
		loaded();
	},
	error: error => {
		errorMsg('Erro interno');
		killPage(page);
		loaded();
	}
});
