const itemToRow = item => {
	const row = $new('div').addClass('row').html(
		'<div class="cell">' +
			'<div class="content"></div>' +
		'</div>' +
		'<div class="cell open">' +
			'<div class="content">' +
				'<span class="fas fa-edit"></span>' +
			'</div>' +
		'</div>' +
		'<input type="hidden" name="id" value="' + item.id + '"/>'
	);
	const contents = row.find('.content');
	contents.first().append(domTxt(item.nome));
	contents.last().css({
		'text-align': 'right',
		'margin-right': '18px',
		'cursor': 'pointer'
	});
	return row;
};
initPaginationTable(page.find('.pagination-table'), {
	url: '/curso/list',
	cols: [
		{title: 'Curso', width: '1w'},
		{title: '', width: '50px'}
	],
	itemToRow,
	success: loaded,
	error: () => {
		errorMsg('Erro interno');
		loaded();
	}
});
page.find('[target="sair"]').bind('click', () => {
	killPage(page);
});
page.on('click', '.open', function () {
	const row = $(this).closest('.row');
	const id = row.find('[name="id"]').val();
	openForm('curso/manter', {id});
});
