const {callback, idCurso} = args;
const inputNome = page.find('[name="nome"]');
const inputCancel = page.find('[name="cancel"]');
let lastSearch = '';
let curso = null;
const {applyFilter, setList} = initSelectionTable(page.find('.selection-table'), {
	url: '/listOfertaCurso',
	data: {idCurso},
	cols: [
		{title: 'Curso', width: '1w'},
		{title: 'Modalidade', width: '120px'},
	],
	toHTMLRow: item => `
		<div class="selection-table-row" tabindex="0">
			<div class="selection-table-cell"><div>${ item.nome }</div></div>
			<div class="selection-table-cell"><div>${ item.modalidade }</div></div>
			<input type="hidden" name="id" value="${ item.id }"/>
			<input type="hidden" name="nome" value="${ item.nome }"/>
			<input type="hidden" name="modalidade" value="${ item.modalidade }"/>
		</div>
	`,
	success: () => {
		inputNome.focus();
		loaded();
	},
	error: _=> {
		errorMsg('Erro interno');
		loaded();
	}
});
const filter = items => {
	const res = [];
	const str = inputNome.val().trim().toLowerCase();
	while (str.indexOf('  ') !== -1) {
		str = str.replace('  ', ' ');
	}
	if (str === lastSearch) return false;
	if (!str) {
		lastSearch = '';
		return items.slice(0, 5);
	}
	for (let i=0; i<items.length && res.length < 5; ++i) {
		const item = items[i];
		if (noAccent(item.nome.toLowerCase()).indexOf(str) !== -1) {
			res.push(item);
		}
	}
	lastSearch = str;
	return res;
};
inputCancel.bind('click', function(){
	killPage(page);
});
inputNome.bind('keyup', function(){
	if (applyFilter) {
		applyFilter(filter);
	}
});
function selectRow(row) {
	const item = getData(row);
	if (!item.id) return;
	killPage(page);
	if (callback) callback(item);
}
page.on('click', '.selection-table-row', function() {
	selectRow($(this));
});
page.on('keydown', '.selection-table-row', function (e) {
	const key = e.key.toLowerCase();
	if (key === 'enter' || key === '\n' || key === ' ') {
		selectRow($(this));
	}
});