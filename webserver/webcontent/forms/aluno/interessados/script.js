const {usuario} = args;
const itemToRow = item => {
	const {idAluno, nomeAluno, nomeCurso, nomeSite, nomeUsuario} = item;
	const colaborador = nomeSite || nomeUsuario;
	const row = $new('div').addClass('row').html(
		'<div class="cell"><div class="content"></div></div>' +
		'<div class="cell"><div class="content"></div></div>' +
		'<div class="cell"><div class="content"></div></div>' +
		'<div class="cell open"><div class="content">' +
			'<span class="fas fa-list"></span>' +
		'</div></div>' +
		'<input type="hidden" name="idAluno" value="' + idAluno + '"/>'
	);
	const contents = row.find('.content');
	contents.eq(0).append(domTxt(nomeAluno)).parent().attr('title', nomeAluno);
	contents.eq(1).append(domTxt(nomeCurso)).parent().attr('title', nomeCurso);
	contents.eq(2).append(domTxt(colaborador)).parent().attr('title', colaborador);
	contents.eq(3).css({
		'text-align': 'right',
		'margin-right': '18px',
		'cursor': 'pointer'
	});
	return row;
};
const {reload} = initPaginationTable(page.find('.pagination-table'), {
	url: '/matricula/interessados',
	cols: [
		{title: 'Aluno', width: '1w'},
		{title: 'Curso de Interesse', width: '150px'},
		{title: 'Colaborador', width: '120px'},
		{title: 'Ver', width: '50px'}
	],
	itemToRow,
	success: loaded,
	error: () => {
		errorMsg('Erro interno');
		loaded();
	}
});
page.find('[target="sair"]').bind('click', () => {
	killPage(page);
});
page.on('click', '.open', function () {
	const row = $(this).closest('.row');
	const id = row.find('[name="idAluno"]').val();
	openForm('aluno/manter', {id, usuario});
});
const updateTable = () => {
	ocupyWorkspace(new Promise(done => {
		const data = Util.getData(page);
		reload({data})
			.then(done)
			.fail(() => {
				errorMsg('Erro ao atualizar listagem');
				done();
			});
	}));
};
page.find('[target="alterarColaborador"]').bind('click', () => {
	openForm('colaborador/selecionar', {
		callback: colaborador => {
			page.find('[name="colaborador"]').val(colaborador.nome);
			page.find('[name="idUsuario"]').val(colaborador.idUsuario);
			page.find('[name="idSiteParceiro"]').val(colaborador.idSiteParceiro);
			updateTable();
		}
	});
});