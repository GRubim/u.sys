const {id, usuario} = args;
const getSiteParceiro = () => new Promise(done => {
	openForm('colaborador/siteParceiro/selecionar', {callback: done});
});
const getOfertaCurso = () => new Promise(done => {
	openForm('ofertaCurso/selecionar', {callback: done});
});
let aluno = null;
let matriculas = null;
const decodeEstado = val => {
	switch (val) {
		case 1: return 'Matriculado';
		case 2: return 'Trancado';
		case 3: return 'Cancelado';
		case 4: return 'Interessado';
	}
};
let putMatriculas = () => {
	const divMatriculas = page.find('.matriculas div.content').html('');
	const divInteresses = page.find('.interesses div.content').html('');
	matriculas.forEach(({id, estado, modalidade, nomeCurso}) => {
		const div = $new('div').addClass('matricula');
		if (estado !== 1 && estado !== 4) {
			div.addClass('inativa');
		}
		const addCol = (name, val) => div.append($new('div').addClass(name).append(domTxt(val)));
		addCol('nome', nomeCurso);
		addCol('modalidade', modalidade);
		if (estado < 4) {
			addCol('estado', decodeEstado(estado));
		} else if (estado === 5) {
			addCol('estado', '(inativo)');
		}
		addCol('setup', '');
		div.append(`<input type="hidden" name="idMatricula" value="${ id }">`);
		div.find('.setup').html('<span class="fas fa-ellipsis-h setup" tabindex="0"></span>');
		if (estado >= 4) {
			divInteresses.append(div);
		} else {
			divMatriculas.append(div);
		}
	});
};
const atualizarMatriculas = () => {
	ocupyWorkspace(new Promise(done => $.get({
		url: '/listMatriculas',
		data: {idAluno: id},
		success: res => {
			matriculas = res;
			putMatriculas();
			done();
		},
		error: err => {
			errorMsg('Erro interno');
			done();
		}
	})));
};
syncAjax({
	get: [
		{url: '/aluno/get', data: {id}, success: res => aluno = res},
		{url: '/listMatriculas', data: {
			idAluno: id
		}, success: res => matriculas = res}
	],
	success: () => {
		page.find('[name="id"]').val(id);
		page.find('[name="nome"]').val(aluno.nome);
		page.find('[name="telefone"]').val(aluno.telefone);
		page.find('[name="email"]').val(aluno.email);
		putMatriculas();
		loaded();
	},
	error: () => {
		errorMsg('Erro interno');
		killPage(page);
		loaded();
	}
});
page.find('[name="submit"]').bind('click', () => {
	ocupyWorkspace(new Promise(done => {
		$.post({
			url: '/aluno/update',
			data: getData(page),
			success: res => {
				sendMsg('Contato salvo');
				done();
			},
			error: res => {
				errorMsg('Erro interno');
				done();
			}
		});
	}));
});
page.find('[name="cancel"]').bind('click', () => {
	killPage(page);
});
page.find('.add-matricula').bind('click', () => {
	openForm('matricula/add', {aluno, usuario, callback: id => {
		if (id) atualizarMatriculas();
	}});
});
page.find('.add-interesse').bind('click', () => {
	openForm('matricula/add/interesse', {aluno, usuario, callback: id => {
		if (id) atualizarMatriculas();
	}});
});
page.on('click', '.setup', function(e){
	e.stopPropagation();
	const {idMatricula} = getData($(this).closest('.matricula'));
	openForm('matricula/manter', {idMatricula, callback: atualizarMatriculas});
});
page.find('[name="historico"]').bind('click', () => {
	openForm('comentario/manter', {idAluno: id, usuario});
});
page.find('[target="add-lembrete"]').bind('click', () => {
	openForm('lembrete/add', {idAluno: id});
});
page.find('[target="delete"]').bind('click', () => {
	openForm('confirm', {
		title: 'Excluir aluno',
		text: 'Deseja mesmo excluir este aluno?\nIsso também irá remover todas suas matrículas',
		callback: res => {
			if (res) {
				ocupyWorkspace(new Promise(done => {
					$.post({
						url: '/aluno/remove',
						data: {id},
						success: ok => {
							if (ok) {
								sendMsg('Aluno removido');
								killPage(page);
							} else {
								errorMsg('Falha ao remover aluno');
							}
							done();
						},
						error: err => {
							errorMsg('Erro interno');
							done();
						}
					});
				}));
			}
		}
	});
});
