const {usuario} = args;
const inputNome = page.find('[name="nome"]');
const sendButton = page.find('[name="submit"]');
const cancelButton = page.find('[name="cancel"]');
const keep = page.find('[name="switch"]');
const getData = () => {
	const nome = inputNome.val().trim();
	const telefone = page.find('[name="telefone"]').val().trim();
	const email = page.find('[name="email"]').val().trim();
	return {nome, telefone, email};
};
inputNome.focus();
sendButton.bind('click', () => {
	const data = getData();
	const {nome, telefone, email} = data;
	if (!nome && !telefone) {
		errorMsg('Os campos nome e telefone são obrigatórios');
		return;
	}
	if (!nome) {
		errorMsg('Insira o nome do aluno');
		return;
	}
	if (!telefone) {
		errorMsg('Insira o telefone do aluno');
		return;
	}
	ocupyWorkspace(new Promise(done => {
		$.post({
			url: '/aluno/add',
			data,
			success: id => {
				if (id) {
					sendMsg('Cadastro concluído');
					if (keep.val() === 'false') {
						killPage(page);
						openForm('aluno/manter', {id, usuario});
					} else {
						'nome,telefone,email'.split(',').forEach(name => {
							page.find(`[name="${name}"]`).val('');
						});
						inputNome.focus();
					}
				} else {
					errorMsg('Falha ao realizar cadastro');
				}
				done();
			},
			error: res => {
				errorMsg('Erro na requisição');
				console.error(res);
				done();
			}
		})
	}));
});
cancelButton.bind('click', () => {
	const data = getData();
	const {nome, telefone, email} = data;
	if (!nome && !telefone && !email) {
		killPage(page);
		return;
	}
	if (confirm('Deseja mesmo cancelar este cadastro?')) {
		killPage(page);
	}
});
loaded();
