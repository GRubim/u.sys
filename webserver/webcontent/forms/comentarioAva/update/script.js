const {id, callback} = args;
const inputTexto = page.find('[name="texto"]');
page.find('[target="cancelar"]').bind('click', () => {
	killPage(page);
	if (callback) callback(false);
});
page.find('[target="salvar"]').bind('click', () => {
	let texto = inputTexto.val().trim();
	ocupyWorkspace(new Promise(done => {
		$.post({
			url: '/comentarioAva/update',
			data: {id, texto},
			success: updated => {
				if (!updated) {
					errorMsg('Falha ao atualizar comentário');
					done();
				} else {
					sendMsg('Comentário atualizado');
					done();
					killPage(page);
					if (callback) callback(true);
				}
			},
			error: err => {
				errorMsg('Erro interno');
				done();
			}
		});
	}));
});
$.get({
	url: '/comentarioAva/get',
	data: {id},
	success: comentario => {
		if (!comentario) {
			errorMsg('Comentário não encontrado');
			loaded();
			killPage(page);
			if (callback) callback(false);
		} else {
			inputTexto.val(comentario.texto);
			loaded();
		}
	},
	error: err => {
		errorMsg('Erro interno');
		loaded();
		killPage(page);
		if (callback) callback(false);
	}
});