const {id} = args;
page.find('[name="id"]').val(id);
page.find('[target="fechar"]').bind('click', () => {
	killPage(page);
});
page.find('[target="adiar"]').bind('click', () => {
	openForm('lembrete/manter', {id});
});
page.find('[target="concluir"]').bind('click', () => {
	ocupyWorkspace(new Promise(done => {
		$.ajax({
			url: '/lembrete/remove',
			data: {id},
			method: 'POST',
			success: res => {
				if (res) {
					killPage(page);
					sendMsg('Lembrete concluído');
					Lembretes.count();
				} else {
					errorMsg('Falha ao concluir lembrete');
				}
				done();
			},
			error: err => {
				errorMsg('Erro interno');
				console.error(err);
				done();
			}
		});
	}));
});
$.ajax({
	url: '/lembrete/get',
	data: {id},
	method: 'GET',
	success: lembrete => {
		if (lembrete) {
			const {texto, idAluno, nomeAluno} = lembrete;
			page.find('[target="content"]').html(domTxt(texto));
			page.find('.aluno-link').html(domTxt(nomeAluno)).attr('id-aluno', idAluno);
		} else {
			errorMsg('Lembrete inexistente');
			killPage(page);
		}
		loaded();
	},
	error: err => {
		errorMsg('Erro interno');
		killPage(page);
		loaded();
	}
});
