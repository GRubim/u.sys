const itemToRow = item => {
	const row = $new('div').addClass('row').html(
		'<div class="cell"><div class="content"></div></div>' +
		'<div class="cell"><div class="content"></div></div>' +
		'<div class="cell open">' +
			'<div class="content">' +
				'<span class="fas fa-edit"></span>' +
			'</div>' +
		'</div>' +
		'<input type="hidden" name="id" value="' + item.id + '"/>'
	);
	const contents = row.find('.content');
	contents.eq(0).append(domTxt(item.texto));
	let time = new Date(item.horario);
	const y = time.getFullYear();
	const M = time.getMonth() + 1;
	const d = time.getDate();
	const h = time.getHours();
	const m = time.getMinutes();
	time = `${d>9?d:'0'+d}/${M>9?M:'0'+M}/${y} ${h>9?h:'0'+h}:${m>9?m:'0'+m}`;
	contents.eq(1).append(domTxt(time));
	contents.eq(2).css({
		'text-align': 'right',
		'margin-right': '18px',
		'cursor': 'pointer'
	});
	return row;
};
initPaginationTable(page.find('.pagination-table'), {
	url: '/lembrete/list',
	cols: [
		{title: 'Texto', width: '1w'},
		{title: 'Hora', width: '120px'},
		{title: '', width: '50px'}
	],
	itemToRow,
	success: loaded,
	error: () => {
		errorMsg('Erro interno');
		loaded();
	}
});
page.find('[target="sair"]').bind('click', () => {
	killPage(page);
});
page.on('click', '.open', function () {
	const row = $(this).closest('.row');
	const id = row.find('[name="id"]').val();
	openForm('lembrete/manter', {id});
});
