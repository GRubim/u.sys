const {id} = args;
page.find('[target="cancel"]').bind('click', ()=>{
	killPage(page);
});
page.find('[target="submit"]').bind('click', ()=>{
	const info = getData(page);
	const {texto} = info;
	let data = parseDate(info.data);
	let horario = parseTime(info.horario);
	if (!data) {
		errorMsg('Informe uma data válida');
		return;
	}
	if (!horario) {
		errorMsg('Informe um horário válido');
		return;
	}
	if (!texto) {
		errorMsg('Informe o texto do lembrete');
		return;
	}
	let time = new Date(`${data} ${horario}`);
	let now = new Date();
	if (time < now) {
		errorMsg('Informe uma data e horário futuros');
		return;
	}
	horario = `${data} ${horario}`;
	ocupyWorkspace(new Promise(done => {
		$.ajax({
			data: {id, texto, horario},
			method: 'POST',
			url: '/lembrete/update',
			success: updated => {
				if (updated) {
					sendMsg('Lembrete atualizado com sucesso');
					Lembretes.count();
				} else {
					errorMsg('Falha ao atualizar lembrete');
				}
				done();
			},
			error: err => {
				errorMsg('Falha ao atualizar lembrete');
				done();
			}
		});
	}));
});
$.ajax({
	method: 'GET',
	url: '/lembrete/get',
	data: {id},
	success: obj => {
		if (!obj) {
			errorMsg('Lembrete inexistente');
			killPage(page);
		} else {
			page.find('[name="nomeAluno"]').val(obj.nomeAluno);
			page.find('[name="nomeUsuario"]').val(obj.nomeUsuario);
			page.find('[name="texto"]').val(obj.texto);
			const time = new Date(obj.horario);
			const y = time.getFullYear();
			const M = time.getMonth() + 1;
			const d = time.getDate();
			const h = time.getHours();
			const m = time.getMinutes();
			const data = `${d>9?d:'0'+d}/${M>9?M:'0'+M}/${y}`;
			const hora = `${h>9?h:'0'+h}:${m>9?m:'0'+m}`
			page.find('[name="data"]').val(data);
			page.find('[name="horario"]').val(hora);
		}
		loaded();
	},
	error: err => {
		errorMsg('Erro interno');
		killPage(page);
		loaded();
	}
});
page.find('[target="concluir"]').bind('click', ()=>{
	ocupyWorkspace(new Promise(done => {
		$.ajax({
			url: '/lembrete/remove',
			method: 'POST',
			data: {id},
			success: removed => {
				if (removed) {
					sendMsg('Lembrete concluído');
					killPage(page);
					Lembretes.count();
				} else {
					errorMsg('Falha ao excluir lembrete');
				}
				done();
			},
			error: err => {
				errorMsg('Falha ao excluir lembrete');
				done();
			}
		});
	}));
});