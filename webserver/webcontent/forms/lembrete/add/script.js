const {idAluno, nomeAluno} = args;
page.find('[target="cancel"]').bind('click', ()=>{
	killPage(page);
});
page.find('[target="pick-user"]').bind('click', () => {
	openForm('usuario/selecionar', {callback: res => {
		page.find('[name="idUsuarioDst"]').val(res.id);
		page.find('[name="nomeUsuarioDst"]').val(res.nome);
	}});
});
page.find('[target="submit"]').bind('click', ()=>{
	const info = getData(page);
	const {texto} = info;
	let data = parseDate(info.data);
	let horario = parseTime(info.horario);
	if (!data) {
		errorMsg('Informe uma data válida');
		return;
	}
	if (!horario) {
		errorMsg('Informe um horário válido');
		return;
	}
	if (!texto) {
		errorMsg('Informe o texto do lembrete');
		return;
	}
	let time = new Date(`${data} ${horario}`);
	let now = new Date();
	if (time < now) {
		errorMsg('Informe uma data e horário futuros');
		return;
	}
	info.horario = `${data} ${horario}`;
	ocupyWorkspace(new Promise(done => {
		$.ajax({
			data: info,
			method: 'POST',
			url: '/lembrete/add',
			success: id => {
				if (id) {
					sendMsg('Lembrete adicionado com sucesso');
					killPage(page);
				} else {
					errorMsg('Falha ao adicionar lembrete');
				}
				done();
			},
			error: err => {
				errorMsg('Falha ao adicionar lembrete');
				done();
			}
		});
	}));
});
page.find('[name="idAluno"]').val(idAluno);
page.find('[type="text"]').first().focus();
(() => {
	const now = new Date();
	now.setDate(now.getDate() + 1);
	const y = now.getFullYear();
	const M = now.getMonth() + 1;
	const d = now.getDate();
	const h = now.getHours();
	const m = now.getMinutes();
	const data = `${d>9?d:'0'+d}/${M>9?M:'0'+M}/${y}`;
	const hora = `${h>9?h:'0'+h}:${m>9?m:'0'+m}`
	page.find('[name="data"]').val(data);
	page.find('[name="horario"]').val(hora);
})();
$.ajax({
	url: '/aluno/get',
	data: {id: idAluno},
	method: 'GET',
	success: aluno => {
		page.find('[name="nomeAluno"]').val(aluno.nome);
		loaded();
	},
	error: err => {
		errorMsg('Erro interno').
		killPage(page);
		loaded();
	}
});
