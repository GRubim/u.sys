const inputNome = page.find('[name="nome"]').focus();
page.find('[name="cancel"]').bind('click', ()=>{
	killPage(page);
});
page.find('[name="submit"]').bind('click', ()=>{
	let nome = inputNome.val().trim();
	while (nome.indexOf('  ') !== -1) {
		nome = nome.replace('  ', ' ');
	}
	if (!nome) return errorMsg('Insira o nome do site parceiro');
	ocupyWorkspace(new Promise((done, fail) => {
		$.post({
			url: '/addSiteParceiro',
			data: {nome},
			success: id => {
				if (!id) {
					errorMsg('Falha ao registrar site parceiro');
				} else {
					sendMsg('Curso cadastrado com sucesso');
					inputNome.val('');
				}
				done();
			},
			error: err => {
				errorMsg('Erro interno');
				done();
			}
		});
	}));
});
loaded();
