const itemToRow = item => {
	const row = $new('div').addClass('row');
	row.html(
		'<div class="cell"><div class="content"></div></div>' +
		'<div class="cell"><div class="content"></div></div>'
	);
	const contents = row.find('.content');
	contents.eq(0).append(domTxt(item.nomeAluno));
	contents.eq(1).append(domTxt(item.nomeCurso));
	return row;
};
const cols = [
	{title: 'Aluno', width: '1w'},
	{title: 'Curso', width: '250px'},
];
const search = ({data, success, error}) => {
	initPaginationTable(page.find('.pagination-table'), {
		url: '/matricula/listPorColaborador',
		data, cols, success, error, itemToRow,
	});
};
initPaginationTable(page.find('.pagination-table'), {cols});
const inputNome = page.find('[name="nome"]');
const inputIdUsuario = page.find('[name="idUsuario"]');
const inputIdSiteParceiro = page.find('[name="idSiteParceiro"]');
page.find('[target="alterarColaborador"]').bind('click', function(){
	openForm('colaborador/selecionar', {
		callback: item => {
			if (item) {
				const {nome, idUsuario, idSiteParceiro} = item;
				inputNome.val(nome + (idUsuario ? ' (Usuário)' : ' (Site parceiro)'));
				inputIdUsuario.val(idUsuario);
				inputIdSiteParceiro.val(idSiteParceiro);
			}
		}
	});
});
page.find('[target="sair"]').bind('click', ()=>{
	killPage(page);
});
page.find('[target="buscar"]').bind('click', ()=>{
	const data = getData(page);
	const {idUsuario, idSiteParceiro} = data;
	if (!idUsuario && !idSiteParceiro) {
		return errorMsg('Selecione um colaborador');
	}
	const ini = parseDate(data.ini);
	if (!ini) {
		return errorMsg('Data de início inválida');
	}
	const fim = parseDate(data.fim);
	if (!fim) {
		return errorMsg('Data final inválida');
	}
	ocupyWorkspace(new Promise(done => {
		search({
			data: {idUsuario, idSiteParceiro, ini, fim},
			success: done,
			error: () => {
				errorMsg('Erro interno');
				done();
			}
		})
	}));
});
const initDates = () => {
	const n = new Date();
	const a = new Date(n.getFullYear(), n.getMonth() - 1, 1);
	const b = new Date(n.getFullYear(), n.getMonth(), 0);
	const toStr = date => {
		let y = date.getFullYear().toString();
		let m = date.getMonth() + 1;
		let d = date.getDate();
		return `${d<10?'0'+d:d}/${m<10?'0'+m:m}/${'0'.repeat(4-y.length)+y}`
	};
	page.find('[name="ini"]').val(toStr(a));
	page.find('[name="fim"]').val(toStr(b));
};
initDates();
loaded();
