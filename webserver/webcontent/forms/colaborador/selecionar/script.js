const {callback} = args;
const inputNome = page.find('[name="nome"]');
const inputSite = page.find('[name="site"]');
inputNome.focus();
let lastSearchName = '';
let lastSearchSite = '';
const {applyFilter, setList} = initSelectionTable(page.find('.selection-table'), {
	url: '/colaborador/list',
	cols: [{title: 'Nome', width: '1w'}],
	toHTMLRow: item => `
		<div class="selection-table-row" tabindex="0">
			<div class="selection-table-cell"><div>${ item.nome }</div></div>
			<input type="hidden" name="idUsuario" value="${ item.idUsuario || '' }"/>
			<input type="hidden" name="idSiteParceiro" value="${ item.idSiteParceiro || '' }"/>
			<input type="hidden" name="nome" value="${ item.nome }"/>
		</div>
	`,
	success: () => {
		inputNome.focus();
		applyFilter(filter);
		loaded();
	},
	error: _=> {
		errorMsg('Erro interno');
		loaded();
	}
});
const filter = array => {
	const res = [];
	const searchName = inputNome.val().trim().toLowerCase();
	const searchSite = inputSite.val();
	if (searchName === lastSearchName && searchSite === lastSearchSite) {
		return false;
	}
	const flagSite = searchSite === 'true';
	for (let i=0; i !== array.length && res.length !== 5; ++i) {
		const item = array[i];
		if ((flagSite === true) !== (item.idUsuario === undefined)) {
			continue;
		}
		if (noAccent(item.nome.toLowerCase()).indexOf(searchName) !== -1) {
			res.push(item);
		}
	}
	lastSearchName = searchName;
	lastSearchSite = searchSite;
	return res;
}
page.find('[name="cancelar"]').bind('click', function(){
	killPage(page);
	if (callback) {
		callback(false);
	}
});
inputSite.bind('change', function(){
	applyFilter(filter);
});
inputNome.bind('keyup', function(){
	applyFilter(filter);
});
function selectRow(row) {
	const item = getData(row);
	if (!item.idSiteParceiro && !item.idUsuario) return;
	killPage(page);
	if (callback) callback(item);
}
page.on('click', '.selection-table-row', function(){
	selectRow($(this));
});
