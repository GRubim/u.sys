const {callback} = args;
const button = page.find('[type="button"]');
const inputLogin = page.find('[name="login"]');
const inputSenha = page.find('[name="senha"]');
inputLogin.focus();
const submit = () => {
	const login = inputLogin.val().trim();
	const senha = inputSenha.val();
	if (!login && !senha) return errorMsg('Insira o login e a senha');
	if (!login) return errorMsg('Insira o login');
	if (!senha) return errorMsg('Insira a senha');
	const data = {login, senha};
	ocupyWorkspace(new Promise(done => {
		$.post({
			url: '/login',
			data,
			success: res => {
				if (!res.found) {
					errorMsg('Este login não existe');
				} else if (!res.authenticated) {
					errorMsg('Senha incorreta');
				} else {
					killPage(page);
					if (callback) callback(res.usuario);
				}
				done();
			},
			error: res => {
				errorMsg('Erro interno');
				killPage(page);
				done();
			}
		})
	}));
};
button.bind('click', submit);
inputSenha.bind('keydown', e => {
	const key = e.key.toLowerCase();
	if (key === '\n' || key === 'enter') {
		submit();
	}
});
loaded();
