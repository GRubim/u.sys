const select = page.find('select');
const inputIdAluno = page.find('[name="idAluno"]');
page.find('[name="cancel"]').bind('click', ()=>{
	killPage(page);
});
const loadCursos = () => {
	const idAluno = inputIdAluno.val();
	if (!idAluno) return;
	ocupyWorkspace(new Promise(done => {
		$.get({
			url: '/curso/listByIdAluno',
			data: { idAluno },
			success: array => {
				select.html('');
				array.forEach(curso => {
					const {
						nome,
						modalidade,
						idMatricula
					} = curso;
					const option = $new('option');
					option.attr('value', idMatricula);
					option.html(domTxt(`${ nome } - ${ modalidade }`));
					select.append(option);
				});
				done();
			},
			error: err => {
				errorMsg('Erro ao carregar cursos');
				done();
			}
		});
	}));
};
page.find('[target="alterar_aluno"]').bind('click', ()=>{
	openForm('aluno/selecionar', {
		callback: aluno => {
			if (!aluno) return;
			const {id, nome} = aluno;
			page.find('[name="nomeAluno"]').val(nome);
			inputIdAluno.val(id);
			loadCursos();
		}
	});
});
const send = obj => {
	ocupyWorkspace(new Promise(done => {
		$.ajax({
			type: 'POST',
			url: '/prova/add',
			data: obj,
			success: id => {
				sendMsg('Agendamento reazliado com sucesso');
				killPage(page);
				done();
			},
			error: err => {
				errorMsg('Falha ao agendar prova');
				done();
			}
		});
	}));
};
page.find('[name="submit"]').bind('click', ()=>{
	let {
		data,
		disciplina,
		horario,
		idAluno,
		semestre,
		idMatricula
	} = getData(page);
	if (!idAluno) {
		return errorMsg('Selecione o aluno');
	}
	if (!idMatricula) {
		return errorMsg('Selecione o curso');
	}
	semestre = semestre.trim();
	if (!semestre) {
		return errorMsg('Informe o semestre');
	}
	disciplina = disciplina.trim();
	if (!disciplina) {
		return errorMsg('Informe a disciplina');
	}
	data = data.trim();
	if (!data) {
		return errorMsg('Informe a data');
	}
	data = parseDate(data);
	if (!data) {
		return errorMsg('Data inválida');
	}
	horario = horario.trim();
	if (horario) {
		horario = parseTime(horario);
		if (!horario) {
			return errorMsg('Hora inválida');
		}
	}
	send({
		data,
		disciplina,
		horario,
		idAluno,
		semestre,
		idMatricula
	});
});
loaded();