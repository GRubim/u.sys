const dateToStr = date => {
	const y = date.getFullYear();
	const M = date.getMonth() + 1;
	const d = date.getDate();
	return `${d>9?d:'0'+d}/${M>9?M:'0'+M}/${y}`;
};
const timeToStr = time => {
	const h = time.getHours();
	const m = time.getMinutes();
	return `${h>9?h:'0'+h}:${m>9?m:'0'+m}`;
};
const itemToRow = item => {
	let html = '';
	const attrs = [];
	const addAttr = attr => {
		attrs.push(attr);
		html += '<div class="cell"><div class="content"></div></div>';
	};
	addAttr(item.nomeAluno);
	addAttr(item.nomeCurso);
	addAttr(item.disciplina);
	addAttr(item.semestre);
	let data = new Date(item.data);
	let data_hora = dateToStr(data);
	if (item.horario) {
		const sql = Util.dateSQL(data) + ' ' + item.horario;
		const time = new Date(sql);
		data_hora += ' ' + timeToStr(time);
	}
	addAttr(data_hora);
	html +=
		'<div class="cell open">' +
			'<div class="content">' +
				'<span class="fas fa-edit"></span>' +
			'</div>' +
		'</div>';
	const row = $new('div').addClass('row').html(html);
	row.find('.open .content').css({
		'text-align': 'right',
		'margin-right': '18px',
		'cursor': 'pointer'
	});
	const input = $new('input').attr({
		'type': 'hidden',
		'name': 'id'
	});
	input.val(item.id);
	row.append(input);
	const contents = row.find('.content');
	attrs.forEach((attr, i) => {
		const content = contents.eq(i);
		content.append(domTxt(attr));
		content.parent().attr('title', attr);
	});
	return row;
};
let ini = new Date();
ini.setHours(0);
ini.setMinutes(0);
ini.setSeconds(0);
ini.setMilliseconds(0);
let d0 = Util.dateSQL(ini);
ini.setDate(ini.getDate() + 7);
let d1 = Util.dateSQL(ini);
const {reload} = initPaginationTable(page.find('.pagination-table'), {
	url: '/prova/list',
	data: { d0, d1 },
	cols: [
		{title: 'Aluno', width: '1w'},
		{title: 'Curso', width: '110px'},
		{title: 'Disciplina', width: '100px'},
		{title: 'Sem.', width: '56px'},
		{title: 'Data/Hora', width: '120px'},
		{title: '', width: '50px'},
	],
	itemToRow,
	success: loaded,
	error: () => {
		errorMsg('Erro interno');
		loaded();
	}
});
page.find('[name="d0"]').val(dateToStr(new Date(d0 + ' 00:00:00')));
page.find('[name="d1"]').val(dateToStr(new Date(d1 + ' 00:00:00')));
page.find('[target="sair"]').bind('click', () => {
	killPage(page);
});
page.find('[target="search"]').bind('click', () => {
	let {d0, t0, d1, t1} = getData(page);
	d0 = parseDate(d0);
	if (!d0) return errorMsg('Data de início inválida');
	if (!t0) {
		t0 = '00:00:00';
	} else {
		t0 = parseTime(t0);
		if (!t0) return errorMsg('Horário de início inválido');
	}
	d1 = parseDate(d1);
	if (!d1) return errorMsg('Data final inválida');
	if (!t1) {
		t1 = '23:59:59';
	} else {
		t1 = parseTime(t1);
		if (!t1) return errorMsg('Horário final inválido');
	}
	reload({
		data: {
			d0, t0, d1, t1
		}
	});
});
page.on('click', '.open', function() {
	const id = $(this).closest('.row').find('[name="id"]').val();
	openForm('prova/manter', {id});
});