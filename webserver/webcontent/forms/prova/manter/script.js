const {id, callback} = args;
const select = page.find('select');
const inputIdAluno = page.find('[name="idAluno"]');
page.find('[target="cancel"]').bind('click', () => {
	killPage(page);
	if (callback) {
		callback();
	}
});
const loadCursos = () => new Promise((done, error) => {
	$.get({
		url: '/curso/listByIdAluno',
		data: { idAluno: inputIdAluno.val() },
		success: array => {
			select.html('');
			array.forEach(curso => {
				const {
					nome,
					modalidade,
					idMatricula
				} = curso;
				const option = $new('option');
				option.attr('value', idMatricula);
				option.html(domTxt(`${ nome } - ${ modalidade }`));
				select.append(option);
			});
			done();
		},
		error: err => {
			errorMsg('Erro interno');
			fail(err);
		}
	});
});
page.find('[target="alterar_aluno"]').bind('click', () => {
	openForm('aluno/selecionar', {
		callback: aluno => {
			if (!aluno) return;
			const {id, nome} = aluno;
			page.find('[name="nomeAluno"]').val(nome);
			inputIdAluno.val(id);
			ocupyWorkspace(loadCursos());
		}
	});
});
page.find('[target="remove"]').bind('click', () => {
	ocupyWorkspace(new Promise(done => {
		$.ajax({
			url: '/prova/remove',
			data: {id},
			type: 'POST',
			success: removed => {
				if (removed) {
					sendMsg('Agendamento excluído');
					done();
					killPage(page);
					if (callback) {
						callback();
					}
				} else {
					errorMsg('Erro ao exlcuir agendamento');
					done();
				}
			},
			error: () => {
				errorMsg('Erro ao exlcuir agendamento');
				done();
			}
		});
	}));
});
const send = obj => {
	ocupyWorkspace(new Promise(done => {
		$.ajax({
			type: 'POST',
			url: '/prova/update',
			data: obj,
			success: id => {
				sendMsg('Alterações salvas');
				killPage(page);
				done();
			},
			error: err => {
				errorMsg('Falha ao salvar alterações');
				done();
			}
		});
	}));
};
page.find('[target="submit"]').bind('click', () => {
	let {
		data,
		disciplina,
		horario,
		idAluno,
		semestre,
		idMatricula
	} = getData(page);
	if (!idAluno) {
		return errorMsg('Selecione o aluno');
	}
	if (!idMatricula) {
		return errorMsg('Selecione o curso');
	}
	semestre = semestre.trim();
	if (!semestre) {
		return errorMsg('Informe o semestre');
	}
	disciplina = disciplina.trim();
	if (!disciplina) {
		return errorMsg('Informe a disciplina');
	}
	data = data.trim();
	if (!data) {
		return errorMsg('Informe a data');
	}
	data = parseDate(data);
	if (!data) {
		return errorMsg('Data inválida');
	}
	horario = horario.trim();
	if (horario) {
		horario = parseTime(horario);
		if (!horario) {
			return errorMsg('Hora inválida');
		}
	}
	send({
		id,
		data,
		disciplina,
		horario,
		idAluno,
		semestre,
		idMatricula
	});
});
$.ajax({
	url: '/prova/get',
	data: {id},
	success: res => {
		res.data = new Date(res.data);
		if (res.horario) {
			let {data} = res;
			data = Util.dateSQL(data);
			data += ' ' + res.horario;
			data = new Date(data);
			let h = data.getHours();
			let m = data.getMinutes();
			res.horario = `${h>9?h:'0'+h}:${m>9?m:'0'+m}`;
		}
		let y = res.data.getFullYear();
		let M = res.data.getMonth() + 1;
		let d = res.data.getDate();
		res.data = `${d>9?d:'0'+d}/${M>9?M:'0'+M}/${y}`;
		inputIdAluno.val(res.idAluno);
		page.find('[name="nomeAluno"]').val(res.nomeAluno);
		page.find('[name="semestre"]').val(res.semestre);
		page.find('[name="disciplina"]').val(res.disciplina);
		page.find('[name="data"]').val(res.data);
		page.find('[name="horario"]').val(res.horario);
		loadCursos()
			.then(() => {
				select.val(res.idMatricula);
				loaded();
			})
			.catch(err => {
				killPage(page);
				loaded();
			});
	},
	error: err => {
		errorMsg('Erro interno');
		killPage(page);
		loaded();
	}
});