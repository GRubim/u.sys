import * as Util from './util.js';
import * as PageControl from './page-control.js';

import {
	ocupyWorkspace,
	killPage,
	openForm,
	setForms,
	sendMsg,
	errorMsg,
	initSelectionTable,
	initPaginationTable
} from './page-control.js';
import {
	syncAjax,
	$new,
	domTxt,
	getData,
	noAccent,
	formatSpaces,
	parseTime,
	parseDate
} from './util.js';
import Lembretes from './lembretes.js';

const forms = {
	"agendamentoAva/list": {
		html: "<div class=\"page-title\">Agendamentos AVA</div><div class=\"form-space\"></div><div class=\"n-cols-12\"><div class=\"form-cell text col-wid-10\"><div class=\"field-title\">Aluno</div><input type=\"text\" name=\"nomeAluno\" disabled/></div><input type=\"hidden\" name=\"idAluno\"><div class=\"form-cell col-wid-2\"><input type=\"button\" target=\"search\" value=\"Alterar\"/></div><div class=\"form-cell col-wid-12 pagination-table\"></div><div class=\"form-space\"></div><div class=\"form-cell col-wid-10\"></div><div class=\"form-cell cancel col-wid-2\"><input type=\"button\" target=\"sair\" value=\"Sair\"/></div></div>",
		init: (args, loaded, page) => {
			const {usuario} = args;
			const itemToRow = item => {
				let html = '';
				const attrs = [];
				const addAttr = attr => {
					attrs.push(attr);
					html += '<div class="cell"><div class="content"></div></div>';
				};
				addAttr(item.nomeAluno);
				addAttr(item.nomeCurso);
				html +=
					'<div class="cell open">' +
						'<div class="content">' +
							'<span class="fas fa-edit"></span>' +
						'</div>' +
					'</div>';
				const row = $new('div').addClass('row').html(html);
				row.find('.open .content').css({
					'text-align': 'right',
					'margin-right': '18px',
					'cursor': 'pointer'
				});
				const input = $new('input').attr({
					'type': 'hidden',
					'name': 'id'
				});
				input.val(item.id);
				row.append(input);
				const contents = row.find('.content');
				attrs.forEach((attr, i) => {
					const content = contents.eq(i);
					content.append(domTxt(attr));
					content.parent().attr('title', attr);
				});
				return row;
			};
			const {reload} = initPaginationTable(page.find('.pagination-table'), {
				url: '/agendamentoAva/list',
				cols: [
					{title: 'Aluno', width: '1w'},
					{title: 'Curso', width: '150px'},
					{title: '', width: '50px'},
				],
				itemToRow,
				success: loaded,
				error: () => {
					errorMsg('Erro interno');
					loaded();
				}
			});
			const inputIdAluno = page.find('[name="idAluno"]');
			const inputNomeAluno = page.find('[name="nomeAluno"]');
			const reloadList = () => {
				let idAluno = inputIdAluno.val() || null;
				ocupyWorkspace(new Promise(done => {
					reload({data: {idAluno}})
						.then(done)
						.catch(() => {
							errorMsg('Erro ao atualizar lista');
							done();
						});
				}));
			}
			page.find('[target="sair"]').bind('click', () => {
				killPage(page);
			});
			page.on('click', '.open', function() {
				const id = $(this).closest('.row').find('[name="id"]').val();
				openForm('agendamentoAva/manter', {id, callback: reloadList, usuario});
			});
			page.on('click', '[target="search"]', () => {
				openForm('aluno/selecionar', {
					callback: aluno => {
						if (!aluno) {
							inputIdAluno.val('');
							inputNomeAluno.val('');
						} else {
							inputIdAluno.val(aluno.id);
							inputNomeAluno.val(aluno.nome);
						}
						reloadList();
					}
				});
			});
		}
	}
,	"agendamentoAva/manter": {
		html: "<div class=\"page-title\">Agendamento AVA</div><div class=\"form-space\"></div><div class=\"n-cols-10\"><input type=\"hidden\" name=\"id\"/><div class=\"form-cell text col-wid-6\"><div class=\"field-title\">Aluno</div><input type=\"text\" disabled=\"true\" name=\"nomeAluno\"/></div><div class=\"form-cell text col-wid-4\"><div class=\"field-title\">Gerado em</div><input type=\"text\" disabled=\"true\" name=\"data\"/></div><div class=\"form-cell text col-wid-6\"><div class=\"field-title\">Curso</div><input type=\"text\" disabled=\"true\" name=\"nomeCurso\"/></div><input type=\"hidden\" name=\"idCurso\"/><div class=\"form-cell text col-wid-4\"><div class=\"field-title\">Modalidade</div><input type=\"text\" disabled=\"true\" name=\"modalidade\"/></div><input type=\"hidden\" name=\"idOfertaCurso\"/><input type=\"hidden\" name=\"idUsuario\"/><input type=\"hidden\" name=\"idSiteParceiro\"/></div><div class=\"form-space\"></div><div class=\"n-cols-10\"><div class=\"form-cell col-wid-7\"></div><div class=\"form-cell col-wid-3\"><input type=\"button\" target=\"comments\" value=\"Comentários\"/></div></div><div class=\"form-space\"></div><div class=\"form-line\"></div><div class=\"form-space\"></div><div class=\"n-cols-10\"><div class=\"form-cell col-wid-6\"></div><div class=\"form-cell col-wid-2 cancel\"><input type=\"button\" value=\"Cancelar\"/></div><div class=\"form-cell col-wid-2\"><input type=\"button\" target=\"remove\" value=\"Excluir\"/></div></div>",
		init: (args, loaded, page) => {
			const {id, usuario, callback} = args;
			const close = () => {
				killPage(page);
				if (callback) callback();
			};
			page.find('.cancel').bind('click', () => {
				close();
			});
			const fillForm = agendamento => {
				const data = Util.dateTimeBR(agendamento.data);
				const {nomeCurso, nomeAluno, modalidade} = agendamento;
				page.find('[name="data"]').val(data);
				page.find('[name="nomeAluno"]').val(nomeAluno);
				page.find('[name="nomeCurso"]').val(nomeCurso);
				page.find('[name="modalidade"]').val(modalidade);
			};
			$.get({
				url: '/agendamentoAva/get',
				data: { id },
				success: agendamento => {
					if (!agendamento) {
						errorMsg('Agendamento não encontrado');
						close();
						loaded();
						return;
					}
					fillForm(agendamento);
					loaded();
				},
				error: error => {
					errorMsg('Erro interno');
					close();
					loaded();
				}
			})
			page.find('[target="remove"]').bind('click', () => {
				ocupyWorkspace(new Promise(done => {
					$.post({
						url: '/agendamentoAva/remove',
						data: {id},
						success: removed => {
							sendMsg('Agendamento AVA excluído');
							close();
							done();
						},
						error: error => {
							errorMsg('Erro ao excluir');
							done();
						}
					})
				}));
			});
			page.find('[target="comments"]').bind('click', function() {
				openForm('comentarioAva/manter', { idAgendamento: id, usuario });
			});
		}
	}
,	"aluno/add": {
		html: "<div class=\"page-title\">Aluno</div><div class=\"form-space\"></div><div class=\"n-cols-12\"><div class=\"form-cell text col-wid-12\"><div class=\"field-title\">Nome Completo</div><input type=\"text\" name=\"nome\" autocomplete=\"off\"/></div><div class=\"form-cell text col-wid-8\"><div class=\"field-title\">Email</div><input type=\"text\" name=\"email\" autocomplete=\"off\"/></div><div class=\"form-cell text col-wid-4\"><div class=\"field-title\">Telefone</div><input type=\"text\" name=\"telefone\" autocomplete=\"off\"/></div><div class=\"form-space\"></div><div class=\"form-cell switch col-wid-12\"><div class=\"switch-title\">Continuar cadastrando</div><div class=\"pin-wrap\" tabindex=\"0\"><div class=\"pin\"></div></div><input type=\"hidden\" name=\"switch\" value=\"false\"/></div><div class=\"form-space\"></div><div class=\"form-cell col-wid-8\"></div><div class=\"form-cell cancel col-wid-2\"><input type=\"button\" name=\"cancel\" value=\"Cancelar\"/></div><div class=\"form-cell col-wid-2\"><input type=\"button\" name=\"submit\" value=\"Salvar\"/></div></div>",
		init: (args, loaded, page) => {
			const {usuario} = args;
			const inputNome = page.find('[name="nome"]');
			const sendButton = page.find('[name="submit"]');
			const cancelButton = page.find('[name="cancel"]');
			const keep = page.find('[name="switch"]');
			const getData = () => {
				const nome = inputNome.val().trim();
				const telefone = page.find('[name="telefone"]').val().trim();
				const email = page.find('[name="email"]').val().trim();
				return {nome, telefone, email};
			};
			inputNome.focus();
			sendButton.bind('click', () => {
				const data = getData();
				const {nome, telefone, email} = data;
				if (!nome && !telefone) {
					errorMsg('Os campos nome e telefone são obrigatórios');
					return;
				}
				if (!nome) {
					errorMsg('Insira o nome do aluno');
					return;
				}
				if (!telefone) {
					errorMsg('Insira o telefone do aluno');
					return;
				}
				ocupyWorkspace(new Promise(done => {
					$.post({
						url: '/aluno/add',
						data,
						success: id => {
							if (id) {
								sendMsg('Cadastro concluído');
								if (keep.val() === 'false') {
									killPage(page);
									openForm('aluno/manter', {id, usuario});
								} else {
									'nome,telefone,email'.split(',').forEach(name => {
										page.find(`[name="${name}"]`).val('');
									});
									inputNome.focus();
								}
							} else {
								errorMsg('Falha ao realizar cadastro');
							}
							done();
						},
						error: res => {
							errorMsg('Erro na requisição');
							console.error(res);
							done();
						}
					})
				}));
			});
			cancelButton.bind('click', () => {
				const data = getData();
				const {nome, telefone, email} = data;
				if (!nome && !telefone && !email) {
					killPage(page);
					return;
				}
				if (confirm('Deseja mesmo cancelar este cadastro?')) {
					killPage(page);
				}
			});
			loaded();
			
		}
	}
,	"aluno/interessados": {
		html: "<div class=\"page-title\">Alunos em Negociação</div><div class=\"form-space\"></div><div class=\"n-cols-14\"><div class=\"form-cell text col-wid-12\"><div class=\"field-title\">Colaborador</div><input type=\"text\" name=\"colaborador\" disabled/></div><div class=\"form-cell col-wid-2\" style=\"vertical-align: bottom;\"><input type=\"button\" target=\"alterarColaborador\" value=\"Alterar\"/></div><input type=\"hidden\" name=\"idUsuario\"/><input type=\"hidden\" name=\"idSiteParceiro\"/><div class=\"form-cell col-wid-14 pagination-table\"></div><div class=\"form-space\"></div><div class=\"form-cell col-wid-12\"></div><div class=\"form-cell cancel col-wid-2\"><input type=\"button\" target=\"sair\" value=\"Sair\"/></div></div>",
		init: (args, loaded, page) => {
			const {usuario} = args;
			const itemToRow = item => {
				const {idAluno, nomeAluno, nomeCurso, nomeSite, nomeUsuario} = item;
				const colaborador = nomeSite || nomeUsuario;
				const row = $new('div').addClass('row').html(
					'<div class="cell"><div class="content"></div></div>' +
					'<div class="cell"><div class="content"></div></div>' +
					'<div class="cell"><div class="content"></div></div>' +
					'<div class="cell open"><div class="content">' +
						'<span class="fas fa-list"></span>' +
					'</div></div>' +
					'<input type="hidden" name="idAluno" value="' + idAluno + '"/>'
				);
				const contents = row.find('.content');
				contents.eq(0).append(domTxt(nomeAluno)).parent().attr('title', nomeAluno);
				contents.eq(1).append(domTxt(nomeCurso)).parent().attr('title', nomeCurso);
				contents.eq(2).append(domTxt(colaborador)).parent().attr('title', colaborador);
				contents.eq(3).css({
					'text-align': 'right',
					'margin-right': '18px',
					'cursor': 'pointer'
				});
				return row;
			};
			const {reload} = initPaginationTable(page.find('.pagination-table'), {
				url: '/matricula/interessados',
				cols: [
					{title: 'Aluno', width: '1w'},
					{title: 'Curso de Interesse', width: '150px'},
					{title: 'Colaborador', width: '120px'},
					{title: 'Ver', width: '50px'}
				],
				itemToRow,
				success: loaded,
				error: () => {
					errorMsg('Erro interno');
					loaded();
				}
			});
			page.find('[target="sair"]').bind('click', () => {
				killPage(page);
			});
			page.on('click', '.open', function () {
				const row = $(this).closest('.row');
				const id = row.find('[name="idAluno"]').val();
				openForm('aluno/manter', {id, usuario});
			});
			const updateTable = () => {
				ocupyWorkspace(new Promise(done => {
					const data = Util.getData(page);
					reload({data})
						.then(done)
						.fail(() => {
							errorMsg('Erro ao atualizar listagem');
							done();
						});
				}));
			};
			page.find('[target="alterarColaborador"]').bind('click', () => {
				openForm('colaborador/selecionar', {
					callback: colaborador => {
						page.find('[name="colaborador"]').val(colaborador.nome);
						page.find('[name="idUsuario"]').val(colaborador.idUsuario);
						page.find('[name="idSiteParceiro"]').val(colaborador.idSiteParceiro);
						updateTable();
					}
				});
			});
		}
	}
,	"aluno/manter": {
		html: "<div class=\"page-title\">Aluno</div><div class=\"form-space\"></div><div class=\"n-cols-12\"><input type=\"hidden\" name=\"id\"/><div class=\"form-cell text col-wid-12\"><div class=\"field-title\">Nome Completo</div><input type=\"text\" name=\"nome\" autocomplete=\"off\"/></div><div class=\"form-cell text col-wid-8\"><div class=\"field-title\">Email</div><input type=\"text\" name=\"email\" autocomplete=\"off\"/></div><div class=\"form-cell text col-wid-4\"><div class=\"field-title\">Telefone</div><input type=\"text\" name=\"telefone\" autocomplete=\"off\"/></div><div class=\"form-space\"></div><div class=\"form-cell col-wid-5\"></div><div class=\"form-cell col-wid-1\"><button target=\"add-lembrete\" title=\"Criar lembrete\"><span class=\"far fa-bell\"></span></button></div><div class=\"form-cell col-wid-2\"><input type=\"button\" target=\"delete\" value=\"Excluir\"/></div><div class=\"form-cell col-wid-2\"><input type=\"button\" name=\"historico\" value=\"Histórico\"/></div><div class=\"form-cell col-wid-2\"><input type=\"button\" name=\"submit\" value=\"Salvar\"/></div><div class=\"n-cols-12\"><div class=\"form-space\"></div><div class=\"form-line\"></div><div class=\"form-space\"></div><div class=\"n-cols-12\"><div class=\"page-subtitle\">Matrículas<input type=\"button\" class=\"add-button add-matricula\" value=\"Adicionar\"></div><div class=\"form-cell table-cursos matriculas col-wid-12\"><div class=\"content\"></div></div><div class=\"page-subtitle\">Interesses<input type=\"button\" class=\"add-button add-interesse\" value=\"Adicionar\"></div><div class=\"form-cell table-cursos interesses col-wid-12\"><div class=\"content\"></div></div><div class=\"form-cell col-wid-10\"></div><div class=\"form-cell cancel col-wid-2\"><input type=\"button\" name=\"cancel\" value=\"Sair\"/></div></div>",
		init: (args, loaded, page) => {
			const {id, usuario} = args;
			const getSiteParceiro = () => new Promise(done => {
				openForm('colaborador/siteParceiro/selecionar', {callback: done});
			});
			const getOfertaCurso = () => new Promise(done => {
				openForm('ofertaCurso/selecionar', {callback: done});
			});
			let aluno = null;
			let matriculas = null;
			const decodeEstado = val => {
				switch (val) {
					case 1: return 'Matriculado';
					case 2: return 'Trancado';
					case 3: return 'Cancelado';
					case 4: return 'Interessado';
				}
			};
			let putMatriculas = () => {
				const divMatriculas = page.find('.matriculas div.content').html('');
				const divInteresses = page.find('.interesses div.content').html('');
				matriculas.forEach(({id, estado, modalidade, nomeCurso}) => {
					const div = $new('div').addClass('matricula');
					if (estado !== 1 && estado !== 4) {
						div.addClass('inativa');
					}
					const addCol = (name, val) => div.append($new('div').addClass(name).append(domTxt(val)));
					addCol('nome', nomeCurso);
					addCol('modalidade', modalidade);
					if (estado < 4) {
						addCol('estado', decodeEstado(estado));
					} else if (estado === 5) {
						addCol('estado', '(inativo)');
					}
					addCol('setup', '');
					div.append(`<input type="hidden" name="idMatricula" value="${ id }">`);
					div.find('.setup').html('<span class="fas fa-ellipsis-h setup" tabindex="0"></span>');
					if (estado >= 4) {
						divInteresses.append(div);
					} else {
						divMatriculas.append(div);
					}
				});
			};
			const atualizarMatriculas = () => {
				ocupyWorkspace(new Promise(done => $.get({
					url: '/listMatriculas',
					data: {idAluno: id},
					success: res => {
						matriculas = res;
						putMatriculas();
						done();
					},
					error: err => {
						errorMsg('Erro interno');
						done();
					}
				})));
			};
			syncAjax({
				get: [
					{url: '/aluno/get', data: {id}, success: res => aluno = res},
					{url: '/listMatriculas', data: {
						idAluno: id
					}, success: res => matriculas = res}
				],
				success: () => {
					page.find('[name="id"]').val(id);
					page.find('[name="nome"]').val(aluno.nome);
					page.find('[name="telefone"]').val(aluno.telefone);
					page.find('[name="email"]').val(aluno.email);
					putMatriculas();
					loaded();
				},
				error: () => {
					errorMsg('Erro interno');
					killPage(page);
					loaded();
				}
			});
			page.find('[name="submit"]').bind('click', () => {
				ocupyWorkspace(new Promise(done => {
					$.post({
						url: '/aluno/update',
						data: getData(page),
						success: res => {
							sendMsg('Contato salvo');
							done();
						},
						error: res => {
							errorMsg('Erro interno');
							done();
						}
					});
				}));
			});
			page.find('[name="cancel"]').bind('click', () => {
				killPage(page);
			});
			page.find('.add-matricula').bind('click', () => {
				openForm('matricula/add', {aluno, usuario, callback: id => {
					if (id) atualizarMatriculas();
				}});
			});
			page.find('.add-interesse').bind('click', () => {
				openForm('matricula/add/interesse', {aluno, usuario, callback: id => {
					if (id) atualizarMatriculas();
				}});
			});
			page.on('click', '.setup', function(e){
				e.stopPropagation();
				const {idMatricula} = getData($(this).closest('.matricula'));
				openForm('matricula/manter', {idMatricula, callback: atualizarMatriculas});
			});
			page.find('[name="historico"]').bind('click', () => {
				openForm('comentario/manter', {idAluno: id, usuario});
			});
			page.find('[target="add-lembrete"]').bind('click', () => {
				openForm('lembrete/add', {idAluno: id});
			});
			page.find('[target="delete"]').bind('click', () => {
				openForm('confirm', {
					title: 'Excluir aluno',
					text: 'Deseja mesmo excluir este aluno?\nIsso também irá remover todas suas matrículas',
					callback: res => {
						if (res) {
							ocupyWorkspace(new Promise(done => {
								$.post({
									url: '/aluno/remove',
									data: {id},
									success: ok => {
										if (ok) {
											sendMsg('Aluno removido');
											killPage(page);
										} else {
											errorMsg('Falha ao remover aluno');
										}
										done();
									},
									error: err => {
										errorMsg('Erro interno');
										done();
									}
								});
							}));
						}
					}
				});
			});
			
		}
	}
,	"aluno/selecionar": {
		html: "<div class=\"n-cols-8\"><div class=\"form-cell text col-wid-8\"><div class=\"field-title\">Nome do aluno</div><input type=\"text\" name=\"nome\" autocomplete=\"off\"/></div><div class=\"form-cell selection-table col-wid-8 n-rows-5\"></div><div class=\"form-cell col-wid-6\"></div><div class=\"form-cell cancel col-wid-2\"><input type=\"button\" name=\"cancel\" value=\"Cancelar\"/></div></div>",
		init: (args, loaded, page) => {
			const {callback} = args;
			const inputNome = page.find('[name="nome"]');
			const inputCancel = page.find('[name="cancel"]');
			let lastSearch = '';
			const {applyFilter, setList} = initSelectionTable(page.find('.selection-table'), {
				url: '/aluno/list',
				cols: [
					{title: 'Nome', width: '1w'},
					{title: 'Telefone', width: '100px'},
				],
				toHTMLRow: item => `
					<div class="selection-table-row row" tabindex="0">
						<div class="selection-table-cell"><div>${ item.nome }</div></div>
						<div class="selection-table-cell"><div>${ item.telefone }</div></div>
						<input type="hidden" name="id" value="${ item.id }"/>
						<input type="hidden" name="nome" value="${ item.nome }"/>
					</div>
				`,
				success: () => {
					inputNome.focus();
					loaded();
				},
				error: _=> {
					errorMsg('Erro interno');
					loaded();
				}
			});
			const filter = items => {
				const res = [];
				const str = noAccent(inputNome.val().trim().toLowerCase());
				if (str === lastSearch) return false;
				if (!str) {
					lastSearch = '';
					return items.slice(0, 5);
				}
				for (let i=0; i<items.length && res.length < 5; ++i) {
					const item = items[i];
					if (noAccent(item.nome.toLowerCase()).indexOf(str) !== -1) {
						res.push(item);
					}
				}
				lastSearch = str;
				return res;
			};
			inputCancel.bind('click', function(){
				killPage(page);
				if (callback) {
					callback(false);
				}
			});
			inputNome.bind('keyup', function(){
				if (applyFilter) {
					applyFilter(filter);
				}
			});
			function selectRow(row) {
				const item = getData(row);
				if (!item.id) return;
				killPage(page);
				if (callback) callback(item);
			}
			page.on('click', '.selection-table-row.row', function() {
				selectRow($(this));
			});
			page.on('keydown', '.selection-table-row.row', function (e) {
				const key = e.key.toLowerCase();
				if (key === 'enter' || key === '\n' || key === ' ') {
					selectRow($(this));
				}
			});
			
		}
	}
,	"colaborador/matriculas": {
		html: "<div class=\"page-title\">Matrículas</div><div class=\"form-space\"></div><div class=\"n-cols-14\"><div class=\"form-cell text col-wid-6\"><div class=\"field-title\">Colaborador</div><input type=\"text\" name=\"nome\" disabled=\"true\"/><input type=\"hidden\" name=\"idUsuario\"/><input type=\"hidden\" name=\"idSiteParceiro\"/></div><div class=\"form-cell col-wid-2\"><input type=\"button\" target=\"alterarColaborador\" value=\"Alterar\"/></div><div class=\"form-cell text col-wid-3\"><div class=\"field-title\">De</div><input type=\"text\" name=\"ini\"/></div><div class=\"form-cell text col-wid-3\"><div class=\"field-title\">Até</div><input type=\"text\" name=\"fim\"/></div><div class=\"form-cell col-wid-12\"></div><div class=\"form-cell col-wid-2\"><input type=\"button\" target=\"buscar\" value=\"Buscar\"/></div><div class=\"form-cell pagination-table col-wid-14\"></div><div class=\"form-space\"></div><div class=\"form-cell col-wid-12\"></div><div class=\"form-cell cancel col-wid-2\"><input type=\"button\" target=\"sair\" value=\"Sair\"/></div></div>",
		init: (args, loaded, page) => {
			const itemToRow = item => {
				const row = $new('div').addClass('row');
				row.html(
					'<div class="cell"><div class="content"></div></div>' +
					'<div class="cell"><div class="content"></div></div>'
				);
				const contents = row.find('.content');
				contents.eq(0).append(domTxt(item.nomeAluno));
				contents.eq(1).append(domTxt(item.nomeCurso));
				return row;
			};
			const cols = [
				{title: 'Aluno', width: '1w'},
				{title: 'Curso', width: '250px'},
			];
			const search = ({data, success, error}) => {
				initPaginationTable(page.find('.pagination-table'), {
					url: '/matricula/listPorColaborador',
					data, cols, success, error, itemToRow,
				});
			};
			initPaginationTable(page.find('.pagination-table'), {cols});
			const inputNome = page.find('[name="nome"]');
			const inputIdUsuario = page.find('[name="idUsuario"]');
			const inputIdSiteParceiro = page.find('[name="idSiteParceiro"]');
			page.find('[target="alterarColaborador"]').bind('click', function(){
				openForm('colaborador/selecionar', {
					callback: item => {
						if (item) {
							const {nome, idUsuario, idSiteParceiro} = item;
							inputNome.val(nome + (idUsuario ? ' (Usuário)' : ' (Site parceiro)'));
							inputIdUsuario.val(idUsuario);
							inputIdSiteParceiro.val(idSiteParceiro);
						}
					}
				});
			});
			page.find('[target="sair"]').bind('click', ()=>{
				killPage(page);
			});
			page.find('[target="buscar"]').bind('click', ()=>{
				const data = getData(page);
				const {idUsuario, idSiteParceiro} = data;
				if (!idUsuario && !idSiteParceiro) {
					return errorMsg('Selecione um colaborador');
				}
				const ini = parseDate(data.ini);
				if (!ini) {
					return errorMsg('Data de início inválida');
				}
				const fim = parseDate(data.fim);
				if (!fim) {
					return errorMsg('Data final inválida');
				}
				ocupyWorkspace(new Promise(done => {
					search({
						data: {idUsuario, idSiteParceiro, ini, fim},
						success: done,
						error: () => {
							errorMsg('Erro interno');
							done();
						}
					})
				}));
			});
			const initDates = () => {
				const n = new Date();
				const a = new Date(n.getFullYear(), n.getMonth() - 1, 1);
				const b = new Date(n.getFullYear(), n.getMonth(), 0);
				const toStr = date => {
					let y = date.getFullYear().toString();
					let m = date.getMonth() + 1;
					let d = date.getDate();
					return `${d<10?'0'+d:d}/${m<10?'0'+m:m}/${'0'.repeat(4-y.length)+y}`
				};
				page.find('[name="ini"]').val(toStr(a));
				page.find('[name="fim"]').val(toStr(b));
			};
			initDates();
			loaded();
			
		}
	}
,	"colaborador/selecionar": {
		html: "<div class=\"page-title\">Selecionar Colaborador</div><div class=\"form-space\"></div><div class=\"n-cols-8\"><div class=\"form-cell text col-wid-8\"><div class=\"field-title\">Nome</div><input type=\"text\" name=\"nome\"/></div><div class=\"form-cell switch col-wid-8\"><div class=\"switch-title\">Site</div><div class=\"pin-wrap\" tabindex=\"0\"><div class=\"pin\"></div></div><input type=\"hidden\" name=\"site\" value=\"false\"/></div><div class=\"form-cell selection-table col-wid-8 n-rows-5\"></div></div><div class=\"form-space\"></div><div class=\"n-cols-8\"><div class=\"form-cell col-wid-6\"></div><div class=\"form-cell cancel col-wid-2\"><input type=\"button\" name=\"cancelar\" value=\"Cancelar\"/></div></div>",
		init: (args, loaded, page) => {
			const {callback} = args;
			const inputNome = page.find('[name="nome"]');
			const inputSite = page.find('[name="site"]');
			inputNome.focus();
			let lastSearchName = '';
			let lastSearchSite = '';
			const {applyFilter, setList} = initSelectionTable(page.find('.selection-table'), {
				url: '/colaborador/list',
				cols: [{title: 'Nome', width: '1w'}],
				toHTMLRow: item => `
					<div class="selection-table-row" tabindex="0">
						<div class="selection-table-cell"><div>${ item.nome }</div></div>
						<input type="hidden" name="idUsuario" value="${ item.idUsuario || '' }"/>
						<input type="hidden" name="idSiteParceiro" value="${ item.idSiteParceiro || '' }"/>
						<input type="hidden" name="nome" value="${ item.nome }"/>
					</div>
				`,
				success: () => {
					inputNome.focus();
					applyFilter(filter);
					loaded();
				},
				error: _=> {
					errorMsg('Erro interno');
					loaded();
				}
			});
			const filter = array => {
				const res = [];
				const searchName = inputNome.val().trim().toLowerCase();
				const searchSite = inputSite.val();
				if (searchName === lastSearchName && searchSite === lastSearchSite) {
					return false;
				}
				const flagSite = searchSite === 'true';
				for (let i=0; i !== array.length && res.length !== 5; ++i) {
					const item = array[i];
					if ((flagSite === true) !== (item.idUsuario === undefined)) {
						continue;
					}
					if (noAccent(item.nome.toLowerCase()).indexOf(searchName) !== -1) {
						res.push(item);
					}
				}
				lastSearchName = searchName;
				lastSearchSite = searchSite;
				return res;
			}
			page.find('[name="cancelar"]').bind('click', function(){
				killPage(page);
				if (callback) {
					callback(false);
				}
			});
			inputSite.bind('change', function(){
				applyFilter(filter);
			});
			inputNome.bind('keyup', function(){
				applyFilter(filter);
			});
			function selectRow(row) {
				const item = getData(row);
				if (!item.idSiteParceiro && !item.idUsuario) return;
				killPage(page);
				if (callback) callback(item);
			}
			page.on('click', '.selection-table-row', function(){
				selectRow($(this));
			});
			
		}
	}
,	"colaborador/siteParceiro/add": {
		html: "<div class=\"page-title\">Novo Site Parceiro</div><div class=\"form-space\"></div><div class=\"n-cols-6\"><div class=\"form-cell text col-wid-6\"><div class=\"field-title\">Nome</div><input type=\"text\" name=\"nome\" autocomplete=\"off\"/></div><div class=\"form-space\"></div><div class=\"form-cell col-wid-2\"></div><div class=\"form-cell cancel col-wid-2\"><input type=\"button\" name=\"cancel\" value=\"Sair\"/></div><div class=\"form-cell col-wid-2\"><input type=\"button\" name=\"submit\" value=\"Salvar\"/></div></div>",
		init: (args, loaded, page) => {
			const inputNome = page.find('[name="nome"]').focus();
			page.find('[name="cancel"]').bind('click', ()=>{
				killPage(page);
			});
			page.find('[name="submit"]').bind('click', ()=>{
				let nome = inputNome.val().trim();
				while (nome.indexOf('  ') !== -1) {
					nome = nome.replace('  ', ' ');
				}
				if (!nome) return errorMsg('Insira o nome do site parceiro');
				ocupyWorkspace(new Promise((done, fail) => {
					$.post({
						url: '/addSiteParceiro',
						data: {nome},
						success: id => {
							if (!id) {
								errorMsg('Falha ao registrar site parceiro');
							} else {
								sendMsg('Curso cadastrado com sucesso');
								inputNome.val('');
							}
							done();
						},
						error: err => {
							errorMsg('Erro interno');
							done();
						}
					});
				}));
			});
			loaded();
			
		}
	}
,	"colaborador/siteParceiro/selecionar": {
		html: "<div class=\"n-cols-8\"><div class=\"form-cell text col-wid-8\"><div class=\"field-title\">Nome do site</div><input type=\"text\" name=\"nome\" autocomplete=\"off\"/></div><div class=\"form-cell selection-table col-wid-8 n-rows-5\"></div><div class=\"form-cell col-wid-6\"></div><div class=\"form-cell cancel col-wid-2\"><input type=\"button\" name=\"cancel\" value=\"Cancelar\"/></div></div>",
		init: (args, loaded, page) => {
			const {callback} = args;
			const inputNome = page.find('[name="nome"]');
			const inputCancel = page.find('[name="cancel"]');
			let lastSearch = '';
			const {applyFilter, setList} = initSelectionTable(page.find('.selection-table'), {
				url: '/listSiteParceiro',
				cols: [{title: 'Curso', width: '1w'}],
				toHTMLRow: item => `
					<div class="selection-table-row" tabindex="0">
						<div class="selection-table-cell"><div>${ item.nome }</div></div>
						<input type="hidden" name="id" value="${ item.id }"/>
						<input type="hidden" name="nome" value="${ item.nome }"/>
					</div>
				`,
				success: () => {
					inputNome.focus();
					loaded();
				},
				error: _=> {
					errorMsg('Erro interno');
					loaded();
				}
			});
			const filter = items => {
				const res = [];
				const str = inputNome.val().trim().toLowerCase();
				while (str.indexOf('  ') !== -1) {
					str = str.replace('  ', ' ');
				}
				if (str === lastSearch) return false;
				if (!str) {
					lastSearch = '';
					return items.slice(0, 5);
				}
				for (let i=0; i<items.length && res.length < 5; ++i) {
					const item = items[i];
					if (noAccent(item.nome.toLowerCase()).indexOf(str) !== -1) {
						res.push(item);
					}
				}
				lastSearch = str;
				return res;
			};
			inputCancel.bind('click', function(){
				killPage(page);
				if (callback) callback(false);
			});
			inputNome.bind('keyup', function(){
				if (applyFilter) {
					applyFilter(filter);
				}
			});
			function selectRow(row) {
				const item = getData(row);
				if (!item.id) return;
				killPage(page);
				if (callback) callback(item);
			}
			page.on('click', '.selection-table-row', function() {
				selectRow($(this));
			});
			page.on('keydown', '.selection-table-row', function (e) {
				const key = e.key.toLowerCase();
				if (key === 'enter' || key === '\n' || key === ' ') {
					selectRow($(this));
				}
			});
			
		}
	}
,	"comentario/editar": {
		html: "<div class=\"page-title\">Editar comentário</div><div class=\"n-cols-10\"><div class=\"form-cell col-wid-10\"><div class=\"field-title\"></div><textarea name=\"texto\"></textarea></div><div class=\"form-space\"></div><div class=\"form-cell col-wid-6\"></div><div class=\"form-cell col-wid-2 cancel\"><input type=\"button\" target=\"cancelar\" value=\"Cancelar\"></div><div class=\"form-cell col-wid-2\"><input type=\"button\" target=\"salvar\" value=\"Salvar\"></div></div>",
		init: (args, loaded, page) => {
			const {id, callback} = args;
			const inputTexto = page.find('[name="texto"]');
			page.find('[target="cancelar"]').bind('click', () => {
				killPage(page);
				if (callback) callback(false);
			});
			page.find('[target="salvar"]').bind('click', () => {
				let texto = inputTexto.val().trim();
				ocupyWorkspace(new Promise(done => {
					$.post({
						url: '/comentario/update',
						data: {id, texto},
						success: updated => {
							if (!updated) {
								errorMsg('Falha ao atualizar comentário');
								done();
							} else {
								sendMsg('Comentário atualizado');
								done();
								killPage(page);
								if (callback) callback(true);
							}
						},
						error: err => {
							errorMsg('Erro interno');
							done();
						}
					});
				}));
			});
			$.get({
				url: '/comentario/get',
				data: {id},
				success: comentario => {
					if (!comentario) {
						errorMsg('Comentário não encontrado');
						loaded();
						killPage(page);
						if (callback) callback(false);
					} else {
						inputTexto.val(comentario.texto);
						loaded();
					}
				},
				error: err => {
					errorMsg('Erro interno');
					loaded();
					killPage(page);
					if (callback) callback(false);
				}
			});
		}
	}
,	"comentario/manter": {
		html: "<div class=\"page-title\">Histórico de Comentários</div><div class=\"form-space\"></div><div class=\"n-cols-10\"><div class=\"form-cell col-wid-10\"><div class=\"field-title\">Novo comentário</div><textarea name=\"texto\"></textarea></div><div class=\"form-cell col-wid-8\"></div><div class=\"form-cell col-wid-2\"><button target=\"enviar\"><span class=\"fas fa-envelope\"></span></button></div><div class=\"form-cell chat col-wid-10\"><div class=\"chat-content\"><div class=\"empty\">Nenhum comentário ainda</div></div></div><div class=\"form-space\"></div><div class=\"form-cell col-wid-8\"></div><div class=\"form-cell cancel col-wid-2\"><input type=\"button\" target=\"sair\" value=\"Sair\"/></div></div>",
		init: (args, loaded, page) => {
			const {idAluno, usuario} = args;
			const idUsuario = usuario.id;
			const chatContent = page.find('.chat-content');
			const empty = chatContent.find('.empty');
			const inputTexto = page.find('[name="texto"]');
			const timeToText = date => {
				const y = date.getFullYear();
				const M = date.getMonth() + 1;
				const d = date.getDate();
				const h = date.getHours();
				const m = date.getMinutes();
				return `- ${d<10?'0'+d:d}/${M<10?'0'+M:M}/${y} - ${h<10?'0'+h:h}:${m<10?'0'+m:m}`;
			};
			const setComentarios = array => {
				chatContent.html('');
				if (!array.length) chatContent.append(empty);
				array.forEach(comment => {
					const {id, usuario, texto, horario} = comment;
					const div = $new('div').addClass('comment');
					let editButton = '';
					if (comment.idUsuario == idUsuario) {
						editButton = '<div class="edit-button"><span class="fas fa-edit"></span></div>';
					}
					div[0].innerHTML =
						'<div class="head">' +
							editButton +
							'<span class="username"></span> <span class="time"></span>' +
						'</div>' +
						'<div class="text"></div>' +
						'<input type="hidden" name="id" value="' + id + '">';
					div.find('.username').html(domTxt(usuario));
					const text = div.find('.text');
					texto.split('\n').forEach(line => {
						text.append($new('div').html(domTxt(line)));
					});
					div.find('.time').html(domTxt(timeToText(new Date(horario))));
					chatContent.append(div);
				});
			};
			const updateComentarios = callback => {
				$.get({
					url: '/comentario/list',
					data: {idAluno},
					success: array => {
						setComentarios(array);
						callback();
					},
					error: err => {
						errorMsg('Erro ao atualizar comentários');
						callback();
					}
				});
			};
			page.find('[target="sair"]').bind('click', () => killPage(page));
			page.find('[target="enviar"]').bind('click', () => {
				const texto = inputTexto.val().trim();
				if (!texto) return;
				inputTexto.val('');
				ocupyWorkspace(new Promise(done => {
					$.post({
						url: '/comentario/add',
						data: {idAluno, texto},
						success: res => {
							updateComentarios(done);
						},
						error: err => {
							errorMsg('Erro ao enviar comentário');
							done();
						}
					})
				}));
			});
			page.on('click', 'div.edit-button span', function(){
				const {id} = getData($(this).closest('.comment'));
				openForm('comentario/editar', {id, callback: updated => {
					if (updated) {
						ocupyWorkspace(new Promise(updateComentarios));
					}
				}});
			});
			inputTexto.focus();
			updateComentarios(loaded);
			
		}
	}
,	"comentarioAva/manter": {
		html: "<div class=\"page-title\">Histórico de Comentários</div><div class=\"form-space\"></div><div class=\"n-cols-10\"><div class=\"form-cell col-wid-10\"><div class=\"field-title\">Novo comentário</div><textarea name=\"texto\"></textarea></div><div class=\"form-cell col-wid-8\"></div><div class=\"form-cell col-wid-2\"><button target=\"enviar\"><span class=\"fas fa-envelope\"></span></button></div><div class=\"form-cell chat col-wid-10\"><div class=\"chat-content\"><div class=\"empty\">Nenhum comentário ainda</div></div></div><div class=\"form-space\"></div><div class=\"form-cell col-wid-8\"></div><div class=\"form-cell cancel col-wid-2\"><input type=\"button\" target=\"sair\" value=\"Sair\"/></div></div>",
		init: (args, loaded, page) => {
			const {idAgendamento, usuario} = args;
			const idUsuario = usuario.id;
			const chatContent = page.find('.chat-content');
			const empty = chatContent.find('.empty');
			const inputTexto = page.find('[name="texto"]');
			const timeToText = date => {
				const y = date.getFullYear();
				const M = date.getMonth() + 1;
				const d = date.getDate();
				const h = date.getHours();
				const m = date.getMinutes();
				return `- ${d<10?'0'+d:d}/${M<10?'0'+M:M}/${y} - ${h<10?'0'+h:h}:${m<10?'0'+m:m}`;
			};
			const setComentarios = array => {
				chatContent.html('');
				if (!array.length) chatContent.append(empty);
				array.forEach(comment => {
					const {id, usuario, texto, horario} = comment;
					const div = $new('div').addClass('comment');
					let editButton = '';
					if (comment.idUsuario == idUsuario) {
						editButton = '<div class="edit-button"><span class="fas fa-edit"></span></div>';
					}
					div[0].innerHTML =
						'<div class="head">' +
							editButton +
							'<span class="username"></span> <span class="time"></span>' +
						'</div>' +
						'<div class="text"></div>' +
						'<input type="hidden" name="id" value="' + id + '">';
					div.find('.username').html(domTxt(usuario));
					const text = div.find('.text');
					texto.split('\n').forEach(line => {
						text.append($new('div').html(domTxt(line)));
					});
					div.find('.time').html(domTxt(timeToText(new Date(horario))));
					chatContent.append(div);
				});
			};
			const updateComentarios = callback => {
				$.get({
					url: '/comentarioAva/list',
					data: {idAgendamento},
					success: array => {
						setComentarios(array);
						callback();
					},
					error: err => {
						errorMsg('Erro ao atualizar comentários');
						callback();
					}
				});
			};
			page.find('[target="sair"]').bind('click', () => killPage(page));
			page.find('[target="enviar"]').bind('click', () => {
				const texto = inputTexto.val().trim();
				if (!texto) return;
				inputTexto.val('');
				ocupyWorkspace(new Promise(done => {
					$.post({
						url: '/comentarioAva/add',
						data: {idAgendamento, texto},
						success: res => {
							updateComentarios(done);
						},
						error: err => {
							errorMsg('Erro ao enviar comentário');
							done();
						}
					})
				}));
			});
			page.on('click', 'div.edit-button span', function(){
				const {id} = getData($(this).closest('.comment'));
				openForm('comentarioAva/update', {id, callback: updated => {
					if (updated) {
						ocupyWorkspace(new Promise(updateComentarios));
					}
				}});
			});
			inputTexto.focus();
			updateComentarios(loaded);
			
		}
	}
,	"comentarioAva/update": {
		html: "<div class=\"page-title\">Editar comentário</div><div class=\"n-cols-10\"><div class=\"form-cell col-wid-10\"><div class=\"field-title\"></div><textarea name=\"texto\"></textarea></div><div class=\"form-space\"></div><div class=\"form-cell col-wid-6\"></div><div class=\"form-cell col-wid-2 cancel\"><input type=\"button\" target=\"cancelar\" value=\"Cancelar\"></div><div class=\"form-cell col-wid-2\"><input type=\"button\" target=\"salvar\" value=\"Salvar\"></div></div>",
		init: (args, loaded, page) => {
			const {id, callback} = args;
			const inputTexto = page.find('[name="texto"]');
			page.find('[target="cancelar"]').bind('click', () => {
				killPage(page);
				if (callback) callback(false);
			});
			page.find('[target="salvar"]').bind('click', () => {
				let texto = inputTexto.val().trim();
				ocupyWorkspace(new Promise(done => {
					$.post({
						url: '/comentarioAva/update',
						data: {id, texto},
						success: updated => {
							if (!updated) {
								errorMsg('Falha ao atualizar comentário');
								done();
							} else {
								sendMsg('Comentário atualizado');
								done();
								killPage(page);
								if (callback) callback(true);
							}
						},
						error: err => {
							errorMsg('Erro interno');
							done();
						}
					});
				}));
			});
			$.get({
				url: '/comentarioAva/get',
				data: {id},
				success: comentario => {
					if (!comentario) {
						errorMsg('Comentário não encontrado');
						loaded();
						killPage(page);
						if (callback) callback(false);
					} else {
						inputTexto.val(comentario.texto);
						loaded();
					}
				},
				error: err => {
					errorMsg('Erro interno');
					loaded();
					killPage(page);
					if (callback) callback(false);
				}
			});
		}
	}
,	"confirm": {
		html: "<div class=\"page-title\"></div><div class=\"n-cols-8\"><div class=\"form-cell col-wid-8 plain-text\"></div><div class=\"form-space\"></div><div class=\"form-cell col-wid-4\"></div><div class=\"form-cell col-wid-2 cancel\"><input type=\"button\" target=\"no\" value=\"Não\"></div><div class=\"form-cell col-wid-2\"><input type=\"button\" target=\"yes\" value=\"Sim\"></div></div>",
		init: (args, loaded, page) => {
			const {title, text, callback} = args;
			if (title) {
				page.find('.page-title').html(domTxt(title));
			} else {
				page.find('.page-title').remove();
			}
			text.split('\n').forEach(line => {
				page.find('.plain-text').append($new('div').html(domTxt(line)));
			});
			page.find('[target="yes"]').bind('click', () => {
				killPage(page);
				callback(true);
			});
			page.find('[target="no"]').bind('click', () => {
				killPage(page);
				callback(false);
			});
			loaded();
			
		}
	}
,	"curso/add": {
		html: "<div class=\"page-title\">Novo Curso</div><div class=\"form-space\"></div><div class=\"n-cols-10\"><div class=\"form-cell text col-wid-6\"><div class=\"field-title\">Nome do curso</div><input type=\"text\" name=\"nome\" autocomplete=\"off\"/></div><div class=\"form-cell col-wid-4\"><div class=\"field-title\">Modalidade</div><select name=\"modalidades\"><option value=\"1\">Semi presencial</option><option value=\"2\">100% on-line</option><option value=\"1,2\">Todos</option></select></div><div class=\"form-space\"></div><div class=\"form-cell col-wid-4\"></div><div class=\"form-cell cancel col-wid-3\"><input type=\"button\" name=\"cancel\" value=\"Sair\"/></div><div class=\"form-cell col-wid-3\"><input type=\"button\" name=\"submit\" value=\"Salvar\"/></div></div>",
		init: (args, loaded, page) => {
			const inputNome = page.find('[name="nome"]').focus();
			const select = page.find('select');
			page.find('[name="cancel"]').bind('click', ()=>{
				killPage(page);
			});
			page.find('[name="submit"]').bind('click', ()=>{
				let nome = inputNome.val().trim();
				while (nome.indexOf('  ') !== -1) {
					nome = nome.replace('  ', ' ');
				}
				if (!nome) {
					return errorMsg('Insira o nome do curso');
				}
				ocupyWorkspace(new Promise((done, fail) => {
					$.post({
						url: '/addCurso',
						data: {nome, modalidades: select.val()},
						success: id => {
							if (!id) {
								errorMsg('Falha ao registrar curso');
							} else {
								sendMsg('Curso cadastrado com sucesso');
								inputNome.val('').focus();
							}
							done();
						},
						error: err => {
							errorMsg('Erro interno');
							done();
						}
					});
				}));
			});
			loaded();
		}
	}
,	"curso/list": {
		html: "<div class=\"page-title\">Cursos</div><div class=\"form-space\"></div><div class=\"n-cols-12\"><div class=\"form-cell col-wid-12 pagination-table\"></div><div class=\"form-space\"></div><div class=\"form-cell col-wid-10\"></div><div class=\"form-cell cancel col-wid-2\"><input type=\"button\" target=\"sair\" value=\"Sair\"/></div></div>",
		init: (args, loaded, page) => {
			const itemToRow = item => {
				const row = $new('div').addClass('row').html(
					'<div class="cell">' +
						'<div class="content"></div>' +
					'</div>' +
					'<div class="cell open">' +
						'<div class="content">' +
							'<span class="fas fa-edit"></span>' +
						'</div>' +
					'</div>' +
					'<input type="hidden" name="id" value="' + item.id + '"/>'
				);
				const contents = row.find('.content');
				contents.first().append(domTxt(item.nome));
				contents.last().css({
					'text-align': 'right',
					'margin-right': '18px',
					'cursor': 'pointer'
				});
				return row;
			};
			initPaginationTable(page.find('.pagination-table'), {
				url: '/curso/list',
				cols: [
					{title: 'Curso', width: '1w'},
					{title: '', width: '50px'}
				],
				itemToRow,
				success: loaded,
				error: () => {
					errorMsg('Erro interno');
					loaded();
				}
			});
			page.find('[target="sair"]').bind('click', () => {
				killPage(page);
			});
			page.on('click', '.open', function () {
				const row = $(this).closest('.row');
				const id = row.find('[name="id"]').val();
				openForm('curso/manter', {id});
			});
			
		}
	}
,	"curso/manter": {
		html: "<div class=\"page-title\">Curso</div><div class=\"form-space\"></div><div class=\"n-cols-6\"><div class=\"form-cell text col-wid-6\"><div class=\"field-title\">Curso</div><input type=\"text\" name=\"nome\" autocomplete=\"off\"/></div><div class=\"form-space\"></div><div class=\"form-cell col-wid-2\"></div><div class=\"form-cell cancel col-wid-2\"><input type=\"button\" target=\"sair\" value=\"Sair\"/></div><div class=\"form-cell col-wid-2\"><input type=\"button\" target=\"salvar\" value=\"Salvar\"/></div></div>",
		init: (args, loaded, page) => {
			const {id} = args;
			const inputNome = page.find('[name="nome"]');
			page.find('[target="sair"]').bind('click', ()=>{
				killPage(page);
			});
			page.find('[target="salvar"]').bind('click', ()=>{
				ocupyWorkspace(new Promise(done => {
					$.post({
						url: '/curso/update',
						data: {id, nome: inputNome.val()},
						success: updated => {
							if (updated) {
								sendMsg('Alterações salvas');
							} else {
								errorMsg('Falha ao salvar alterações');
							}
							done();
						},
						error: error => {
							errorMsg('Erro interno');
							done();
						}
					})
				}));
			});
			$.get({
				url: '/curso/get',
				data: {id},
				success: curso => {
					if (!curso) {
						errorMsg('Curso inexistente');
						killPage(page);
					} else {
						inputNome.val(curso.nome);
					}
					loaded();
				},
				error: error => {
					errorMsg('Erro interno');
					killPage(page);
					loaded();
				}
			});
			
		}
	}
,	"curso/selecionar": {
		html: "<div class=\"n-cols-8\"><div class=\"form-cell text col-wid-8\"><div class=\"field-title\">Nome do curso</div><input type=\"text\" name=\"nome\" autocomplete=\"off\"/></div><div class=\"form-cell selection-table col-wid-8 n-rows-5\"></div><div class=\"form-cell col-wid-6\"></div><div class=\"form-cell cancel col-wid-2\"><input type=\"button\" target=\"cancel\" value=\"Cancelar\"/></div></div>",
		init: (args, loaded, page) => {
			const {callback} = args;
			const inputNome = page.find('[name="nome"]');
			const inputCancel = page.find('[target="cancel"]');
			let lastSearch = '';
			const {applyFilter, setList} = initSelectionTable(page.find('.selection-table'), {
				url: '/curso/list',
				cols: [{title: 'Nome', width: '1w'}],
				toHTMLRow: item => `
					<div class="selection-table-row row" tabindex="0">
						<div class="selection-table-cell"><div>${ item.nome }</div></div>
						<input type="hidden" name="id" value="${ item.id }"/>
					</div>
				`,
				success: () => {
					inputNome.focus();
					loaded();
				},
				error: _=> {
					errorMsg('Erro interno');
					loaded();
				}
			});
			const filter = items => {
				const res = [];
				const str = inputNome.val().trim().toLowerCase();
				while (str.indexOf('  ') !== -1) {
					str = str.replace('  ', ' ');
				}
				if (str === lastSearch) return false;
				if (!str) {
					lastSearch = '';
					return items.slice(0, 5);
				}
				for (let i=0; i<items.length && res.length < 5; ++i) {
					const item = items[i];
					if (noAccent(item.nome.toLowerCase()).indexOf(str) !== -1) {
						res.push(item);
					}
				}
				lastSearch = str;
				return res;
			};
			inputCancel.bind('click', function(){
				killPage(page);
				if (callback) {
					callback(false);
				}
			});
			inputNome.bind('keyup', function(){
				if (applyFilter) {
					applyFilter(filter);
				}
			});
			function selectRow(row) {
				const item = getData(row);
				if (!item.id) return;
				killPage(page);
				if (callback) callback(item);
			}
			page.on('click', '.selection-table-row.row', function() {
				selectRow($(this));
			});
			page.on('keydown', '.selection-table-row.row', function (e) {
				const key = e.key.toLowerCase();
				if (key === 'enter' || key === '\n' || key === ' ') {
					selectRow($(this));
				}
			});
			
		}
	}
,	"lembrete/add": {
		html: "<div class=\"page-title\">Novo Lembrete</div><div class=\"form-space\"></div><div class=\"n-cols-8\"><div class=\"form-cell col-wid-3\"><div class=\"field-title\">Data</div><input type=\"text\" name=\"data\"/></div><div class=\"form-cell col-wid-3\"><div class=\"field-title\">Horário</div><input type=\"text\" name=\"horario\"/></div><div class=\"form-cell text col-wid-8\"><div class=\"field-title\">Aluno</div><input type=\"text\" disabled=\"true\" name=\"nomeAluno\"/></div><input type=\"hidden\" name=\"idAluno\"><div class=\"form-cell text col-wid-6\"><div class=\"field-title\">Usuário</div><input type=\"text\" disabled=\"true\" name=\"nomeUsuarioDst\"/></div><div class=\"form-cell col-wid-2\" style=\"vertical-align: bottom\"><input type=\"button\" target=\"pick-user\" value=\"Alterar\"/></div><input type=\"hidden\" name=\"idUsuarioDst\"><div class=\"form-cell col-wid-8\"><div class=\"field-title\">Texto</div><textarea name=\"texto\"></textarea></div><div class=\"form-space\"></div><div class=\"form-cell col-wid-4\"></div><div class=\"form-cell cancel col-wid-2\"><input type=\"button\" target=\"cancel\" value=\"Cancelar\"/></div><div class=\"form-cell col-wid-2\"><input type=\"button\" target=\"submit\" value=\"Salvar\"/></div></div>",
		init: (args, loaded, page) => {
			const {idAluno, nomeAluno} = args;
			page.find('[target="cancel"]').bind('click', ()=>{
				killPage(page);
			});
			page.find('[target="pick-user"]').bind('click', () => {
				openForm('usuario/selecionar', {callback: res => {
					page.find('[name="idUsuarioDst"]').val(res.id);
					page.find('[name="nomeUsuarioDst"]').val(res.nome);
				}});
			});
			page.find('[target="submit"]').bind('click', ()=>{
				const info = getData(page);
				const {texto} = info;
				let data = parseDate(info.data);
				let horario = parseTime(info.horario);
				if (!data) {
					errorMsg('Informe uma data válida');
					return;
				}
				if (!horario) {
					errorMsg('Informe um horário válido');
					return;
				}
				if (!texto) {
					errorMsg('Informe o texto do lembrete');
					return;
				}
				let time = new Date(`${data} ${horario}`);
				let now = new Date();
				if (time < now) {
					errorMsg('Informe uma data e horário futuros');
					return;
				}
				info.horario = `${data} ${horario}`;
				ocupyWorkspace(new Promise(done => {
					$.ajax({
						data: info,
						method: 'POST',
						url: '/lembrete/add',
						success: id => {
							if (id) {
								sendMsg('Lembrete adicionado com sucesso');
								killPage(page);
							} else {
								errorMsg('Falha ao adicionar lembrete');
							}
							done();
						},
						error: err => {
							errorMsg('Falha ao adicionar lembrete');
							done();
						}
					});
				}));
			});
			page.find('[name="idAluno"]').val(idAluno);
			page.find('[type="text"]').first().focus();
			(() => {
				const now = new Date();
				now.setDate(now.getDate() + 1);
				const y = now.getFullYear();
				const M = now.getMonth() + 1;
				const d = now.getDate();
				const h = now.getHours();
				const m = now.getMinutes();
				const data = `${d>9?d:'0'+d}/${M>9?M:'0'+M}/${y}`;
				const hora = `${h>9?h:'0'+h}:${m>9?m:'0'+m}`
				page.find('[name="data"]').val(data);
				page.find('[name="horario"]').val(hora);
			})();
			$.ajax({
				url: '/aluno/get',
				data: {id: idAluno},
				method: 'GET',
				success: aluno => {
					page.find('[name="nomeAluno"]').val(aluno.nome);
					loaded();
				},
				error: err => {
					errorMsg('Erro interno').
					killPage(page);
					loaded();
				}
			});
			
		}
	}
,	"lembrete/list": {
		html: "<div class=\"page-title\">Lembretes</div><div class=\"form-space\"></div><div class=\"n-cols-12\"><div class=\"form-cell col-wid-12 pagination-table\"></div><div class=\"form-space\"></div><div class=\"form-cell col-wid-10\"></div><div class=\"form-cell cancel col-wid-2\"><input type=\"button\" target=\"sair\" value=\"Sair\"/></div></div>",
		init: (args, loaded, page) => {
			const itemToRow = item => {
				const row = $new('div').addClass('row').html(
					'<div class="cell"><div class="content"></div></div>' +
					'<div class="cell"><div class="content"></div></div>' +
					'<div class="cell open">' +
						'<div class="content">' +
							'<span class="fas fa-edit"></span>' +
						'</div>' +
					'</div>' +
					'<input type="hidden" name="id" value="' + item.id + '"/>'
				);
				const contents = row.find('.content');
				contents.eq(0).append(domTxt(item.texto));
				let time = new Date(item.horario);
				const y = time.getFullYear();
				const M = time.getMonth() + 1;
				const d = time.getDate();
				const h = time.getHours();
				const m = time.getMinutes();
				time = `${d>9?d:'0'+d}/${M>9?M:'0'+M}/${y} ${h>9?h:'0'+h}:${m>9?m:'0'+m}`;
				contents.eq(1).append(domTxt(time));
				contents.eq(2).css({
					'text-align': 'right',
					'margin-right': '18px',
					'cursor': 'pointer'
				});
				return row;
			};
			initPaginationTable(page.find('.pagination-table'), {
				url: '/lembrete/list',
				cols: [
					{title: 'Texto', width: '1w'},
					{title: 'Hora', width: '120px'},
					{title: '', width: '50px'}
				],
				itemToRow,
				success: loaded,
				error: () => {
					errorMsg('Erro interno');
					loaded();
				}
			});
			page.find('[target="sair"]').bind('click', () => {
				killPage(page);
			});
			page.on('click', '.open', function () {
				const row = $(this).closest('.row');
				const id = row.find('[name="id"]').val();
				openForm('lembrete/manter', {id});
			});
			
		}
	}
,	"lembrete/manter": {
		html: "<div class=\"page-title\">Editar Lembrete</div><div class=\"form-space\"></div><div class=\"n-cols-8\"><div class=\"form-cell col-wid-3\"><div class=\"field-title\">Data</div><input type=\"text\" name=\"data\"/></div><div class=\"form-cell col-wid-3\"><div class=\"field-title\">Horário</div><input type=\"text\" name=\"horario\"/></div><div class=\"form-cell text col-wid-8\"><div class=\"field-title\">Aluno</div><input type=\"text\" disabled=\"true\" name=\"nomeAluno\"/></div><div class=\"form-cell text col-wid-8\"><div class=\"field-title\">Usuário</div><input type=\"text\" disabled=\"true\" name=\"nomeUsuario\"/></div><div class=\"form-cell col-wid-8\"><div class=\"field-title\">Texto</div><textarea name=\"texto\"></textarea></div><div class=\"form-space\"></div><div class=\"form-cell col-wid-2\"></div><div class=\"form-cell cancel col-wid-2\"><input type=\"button\" target=\"cancel\" value=\"Sair\"/></div><div class=\"form-cell col-wid-2\"><input type=\"button\" target=\"concluir\" value=\"Excluir\"/></div><div class=\"form-cell col-wid-2\"><input type=\"button\" target=\"submit\" value=\"Salvar\"/></div></div>",
		init: (args, loaded, page) => {
			const {id} = args;
			page.find('[target="cancel"]').bind('click', ()=>{
				killPage(page);
			});
			page.find('[target="submit"]').bind('click', ()=>{
				const info = getData(page);
				const {texto} = info;
				let data = parseDate(info.data);
				let horario = parseTime(info.horario);
				if (!data) {
					errorMsg('Informe uma data válida');
					return;
				}
				if (!horario) {
					errorMsg('Informe um horário válido');
					return;
				}
				if (!texto) {
					errorMsg('Informe o texto do lembrete');
					return;
				}
				let time = new Date(`${data} ${horario}`);
				let now = new Date();
				if (time < now) {
					errorMsg('Informe uma data e horário futuros');
					return;
				}
				horario = `${data} ${horario}`;
				ocupyWorkspace(new Promise(done => {
					$.ajax({
						data: {id, texto, horario},
						method: 'POST',
						url: '/lembrete/update',
						success: updated => {
							if (updated) {
								sendMsg('Lembrete atualizado com sucesso');
								Lembretes.count();
							} else {
								errorMsg('Falha ao atualizar lembrete');
							}
							done();
						},
						error: err => {
							errorMsg('Falha ao atualizar lembrete');
							done();
						}
					});
				}));
			});
			$.ajax({
				method: 'GET',
				url: '/lembrete/get',
				data: {id},
				success: obj => {
					if (!obj) {
						errorMsg('Lembrete inexistente');
						killPage(page);
					} else {
						page.find('[name="nomeAluno"]').val(obj.nomeAluno);
						page.find('[name="nomeUsuario"]').val(obj.nomeUsuario);
						page.find('[name="texto"]').val(obj.texto);
						const time = new Date(obj.horario);
						const y = time.getFullYear();
						const M = time.getMonth() + 1;
						const d = time.getDate();
						const h = time.getHours();
						const m = time.getMinutes();
						const data = `${d>9?d:'0'+d}/${M>9?M:'0'+M}/${y}`;
						const hora = `${h>9?h:'0'+h}:${m>9?m:'0'+m}`
						page.find('[name="data"]').val(data);
						page.find('[name="horario"]').val(hora);
					}
					loaded();
				},
				error: err => {
					errorMsg('Erro interno');
					killPage(page);
					loaded();
				}
			});
			page.find('[target="concluir"]').bind('click', ()=>{
				ocupyWorkspace(new Promise(done => {
					$.ajax({
						url: '/lembrete/remove',
						method: 'POST',
						data: {id},
						success: removed => {
							if (removed) {
								sendMsg('Lembrete concluído');
								killPage(page);
								Lembretes.count();
							} else {
								errorMsg('Falha ao excluir lembrete');
							}
							done();
						},
						error: err => {
							errorMsg('Falha ao excluir lembrete');
							done();
						}
					});
				}));
			});
		}
	}
,	"lembrete/notificar": {
		html: "<div class=\"page-title\"><span class=\"far fa-bell\"></span> Lembrete</div><div class=\"n-cols-8\"><input type=\"hidden\" name=\"id\"/><div class=\"form-cell col-wid-8 plain-text\" target=\"content\"></div><div class=\"form-cell col-wid-8 plain-text\">Aluno: <span class=\"aluno-link\"></span></div><div class=\"form-space\"></div><div class=\"form-cell col-wid-2\"></div><div class=\"form-cell col-wid-2 cancel\"><input type=\"button\" target=\"fechar\" value=\"Fechar\"></div><div class=\"form-cell col-wid-2\"><input type=\"button\" target=\"adiar\" value=\"Adiar\"></div><div class=\"form-cell col-wid-2\"><input type=\"button\" target=\"concluir\" value=\"Excluir\"></div></div>",
		init: (args, loaded, page) => {
			const {id} = args;
			page.find('[name="id"]').val(id);
			page.find('[target="fechar"]').bind('click', () => {
				killPage(page);
			});
			page.find('[target="adiar"]').bind('click', () => {
				openForm('lembrete/manter', {id});
			});
			page.find('[target="concluir"]').bind('click', () => {
				ocupyWorkspace(new Promise(done => {
					$.ajax({
						url: '/lembrete/remove',
						data: {id},
						method: 'POST',
						success: res => {
							if (res) {
								killPage(page);
								sendMsg('Lembrete concluído');
								Lembretes.count();
							} else {
								errorMsg('Falha ao concluir lembrete');
							}
							done();
						},
						error: err => {
							errorMsg('Erro interno');
							console.error(err);
							done();
						}
					});
				}));
			});
			$.ajax({
				url: '/lembrete/get',
				data: {id},
				method: 'GET',
				success: lembrete => {
					if (lembrete) {
						const {texto, idAluno, nomeAluno} = lembrete;
						page.find('[target="content"]').html(domTxt(texto));
						page.find('.aluno-link').html(domTxt(nomeAluno)).attr('id-aluno', idAluno);
					} else {
						errorMsg('Lembrete inexistente');
						killPage(page);
					}
					loaded();
				},
				error: err => {
					errorMsg('Erro interno');
					killPage(page);
					loaded();
				}
			});
			
		}
	}
,	"login": {
		html: "<div class=\"page-title\">Fazer Login</div><div class=\"form-space\"></div><div class=\"n-cols-6\"><div class=\"form-cell text col-wid-6\"><div class=\"field-title\">Login</div><input type=\"text\" name=\"login\" autocomplete=\"off\"/></div><div class=\"form-cell password col-wid-6\"><div class=\"field-title\">Senha</div><input type=\"password\" name=\"senha\"/></div><div class=\"form-space\"></div><div class=\"form-cell col-wid-4\"></div><div class=\"form-cell col-wid-2\"><input type=\"button\" name=\"submit\" value=\"Enviar\"/></div></div>",
		init: (args, loaded, page) => {
			const {callback} = args;
			const button = page.find('[type="button"]');
			const inputLogin = page.find('[name="login"]');
			const inputSenha = page.find('[name="senha"]');
			inputLogin.focus();
			const submit = () => {
				const login = inputLogin.val().trim();
				const senha = inputSenha.val();
				if (!login && !senha) return errorMsg('Insira o login e a senha');
				if (!login) return errorMsg('Insira o login');
				if (!senha) return errorMsg('Insira a senha');
				const data = {login, senha};
				ocupyWorkspace(new Promise(done => {
					$.post({
						url: '/login',
						data,
						success: res => {
							if (!res.found) {
								errorMsg('Este login não existe');
							} else if (!res.authenticated) {
								errorMsg('Senha incorreta');
							} else {
								killPage(page);
								if (callback) callback(res.usuario);
							}
							done();
						},
						error: res => {
							errorMsg('Erro interno');
							killPage(page);
							done();
						}
					})
				}));
			};
			button.bind('click', submit);
			inputSenha.bind('keydown', e => {
				const key = e.key.toLowerCase();
				if (key === '\n' || key === 'enter') {
					submit();
				}
			});
			loaded();
			
		}
	}
,	"matricula/add": {
		html: "<div class=\"page-title\">Nova Matrícula</div><div class=\"form-space\"></div><div class=\"n-cols-12\"><div class=\"form-cell text col-wid-12\"><div class=\"field-title\">Aluno</div><input type=\"text\" disabled=\"true\" name=\"aluno\"/></div><input type=\"hidden\" name=\"idAluno\"/><div class=\"form-cell text col-wid-6\"><div class=\"field-title\">Curso</div><input type=\"text\" disabled=\"true\" name=\"curso\"/></div><div class=\"form-cell text col-wid-4\"><div class=\"field-title\">Modalidade</div><input type=\"text\" disabled=\"true\" name=\"modalidade\"/></div><input type=\"hidden\" name=\"idOfertaCurso\"/><div class=\"form-cell col-wid-2\" style=\"vertical-align: bottom;\"><input type=\"button\" name=\"alterarCurso\" value=\"Alterar\"/></div><div class=\"form-cell text col-wid-10\"><div class=\"field-title\">Colaborador</div><input type=\"text\" disabled=\"true\" name=\"colaborador\"/></div><div class=\"form-cell col-wid-2\" style=\"vertical-align: bottom;\"><input type=\"button\" name=\"alterarColaborador\" value=\"Alterar\"/></div><input type=\"hidden\" name=\"estado\" value=\"1\"/><input type=\"hidden\" name=\"idUsuario\"/><input type=\"hidden\" name=\"idSiteParceiro\"/></div><div class=\"form-space\"></div><div class=\"n-cols-12\"><div class=\"form-cell col-wid-6\"></div><div class=\"form-cell col-wid-3 cancel\"><input type=\"button\" value=\"Cancelar\"/></div><div class=\"form-cell col-wid-3\"><input type=\"button\" name=\"concluir\" value=\"Concluir\"/></div></div>",
		init: (args, loaded, page) => {
			const {aluno, callback, usuario} = args;
			if (aluno) {
				page.find('[name="idAluno"]').val(aluno.id);
				page.find('[name="aluno"]').val(aluno.nome);
			}
			page.find('[name="alterarAluno"]').bind('click', function(){
				openForm('aluno/selecionar', {callback: aluno => {
					if (aluno) {
						page.find('[name="aluno"]').val(aluno.nome);
						page.find('[name="idAluno"]').val(aluno.id);
					}
				}});
			});
			page.find('[name="alterarCurso"]').bind('click', function(){
				openForm('ofertaCurso/selecionar', {callback: curso => {
					if (curso) {
						const {id, nome, modalidade} = curso;
						page.find('[name="curso"]').val(nome);
						page.find('[name="idOfertaCurso"]').val(id);
						page.find('[name="modalidade"]').val(modalidade);
					}
				}});
			});
			page.find('[name="alterarColaborador"]').bind('click', function(){
				openForm('colaborador/selecionar', {callback: res => {
					if (res) {
						const {idUsuario, idSiteParceiro, nome} = res;
						const str = nome + (idUsuario ? ' (Usuário)' : ' (Site parceiro)');
						page.find('[name="colaborador"]').val(str);
						page.find('[name="idUsuario"]').val(idUsuario);
						page.find('[name="idSiteParceiro"]').val(idSiteParceiro);
					}
				}});
			});
			page.find('.cancel input[type="button"]').bind('click', function(){
				killPage(page);
				if (callback) callback();
			});
			page.find('input[name="concluir"]').bind('click', function(){
				const data = getData(page);
				const {idAluno, idUsuario, idOfertaCurso, idSiteParceiro} = data;
				if (!idAluno) return errorMsg('Selecione um aluno');
				if (!idOfertaCurso) return errorMsg('Selecione um curso');
				if (!idUsuario && !idSiteParceiro) return errorMsg('Selecione um colaborador');
				ocupyWorkspace(new Promise(done => {
					$.post({
						url: '/addMatricula',
						data,
						success: res => {
							done();
							if (res) {
								sendMsg('Matrícula criada');
								if (callback) {
									killPage(page);
									callback(res);
								}
							} else {
								errorMsg('Falha ao registrar matrícula');
							}
						},
						error: res => {
							errorMsg('Erro interno');
							done();
						}
					});
				}));
			});
			page.find('[name="idUsuario"]').val(usuario.id);
			page.find('[name="colaborador"]').val(usuario.nome + ' (Usuário)');
			loaded();
			
		}
	}
,	"matricula/add/interesse": {
		html: "<div class=\"page-title\">Registrar Interesse</div><div class=\"form-space\"></div><div class=\"n-cols-12\"><div class=\"form-cell text col-wid-12\"><div class=\"field-title\">Aluno</div><input type=\"text\" disabled=\"true\" name=\"aluno\"/></div><input type=\"hidden\" name=\"idAluno\"/><div class=\"form-cell text col-wid-6\"><div class=\"field-title\">Curso</div><input type=\"text\" disabled=\"true\" name=\"curso\"/></div><div class=\"form-cell text col-wid-4\"><div class=\"field-title\">Modalidade</div><input type=\"text\" disabled=\"true\" name=\"modalidade\"/></div><input type=\"hidden\" name=\"idOfertaCurso\"/><div class=\"form-cell col-wid-2\" style=\"vertical-align: bottom;\"><input type=\"button\" name=\"alterarCurso\" value=\"Alterar\"/></div><div class=\"form-cell text col-wid-10\"><div class=\"field-title\">Colaborador</div><input type=\"text\" disabled=\"true\" name=\"colaborador\"/></div><div class=\"form-cell col-wid-2\" style=\"vertical-align: bottom;\"><input type=\"button\" name=\"alterarColaborador\" value=\"Alterar\"/></div><input type=\"hidden\" name=\"estado\" value=\"4\"/><input type=\"hidden\" name=\"idUsuario\"/><input type=\"hidden\" name=\"idSiteParceiro\"/></div><div class=\"form-space\"></div><div class=\"n-cols-12\"><div class=\"form-cell col-wid-6\"></div><div class=\"form-cell col-wid-3 cancel\"><input type=\"button\" value=\"Cancelar\"/></div><div class=\"form-cell col-wid-3\"><input type=\"button\" name=\"concluir\" value=\"Concluir\"/></div></div>",
		init: (args, loaded, page) => {
			const {aluno, callback, usuario} = args;
			const dataToStr = data => {
				const fillZ = (val, n) => {
					val = val.toString();
					while (val.length < n) {
						val = '0' + val;
					}
					return val;
				}
				return fillZ(data.getDate(), 2)
					+ '/' + fillZ(data.getMonth() + 1, 2)
					+ '/' + fillZ(data.getFullYear(), 4);
			};
			const parseData = str => {
				const array = str.trim().split('/');
				if (array.length !== 3) return false;
				let d = parseInt(array[0], 10);
				let m = parseInt(array[1], 10);
				let y = parseInt(array[2], 10);
				if (!d || !m || !y)  return false;
				if (d < 1 || d > 31) return false;
				if (m < 1 || m > 12) return false;
				if (y < 1) return false;
				let data = new Date(y, m - 1, d);
				if (data.getFullYear() !== y)     return false;
				if (data.getMonth()    !== m - 1) return false;
				if (data.getDate()     !== d)     return false;
				return data;
			};
			const getData = () => {
				const data = {};
				page.find('input[name]').each(function(){
					const item = $(this);
					data[item.attr('name')] = item.val() || null;
				});
				return data;
			};
			if (aluno) {
				page.find('[name="idAluno"]').val(aluno.id);
				page.find('[name="aluno"]').val(aluno.nome);
			}
			page.find('[name="alterarAluno"]').bind('click', function(){
				openForm('aluno/selecionar', {callback: aluno => {
					if (aluno) {
						page.find('[name="aluno"]').val(aluno.nome);
						page.find('[name="idAluno"]').val(aluno.id);
					}
				}});
			});
			page.find('[name="alterarCurso"]').bind('click', function(){
				openForm('ofertaCurso/selecionar', {callback: curso => {
					if (curso) {
						const {id, nome, modalidade} = curso;
						page.find('[name="curso"]').val(nome);
						page.find('[name="idOfertaCurso"]').val(id);
						page.find('[name="modalidade"]').val(modalidade);
					}
				}});
			});
			page.find('[name="alterarColaborador"]').bind('click', function(){
				openForm('colaborador/selecionar', {callback: res => {
					if (res) {
						const {idUsuario, idSiteParceiro, nome} = res;
						const str = nome + (idUsuario ? ' (Usuário)' : ' (Site parceiro)');
						page.find('[name="colaborador"]').val(str);
						page.find('[name="idUsuario"]').val(idUsuario);
						page.find('[name="idSiteParceiro"]').val(idSiteParceiro);
					}
				}});
			});
			page.find('.cancel input[type="button"]').bind('click', function(){
				killPage(page);
				if (callback) callback();
			});
			page.find('input[name="concluir"]').bind('click', function(){
				const data = getData();
				const {idAluno, idUsuario, idOfertaCurso, idSiteParceiro} = data;
				if (!idAluno) return errorMsg('Selecione um aluno');
				if (!idOfertaCurso) return errorMsg('Selecione um curso');
				if (!idUsuario && !idSiteParceiro) return errorMsg('Selecione um colaborador');
				ocupyWorkspace(new Promise(done => {
					$.post({
						url: '/addMatricula',
						data,
						success: res => {
							done();
							if (res) {
								sendMsg('Interesse registrado');
								if (callback) {
									killPage(page);
									callback(res);
								}
							} else {
								errorMsg('Falha ao registrar interesse');
							}
						},
						error: res => {
							errorMsg('Erro interno');
							done();
						}
					});
				}));
			});
			page.find('[name="data"]').val(dataToStr(new Date()));
			page.find('[name="idUsuario"]').val(usuario.id);
			page.find('[name="colaborador"]').val(usuario.nome + ' (Usuário)');
			loaded();
			
		}
	}
,	"matricula/manter": {
		html: "<div class=\"page-title\">Matrícula</div><div class=\"form-space\"></div><div class=\"n-cols-12\"><input type=\"hidden\" name=\"id\"/><div class=\"form-cell text col-wid-12\"><div class=\"field-title\">Aluno</div><input type=\"text\" disabled=\"true\" name=\"aluno\"/></div><div class=\"form-cell text col-wid-6\"><div class=\"field-title\">Curso</div><input type=\"text\" disabled=\"true\" name=\"curso\"/></div><input type=\"hidden\" name=\"idCurso\"/><div class=\"form-cell text col-wid-4\"><div class=\"field-title\">Modalidade</div><input type=\"text\" disabled=\"true\" name=\"modalidade\"/></div><input type=\"hidden\" name=\"idOfertaCurso\"/><div class=\"form-cell col-wid-2\" style=\"vertical-align: bottom;\"><input type=\"button\" name=\"alterarModalidade\" value=\"Alterar\"/></div><div class=\"form-cell text col-wid-12\"><div class=\"field-title\">Colaborador</div><input type=\"text\" disabled=\"true\" name=\"colaborador\"/></div><div class=\"form-cell text col-wid-4\"><div class=\"field-title\">Estado</div><select name=\"estado\"></select></div><input type=\"hidden\" name=\"idUsuario\"/><input type=\"hidden\" name=\"idSiteParceiro\"/></div><div class=\"form-space\"></div><div class=\"n-cols-12\"><div class=\"form-cell col-wid-5\"></div><div class=\"form-cell col-wid-2 cancel\"><input type=\"button\" value=\"Cancelar\"/></div><div class=\"form-cell col-wid-3\" style=\"vertical-align: bottom;\"><input type=\"button\" target=\"agendar\" value=\"Agendar AVA\"/></div><div class=\"form-cell col-wid-2\"><input type=\"button\" name=\"salvar\" value=\"Salvar\"/></div></div>",
		init: (args, loaded, page) => {
			const ESTADO_MATRICULADO = 1;
			const ESTADO_TRANCADO = 2;
			const ESTADO_CANCELADO = 3;
			const ESTADO_INTERESSADO = 4;
			const ESTADO_INTERESSE_INATIVO = 5;
			
			const {idMatricula, callback} = args;
			const decodeEstado = code => {
				switch (code) {
					case 1: return 'Matriculado';
					case 2: return 'Trancado';
					case 3: return 'Cancelado';
					case 4: return 'Interessado';
					case 5: return 'Interesse inativo';
				}
			};
			const select = page.find('select');
			const addEstado = code => select.append(`<option value="${ code }">${ decodeEstado(code) }</option>`);
			const inputIdCurso = page.find('[name="idCurso"]');
			const inputIdOferta = page.find('[name="idOfertaCurso"]');
			const inputCurso = page.find('[name="curso"]');
			const inputModalidade = page.find('[name="modalidade"]');
			const agendarButton = page.find('[target="agendar"]');
			
			let dataLoaded = null;
			let buttonLoaded = null;
			const asyncCallEnded = () => {
				if (dataLoaded === false || buttonLoaded === false) {
					killPage(page);
					loaded();
				}
				if (dataLoaded && buttonLoaded) {
					loaded();
				}
			};
			
			$.get({
				url: '/getMatriculaById',
				data: {id: idMatricula},
				success: matricula => {
					const {id, estado, idOfertaCurso, nomeAluno, idCurso, nomeCurso, modalidade, nomeSite, nomeUsuario} = matricula;
					addEstado(estado);
					select.val(estado);
					if (estado === 1) {
						addEstado(2);
						addEstado(3);
					} else {
						addEstado(1);
						if (estado === 2) {
							addEstado(3);
						} else if (estado === 4) {
							addEstado(5);
						} else if (estado === 5) {
							addEstado(4);
						}
					}
					select.val(estado);
					inputCurso.val(nomeCurso);
					inputIdCurso.val(idCurso);
					inputIdOferta.val(idOfertaCurso);
					inputModalidade.val(modalidade);
					page.find('[name="aluno"]').val(nomeAluno);
					page.find('[name="colaborador"]').val(nomeSite ? nomeSite + ' (Site parceiro)' : nomeUsuario + ' (Usuário)');
					dataLoaded = true;
					asyncCallEnded();
				},
				error: err => {
					dataLoaded = false;
					errorMsg('Erro interno');
					asyncCallEnded();
				}
			});
			
			const updateButton = () => new Promise((done, fail) => {
				$.get({
					url: '/agendamentoAva/podeAgendar',
					data: {idMatricula},
					success: res => {
						if (res) {
							agendarButton.removeAttr('disabled');
						} else {
							agendarButton.prop('disabled', true);
						}
						done();
					},
					error: err => {
						errorMsg('Erro interno');
						fail();
					}
				});
			});
			
			updateButton()
				.then(() => {
					buttonLoaded = true;
					asyncCallEnded();
				})
				.catch(() => {
					buttonLoaded = false;
					asyncCallEnded();
				});
			
			page.find('[name="id"]').val(idMatricula);
			page.find('.cancel input[type="button"]').bind('click', function(){
				killPage(page);
				if (callback) {
					callback(false);
				}
			});
			page.find('[name="alterarModalidade"]').bind('click', function(){
				openForm('ofertaCurso/selecionar', {
					idCurso: inputIdCurso.val(),
					callback: oferta => {
						inputIdOferta.val(oferta.id);
						inputCurso.val(oferta.nome);
						inputModalidade.val(oferta.modalidade);
					}
				});
			});
			page.find('[name="salvar"]').bind('click', function(){
				ocupyWorkspace(new Promise(done => {
					$.post({
						url: './updateMatricula',
						data: getData(page),
						success: updated => {
							if (updated) {
								if (callback) {
									killPage(page);
									done();
									callback();
								} else {
									sendMsg('Matrícula atualizada');
								}
								done();
								return;
							}
							errorMsg('Falha ao salvar matrícula');
							done();
						},
						error: err => {
							errorMsg('Erro interno');
							done();
						}
					});
				}));
			});
			agendarButton.bind('click', () => {
				if (agendarButton.is('[disabled]')) return;
				ocupyWorkspace(new Promise(done => {
					$.post({
						url: '/agendamentoAva/add',
						data: {idMatricula},
						success: id => {
							sendMsg('Agendamento AVA criado');
							agendarButton.prop("disabled", true);
							done();
						},
						error: err => {
							errorMsg('Erro interno');
							done();
						}
					});
				}));
			});
		}
	}
,	"ofertaCurso/selecionar": {
		html: "<div class=\"n-cols-8\"><div class=\"form-cell text col-wid-8\"><div class=\"field-title\">Nome do usuário</div><input type=\"text\" name=\"nome\" autocomplete=\"off\"/></div><div class=\"form-cell selection-table col-wid-8 n-rows-5\"></div><div class=\"form-cell col-wid-6\"></div><div class=\"form-cell cancel col-wid-2\"><input type=\"button\" name=\"cancel\" value=\"Cancelar\"/></div></div>",
		init: (args, loaded, page) => {
			const {callback, idCurso} = args;
			const inputNome = page.find('[name="nome"]');
			const inputCancel = page.find('[name="cancel"]');
			let lastSearch = '';
			let curso = null;
			const {applyFilter, setList} = initSelectionTable(page.find('.selection-table'), {
				url: '/listOfertaCurso',
				data: {idCurso},
				cols: [
					{title: 'Curso', width: '1w'},
					{title: 'Modalidade', width: '120px'},
				],
				toHTMLRow: item => `
					<div class="selection-table-row" tabindex="0">
						<div class="selection-table-cell"><div>${ item.nome }</div></div>
						<div class="selection-table-cell"><div>${ item.modalidade }</div></div>
						<input type="hidden" name="id" value="${ item.id }"/>
						<input type="hidden" name="nome" value="${ item.nome }"/>
						<input type="hidden" name="modalidade" value="${ item.modalidade }"/>
					</div>
				`,
				success: () => {
					inputNome.focus();
					loaded();
				},
				error: _=> {
					errorMsg('Erro interno');
					loaded();
				}
			});
			const filter = items => {
				const res = [];
				const str = inputNome.val().trim().toLowerCase();
				while (str.indexOf('  ') !== -1) {
					str = str.replace('  ', ' ');
				}
				if (str === lastSearch) return false;
				if (!str) {
					lastSearch = '';
					return items.slice(0, 5);
				}
				for (let i=0; i<items.length && res.length < 5; ++i) {
					const item = items[i];
					if (noAccent(item.nome.toLowerCase()).indexOf(str) !== -1) {
						res.push(item);
					}
				}
				lastSearch = str;
				return res;
			};
			inputCancel.bind('click', function(){
				killPage(page);
			});
			inputNome.bind('keyup', function(){
				if (applyFilter) {
					applyFilter(filter);
				}
			});
			function selectRow(row) {
				const item = getData(row);
				if (!item.id) return;
				killPage(page);
				if (callback) callback(item);
			}
			page.on('click', '.selection-table-row', function() {
				selectRow($(this));
			});
			page.on('keydown', '.selection-table-row', function (e) {
				const key = e.key.toLowerCase();
				if (key === 'enter' || key === '\n' || key === ' ') {
					selectRow($(this));
				}
			});
		}
	}
,	"prova/add": {
		html: "<div class=\"page-title\">Nova Prova</div><div class=\"form-space\"></div><div class=\"n-cols-10\"><div class=\"form-cell text col-wid-8\"><div class=\"field-title\">Aluno</div><input type=\"text\" name=\"nomeAluno\" disabled=\"true\"/><input type=\"hidden\" name=\"idAluno\"/></div><div class=\"form-cell col-wid-2\"><input type=\"button\" target=\"alterar_aluno\" value=\"Alterar\"/></div><div class=\"form-cell col-wid-7\"><div class=\"field-title\">Curso</div><select name=\"idMatricula\"></select></div><div class=\"form-cell col-wid-3\"><div class=\"field-title\">Semestre</div><input type=\"text\" name=\"semestre\"></div><div class=\"form-cell text col-wid-4\"><div class=\"field-title\">Disciplina</div><input type=\"text\" name=\"disciplina\"/></div><div class=\"form-cell col-wid-3\"><div class=\"field-title\">Data</div><input type=\"text\" name=\"data\"></div><div class=\"form-cell col-wid-3\"><div class=\"field-title\">Hora</div><input type=\"text\" name=\"horario\" placeholder=\"opcional\"></div><div class=\"form-space\"></div><div class=\"form-cell col-wid-6\"></div><div class=\"form-cell cancel col-wid-2\"><input type=\"button\" name=\"cancel\" value=\"Sair\"/></div><div class=\"form-cell col-wid-2\"><input type=\"button\" name=\"submit\" value=\"Salvar\"/></div></div>",
		init: (args, loaded, page) => {
			const select = page.find('select');
			const inputIdAluno = page.find('[name="idAluno"]');
			page.find('[name="cancel"]').bind('click', ()=>{
				killPage(page);
			});
			const loadCursos = () => {
				const idAluno = inputIdAluno.val();
				if (!idAluno) return;
				ocupyWorkspace(new Promise(done => {
					$.get({
						url: '/curso/listByIdAluno',
						data: { idAluno },
						success: array => {
							select.html('');
							array.forEach(curso => {
								const {
									nome,
									modalidade,
									idMatricula
								} = curso;
								const option = $new('option');
								option.attr('value', idMatricula);
								option.html(domTxt(`${ nome } - ${ modalidade }`));
								select.append(option);
							});
							done();
						},
						error: err => {
							errorMsg('Erro ao carregar cursos');
							done();
						}
					});
				}));
			};
			page.find('[target="alterar_aluno"]').bind('click', ()=>{
				openForm('aluno/selecionar', {
					callback: aluno => {
						if (!aluno) return;
						const {id, nome} = aluno;
						page.find('[name="nomeAluno"]').val(nome);
						inputIdAluno.val(id);
						loadCursos();
					}
				});
			});
			const send = obj => {
				ocupyWorkspace(new Promise(done => {
					$.ajax({
						type: 'POST',
						url: '/prova/add',
						data: obj,
						success: id => {
							sendMsg('Agendamento reazliado com sucesso');
							killPage(page);
							done();
						},
						error: err => {
							errorMsg('Falha ao agendar prova');
							done();
						}
					});
				}));
			};
			page.find('[name="submit"]').bind('click', ()=>{
				let {
					data,
					disciplina,
					horario,
					idAluno,
					semestre,
					idMatricula
				} = getData(page);
				if (!idAluno) {
					return errorMsg('Selecione o aluno');
				}
				if (!idMatricula) {
					return errorMsg('Selecione o curso');
				}
				semestre = semestre.trim();
				if (!semestre) {
					return errorMsg('Informe o semestre');
				}
				disciplina = disciplina.trim();
				if (!disciplina) {
					return errorMsg('Informe a disciplina');
				}
				data = data.trim();
				if (!data) {
					return errorMsg('Informe a data');
				}
				data = parseDate(data);
				if (!data) {
					return errorMsg('Data inválida');
				}
				horario = horario.trim();
				if (horario) {
					horario = parseTime(horario);
					if (!horario) {
						return errorMsg('Hora inválida');
					}
				}
				send({
					data,
					disciplina,
					horario,
					idAluno,
					semestre,
					idMatricula
				});
			});
			loaded();
		}
	}
,	"prova/list": {
		html: "<div class=\"page-title\">Provas Agendadas</div><div class=\"form-space\"></div><div class=\"n-cols-16\"><div class=\"form-cell text col-wid-3\"><div class=\"field-title\">Data (início)</div><input type=\"text\" name=\"d0\"/></div><div class=\"form-cell text col-wid-3\"><div class=\"field-title\">Hora (início)</div><input type=\"text\" name=\"t0\" placeholder=\"opcional\"/></div><div class=\"form-cell text col-wid-3\"><div class=\"field-title\">Data (fim)</div><input type=\"text\" name=\"d1\"/></div><div class=\"form-cell text col-wid-3\"><div class=\"field-title\">Hora (fim)</div><input type=\"text\" name=\"t1\" placeholder=\"opcional\"/></div><div class=\"form-cell col-wid-2\"><input type=\"button\" target=\"search\" value=\"Buscar\"/></div><div class=\"form-cell col-wid-16 pagination-table\"></div><div class=\"form-space\"></div><div class=\"form-cell col-wid-14\"></div><div class=\"form-cell cancel col-wid-2\"><input type=\"button\" target=\"sair\" value=\"Sair\"/></div></div>",
		init: (args, loaded, page) => {
			const dateToStr = date => {
				const y = date.getFullYear();
				const M = date.getMonth() + 1;
				const d = date.getDate();
				return `${d>9?d:'0'+d}/${M>9?M:'0'+M}/${y}`;
			};
			const timeToStr = time => {
				const h = time.getHours();
				const m = time.getMinutes();
				return `${h>9?h:'0'+h}:${m>9?m:'0'+m}`;
			};
			const itemToRow = item => {
				let html = '';
				const attrs = [];
				const addAttr = attr => {
					attrs.push(attr);
					html += '<div class="cell"><div class="content"></div></div>';
				};
				addAttr(item.nomeAluno);
				addAttr(item.nomeCurso);
				addAttr(item.disciplina);
				addAttr(item.semestre);
				let data = new Date(item.data);
				let data_hora = dateToStr(data);
				if (item.horario) {
					const sql = Util.dateSQL(data) + ' ' + item.horario;
					const time = new Date(sql);
					data_hora += ' ' + timeToStr(time);
				}
				addAttr(data_hora);
				html +=
					'<div class="cell open">' +
						'<div class="content">' +
							'<span class="fas fa-edit"></span>' +
						'</div>' +
					'</div>';
				const row = $new('div').addClass('row').html(html);
				row.find('.open .content').css({
					'text-align': 'right',
					'margin-right': '18px',
					'cursor': 'pointer'
				});
				const input = $new('input').attr({
					'type': 'hidden',
					'name': 'id'
				});
				input.val(item.id);
				row.append(input);
				const contents = row.find('.content');
				attrs.forEach((attr, i) => {
					const content = contents.eq(i);
					content.append(domTxt(attr));
					content.parent().attr('title', attr);
				});
				return row;
			};
			let ini = new Date();
			ini.setHours(0);
			ini.setMinutes(0);
			ini.setSeconds(0);
			ini.setMilliseconds(0);
			let d0 = Util.dateSQL(ini);
			ini.setDate(ini.getDate() + 7);
			let d1 = Util.dateSQL(ini);
			const {reload} = initPaginationTable(page.find('.pagination-table'), {
				url: '/prova/list',
				data: { d0, d1 },
				cols: [
					{title: 'Aluno', width: '1w'},
					{title: 'Curso', width: '110px'},
					{title: 'Disciplina', width: '100px'},
					{title: 'Sem.', width: '56px'},
					{title: 'Data/Hora', width: '120px'},
					{title: '', width: '50px'},
				],
				itemToRow,
				success: loaded,
				error: () => {
					errorMsg('Erro interno');
					loaded();
				}
			});
			page.find('[name="d0"]').val(dateToStr(new Date(d0 + ' 00:00:00')));
			page.find('[name="d1"]').val(dateToStr(new Date(d1 + ' 00:00:00')));
			page.find('[target="sair"]').bind('click', () => {
				killPage(page);
			});
			page.find('[target="search"]').bind('click', () => {
				let {d0, t0, d1, t1} = getData(page);
				d0 = parseDate(d0);
				if (!d0) return errorMsg('Data de início inválida');
				if (!t0) {
					t0 = '00:00:00';
				} else {
					t0 = parseTime(t0);
					if (!t0) return errorMsg('Horário de início inválido');
				}
				d1 = parseDate(d1);
				if (!d1) return errorMsg('Data final inválida');
				if (!t1) {
					t1 = '23:59:59';
				} else {
					t1 = parseTime(t1);
					if (!t1) return errorMsg('Horário final inválido');
				}
				reload({
					data: {
						d0, t0, d1, t1
					}
				});
			});
			page.on('click', '.open', function() {
				const id = $(this).closest('.row').find('[name="id"]').val();
				openForm('prova/manter', {id});
			});
		}
	}
,	"prova/manter": {
		html: "<div class=\"page-title\">Editar Agendamento</div><div class=\"form-space\"></div><div class=\"n-cols-10\"><div class=\"form-cell text col-wid-8\"><div class=\"field-title\">Aluno</div><input type=\"text\" name=\"nomeAluno\" disabled=\"true\"/><input type=\"hidden\" name=\"idAluno\"/></div><div class=\"form-cell col-wid-2\"><input type=\"button\" target=\"alterar_aluno\" value=\"Alterar\"/></div><div class=\"form-cell col-wid-7\"><div class=\"field-title\">Curso</div><select name=\"idMatricula\"></select></div><div class=\"form-cell col-wid-3\"><div class=\"field-title\">Semestre</div><input type=\"text\" name=\"semestre\"></div><div class=\"form-cell text col-wid-4\"><div class=\"field-title\">Disciplina</div><input type=\"text\" name=\"disciplina\"/></div><div class=\"form-cell col-wid-3\"><div class=\"field-title\">Data</div><input type=\"text\" name=\"data\"></div><div class=\"form-cell col-wid-3\"><div class=\"field-title\">Hora</div><input type=\"text\" name=\"horario\" placeholder=\"opcional\"></div><div class=\"form-space\"></div><div class=\"form-cell col-wid-4\"></div><div class=\"form-cell cancel col-wid-2\"><input type=\"button\" target=\"cancel\" value=\"Sair\"/></div><div class=\"form-cell col-wid-2\"><input type=\"button\" target=\"remove\" value=\"Excluir\"/></div><div class=\"form-cell col-wid-2\"><input type=\"button\" target=\"submit\" value=\"Salvar\"/></div></div>",
		init: (args, loaded, page) => {
			const {id, callback} = args;
			const select = page.find('select');
			const inputIdAluno = page.find('[name="idAluno"]');
			page.find('[target="cancel"]').bind('click', () => {
				killPage(page);
				if (callback) {
					callback();
				}
			});
			const loadCursos = () => new Promise((done, error) => {
				$.get({
					url: '/curso/listByIdAluno',
					data: { idAluno: inputIdAluno.val() },
					success: array => {
						select.html('');
						array.forEach(curso => {
							const {
								nome,
								modalidade,
								idMatricula
							} = curso;
							const option = $new('option');
							option.attr('value', idMatricula);
							option.html(domTxt(`${ nome } - ${ modalidade }`));
							select.append(option);
						});
						done();
					},
					error: err => {
						errorMsg('Erro interno');
						fail(err);
					}
				});
			});
			page.find('[target="alterar_aluno"]').bind('click', () => {
				openForm('aluno/selecionar', {
					callback: aluno => {
						if (!aluno) return;
						const {id, nome} = aluno;
						page.find('[name="nomeAluno"]').val(nome);
						inputIdAluno.val(id);
						ocupyWorkspace(loadCursos());
					}
				});
			});
			page.find('[target="remove"]').bind('click', () => {
				ocupyWorkspace(new Promise(done => {
					$.ajax({
						url: '/prova/remove',
						data: {id},
						type: 'POST',
						success: removed => {
							if (removed) {
								sendMsg('Agendamento excluído');
								done();
								killPage(page);
								if (callback) {
									callback();
								}
							} else {
								errorMsg('Erro ao exlcuir agendamento');
								done();
							}
						},
						error: () => {
							errorMsg('Erro ao exlcuir agendamento');
							done();
						}
					});
				}));
			});
			const send = obj => {
				ocupyWorkspace(new Promise(done => {
					$.ajax({
						type: 'POST',
						url: '/prova/update',
						data: obj,
						success: id => {
							sendMsg('Alterações salvas');
							killPage(page);
							done();
						},
						error: err => {
							errorMsg('Falha ao salvar alterações');
							done();
						}
					});
				}));
			};
			page.find('[target="submit"]').bind('click', () => {
				let {
					data,
					disciplina,
					horario,
					idAluno,
					semestre,
					idMatricula
				} = getData(page);
				if (!idAluno) {
					return errorMsg('Selecione o aluno');
				}
				if (!idMatricula) {
					return errorMsg('Selecione o curso');
				}
				semestre = semestre.trim();
				if (!semestre) {
					return errorMsg('Informe o semestre');
				}
				disciplina = disciplina.trim();
				if (!disciplina) {
					return errorMsg('Informe a disciplina');
				}
				data = data.trim();
				if (!data) {
					return errorMsg('Informe a data');
				}
				data = parseDate(data);
				if (!data) {
					return errorMsg('Data inválida');
				}
				horario = horario.trim();
				if (horario) {
					horario = parseTime(horario);
					if (!horario) {
						return errorMsg('Hora inválida');
					}
				}
				send({
					id,
					data,
					disciplina,
					horario,
					idAluno,
					semestre,
					idMatricula
				});
			});
			$.ajax({
				url: '/prova/get',
				data: {id},
				success: res => {
					res.data = new Date(res.data);
					if (res.horario) {
						let {data} = res;
						data = Util.dateSQL(data);
						data += ' ' + res.horario;
						data = new Date(data);
						let h = data.getHours();
						let m = data.getMinutes();
						res.horario = `${h>9?h:'0'+h}:${m>9?m:'0'+m}`;
					}
					let y = res.data.getFullYear();
					let M = res.data.getMonth() + 1;
					let d = res.data.getDate();
					res.data = `${d>9?d:'0'+d}/${M>9?M:'0'+M}/${y}`;
					inputIdAluno.val(res.idAluno);
					page.find('[name="nomeAluno"]').val(res.nomeAluno);
					page.find('[name="semestre"]').val(res.semestre);
					page.find('[name="disciplina"]').val(res.disciplina);
					page.find('[name="data"]').val(res.data);
					page.find('[name="horario"]').val(res.horario);
					loadCursos()
						.then(() => {
							select.val(res.idMatricula);
							loaded();
						})
						.catch(err => {
							killPage(page);
							loaded();
						});
				},
				error: err => {
					errorMsg('Erro interno');
					killPage(page);
					loaded();
				}
			});
		}
	}
,	"usuario/add": {
		html: "<div class=\"page-title\">Usuário</div><div class=\"form-space\"></div><div class=\"n-cols-8\"><div class=\"form-cell text col-wid-8\"><div class=\"field-title\">Nome completo</div><input type=\"text\" name=\"nome\" autocomplete=\"off\"/></div><div class=\"form-cell text col-wid-8\"><div class=\"field-title\">Login</div><input type=\"text\" name=\"login\" autocomplete=\"off\"/></div><div class=\"form-cell text col-wid-8\"><div class=\"field-title\">Senha</div><input type=\"password\" name=\"senha\" autocomplete=\"off\"/></div><div class=\"form-cell text col-wid-8\"><div class=\"field-title\">Confirmar senha</div><input type=\"password\" name=\"senha2\" autocomplete=\"off\"/></div><div class=\"form-space\"></div><div class=\"form-cell col-wid-4\"></div><div class=\"form-cell cancel col-wid-2\"><input type=\"button\" name=\"cancel\" value=\"Cancelar\"/></div><div class=\"form-cell col-wid-2\"><input type=\"button\" name=\"submit\" value=\"Salvar\"/></div></div>",
		init: (args, loaded, page) => {
			const superTrim = str => {
				str = str.trim();
				while (str.indexOf('  ') !== -1) str = str.replace('  ', ' ');
				return str;
			};
			const getData = () => {
				const data = {};
				page.find('[name]').each((i, e) => {
					e = $(e);
					const name = e.attr('name');
					data[name] = e.val();
				});
				return data;
			};
			page.find('input[name="nome"]').focus();
			page.find('[name="submit"]').bind('click', function(){
				const data = getData();
				const {senha, senha2} = data;
				const nome = superTrim(data.nome);
				const login = superTrim(data.login);
				if (!nome && !login) {
					errorMsg('Insira o nome de usuário e o login');
					return;
				}
				if (!nome) {
					errorMsg('Insira o nome de usuário');
					return;
				}
				if (!login) {
					errorMsg('Insira o login');
					return;
				}
				if (senha !== senha2) {
					errorMsg('As senhas não conferem');
					return;
				}
				ocupyWorkspace(new Promise(done => {
					$.post({url: '/usuario/add', data,
						success: id => {
							if (id) {
								sendMsg('Usuário cadastrado');
								killPage(page);
							} else {
								errorMsg('Falha ao realizar cadastro');
							}
							done();
						},
						error: err => {
							errorMsg('Erro interno');
							done();
						}
					})
				}));
			});
			page.find('[name="cancel"]').bind('click', function(){
				killPage(page);
			});
			loaded();
			
		}
	}
,	"usuario/list": {
		html: "<div class=\"page-title\">Nome</div><div class=\"form-space\"></div><div class=\"n-cols-12\"><div class=\"form-cell col-wid-12 pagination-table\"></div><div class=\"form-space\"></div><div class=\"form-cell col-wid-10\"></div><div class=\"form-cell cancel col-wid-2\"><input type=\"button\" target=\"sair\" value=\"Sair\"/></div></div>",
		init: (args, loaded, page) => {
			const itemToRow = item => {
				const row = $new('div').addClass('row').html(
					'<div class="cell"><div class="content"></div></div>' +
					'<div class="cell"><div class="content"></div></div>'
				);
				const contents = row.find('.content');
				contents.first().append(domTxt(item.nome));
				contents.last().append(domTxt(item.login));
				return row;
			};
			initPaginationTable(page.find('.pagination-table'), {
				url: '/usuario/list',
				cols: [
					{title: 'Nome', width: '1w'},
					{title: 'Login', width: '150px'}
				],
				itemToRow,
				success: loaded,
				error: () => {
					errorMsg('Erro interno');
					loaded();
				}
			});
			page.find('[target="sair"]').bind('click', () => {
				killPage(page);
			});
			
		}
	}
,	"usuario/selecionar": {
		html: "<div class=\"n-cols-8\"><div class=\"form-cell text col-wid-8\"><div class=\"field-title\">Nome do curso</div><input type=\"text\" name=\"nome\" autocomplete=\"off\"/></div><div class=\"form-cell selection-table col-wid-8 n-rows-5\"></div><div class=\"form-cell col-wid-6\"></div><div class=\"form-cell cancel col-wid-2\"><input type=\"button\" name=\"cancel\" value=\"Cancelar\"/></div></div>",
		init: (args, loaded, page) => {
			const {callback} = args;
			const inputNome = page.find('[name="nome"]');
			const inputCancel = page.find('[name="cancel"]');
			let lastSearch = '';
			let usuario = null;
			const {applyFilter, setList} = initSelectionTable(page.find('.selection-table'), {
				url: '/usuario/list',
				cols: [{title: 'Curso', width: '1w'}],
				toHTMLRow: item => `
					<div class="selection-table-row" tabindex="0">
						<div class="selection-table-cell"><div>${ item.nome }</div></div>
						<input type="hidden" name="id" value="${ item.id }"/>
						<input type="hidden" name="nome" value="${ item.nome }"/>
						<input type="hidden" name="login" value="${ item.login }"/>
					</div>
				`,
				success: () => {
					inputNome.focus();
					loaded();
				},
				error: _=> {
					errorMsg('Erro interno');
					loaded();
				}
			});
			const filter = items => {
				const res = [];
				const str = inputNome.val().trim().toLowerCase();
				while (str.indexOf('  ') !== -1) {
					str = str.replace('  ', ' ');
				}
				if (str === lastSearch) return false;
				if (!str) {
					lastSearch = '';
					return items.slice(0, 5);
				}
				for (let i=0; i<items.length && res.length < 5; ++i) {
					const item = items[i];
					if (noAccent(item.nome.toLowerCase()).indexOf(str) !== -1) {
						res.push(item);
					}
				}
				lastSearch = str;
				return res;
			};
			inputCancel.bind('click', function(){
				killPage(page);
				if (callback) callback(false);
			});
			inputNome.bind('keyup', function(){
				if (applyFilter) {
					applyFilter(filter);
				}
			});
			function selectRow(row) {
				const item = getData(row);
				if (!item.id) return;
				killPage(page);
				if (callback) callback(item);
			}
			page.on('click', '.selection-table-row', function() {
				selectRow($(this));
			});
			page.on('keydown', '.selection-table-row', function (e) {
				const key = e.key.toLowerCase();
				if (key === 'enter' || key === '\n' || key === ' ') {
					selectRow($(this));
				}
			});
			
		}
	}

};
setForms(forms);
export default forms;