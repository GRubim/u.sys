const PI = Math.PI;
const TAU = PI*2;
const DEF_SIZE = 64;
export default class Loading {
	constructor(arg) {
		if (arg) {
			if (typeof arg === 'number') {
				this.size = arg;
			} else if (arg.hasOwnProperty('size')) {
				this.size = arg.size;
			} else {
				this.size = DEF_SIZE;
			}
		} else {
			this.size = DEF_SIZE;
		}
		this.code = null;
		const canvas = document.createElement('canvas');
		this.canvas = canvas;
		canvas.width = this.size;
		canvas.height = this.size;
		this.ctx = canvas.getContext('2d');
	}
	stop() {
		const {code} = this;
		if (code !== null) {
			clearInterval(code);
			this.code = null;
		}
		return this;
	}
	start() {
		if (this.code !== null) return this;
		const {ctx, size, canvas} = this;
		if (size !== canvas.width) {
			canvas.width = canvas.height = size;
		}
		const c = size*0.5;
		const len = PI*0.75;
		const r1 = size*0.31, w1 = size*0.055, l1 = PI*0.70;
		const r2 = size*0.47, w2 = size*0.039, l2 = PI*0.75;
		ctx.strokeStyle = '#456';
		this.code = setInterval(() => {

			const t = (new Date() - 0)*0.006;
			let a1 = t*+1.25 % TAU;
			let a2 = t*-1.00 % TAU;
			a1 = (1 - Math.cos(a1/2))*PI;

			ctx.clearRect(0, 0, size, size);

			ctx.lineWidth = w1;
			ctx.beginPath();
			ctx.arc(c, c, r1, a1, a1 + l1);
			ctx.stroke();
			ctx.beginPath();
			ctx.arc(c, c, r1, a1 + PI, a1 + PI + l1);
			ctx.stroke();
			
			ctx.lineWidth = w2;
			ctx.beginPath();
			ctx.arc(c, c, r2, a2, a2 + l2);
			ctx.stroke();
			ctx.beginPath();
			ctx.arc(c, c, r2, a2 + PI, a2 + PI + l2);
			ctx.stroke();
		}, 0);
	}
	reset() {
		return this.stop().start();
	}
}