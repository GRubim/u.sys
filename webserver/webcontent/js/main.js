import {
	init,
	hideLeftBar,
	showLeftBar,
	setUserName,
	createPage,
	ocupyWorkspace,
	addMenuOption,
	updateMenuOptionTitle,
	currentPage,
	killPage,
	initSelectionTable,
	sendMsg,
	errorMsg,
	openForm
} from './page-control.js';

import {
	$new,
	dateTimeSQL
} from './util.js';

import Lembretes from './lembretes.js';

let currentUser = null;

const handleLogout = () => {
	let page;
	while ((page = currentPage()).length) {
		killPage(page);
	}
	currentUser = null;
	hideLeftBar();
	setUserName('');
	openForm('login', {callback: user => {
		currentUser = user;
		handleLogin();
	}});
	Lembretes.stop();
};
const handleLogin = () => {
	setUserName(currentUser.nome);
	showLeftBar();
	Lembretes.start();
};

addMenuOption({
	id: 'lembretes', title: 'Lembretes',
	onclick: () => openForm('lembrete/list')
});
addMenuOption({id: 'alunos', title: 'Alunos'});
addMenuOption({id: 'cursos', title: 'Cursos'});
addMenuOption({id: 'parcerias', title: 'Sites Parceiros'});
addMenuOption({id: 'usuarios', title: 'Usuários'});
addMenuOption({
	id: 'matriculas',
	title: 'Matrículas',
	onclick: () => openForm('colaborador/matriculas')
});
addMenuOption({ id: 'provas', title: 'Provas' });
addMenuOption({
	id: 'addProva',
	title: 'Agendar Prova',
	parentId: 'provas',
	onclick: () => openForm('prova/add')
});
addMenuOption({
	id: 'listProvas',
	title: 'Listar Agendamentos',
	parentId: 'provas',
	onclick: () => openForm('prova/list')
});
addMenuOption({
	id: 'cadastroAluno',
	title: 'Cadastrar Aluno',
	parentId: 'alunos',
	onclick: () => openForm('aluno/add', {usuario: currentUser})
});
addMenuOption({
	id: 'addUsuario',
	title: 'Cadastrar Usuario',
	parentId: 'usuarios',
	onclick: () => openForm('usuario/add')
});
addMenuOption({
	id: 'addSiteParceiro',
	title: 'Novo Site Parceiro',
	parentId: 'parcerias',
	onclick: () => openForm('colaborador/siteParceiro/add')
});
addMenuOption({
	id: 'addCurso',
	title: 'Novo Curso',
	parentId: 'cursos',
	onclick: () => openForm('curso/add')
});
addMenuOption({
	id: 'listCursos',
	title: 'Listar Cursos',
	parentId: 'cursos',
	onclick: () => openForm('curso/list')
});
addMenuOption({
	id: 'listUsuarios',
	title: 'Listar Usuarios',
	parentId: 'usuarios',
	onclick: () => openForm('usuario/list')
});
addMenuOption({
	id: 'buscarAluno',
	title: 'Buscar Aluno',
	parentId: 'alunos',
	onclick: () => {
		const page = openForm('aluno/selecionar', {
			callback: aluno => {
				if (aluno) {
					openForm('aluno/manter', {
						id: aluno.id,
						usuario: currentUser
					});
				}
			}
		});
	}
});
addMenuOption({
	id: 'buscarCurso',
	title: 'Buscar Curso',
	parentId: 'cursos',
	onclick: () => {
		const page = openForm('curso/selecionar', {
			callback: aluno => {
				if (aluno) {
					openForm('curso/manter', {
						id: aluno.id,
						usuario: currentUser
					});
				}
			}
		});
	}
});
addMenuOption({
	id: 'interessados',
	title: 'Alunos em Negociação',
	parentId: 'alunos',
	onclick: () => openForm('aluno/interessados', {
		usuario: currentUser
	})
});
addMenuOption({
	id: 'agendamentoAva',
	title: 'Agendamentos AVA',
	onclick: () => openForm('agendamentoAva/list', {
		usuario: currentUser
	})
});

init(() => {
	$('#logout span').bind('click', ()=>{
		$.post({
			url: '/logout',
			success: res => {
				handleLogout();
			},
			error: res => {
				errorMsg('Erro interno');
			}
		});
	});
	ocupyWorkspace(new Promise(done => {
		$.get({
			url: '/current_user',
			success: user => {
				currentUser = user;
				if (!user) {
					handleLogout();
				} else {
					handleLogin();
				}
				done();
			}
		});
	}));
	$('body').on('click', '.aluno-link', function(){
		const id = $(this).attr('id-aluno');
		openForm('aluno/manter', {id, usuario: currentUser});
	});
	Lembretes.onupdate(n => {
		updateMenuOptionTitle('lembretes', `Lembretes: ${n}`);
	});
});