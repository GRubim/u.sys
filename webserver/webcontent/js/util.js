export const syncAjax = ({get, post, success, error}) => {
	var counter = 0;
	var failed = false;
	const array = [];
	const done = success;
	const fail = error;
	const addCalls = (calls, method) => calls.forEach(call => {
		const {url, data, success, error} = call;
		++ counter;
		array.push($.ajax({
			url, data, method,
			success: res => {
				if (failed === true) return;
				if (success) success(res);
				if (--counter === 0 && done) done();
			},
			error: err => {
				if (failed === true) return;
				failed = true;
				array.forEach(obj => obj.abort());
				if (error) error(err);
				if (fail) fail();
			}
		}));
	});
	if (get) addCalls(get, 'GET');
	if (post) addCalls(post, 'POST');
};
export const $new = tagName => $(document.createElement(tagName));
export const domTxt = text => document.createTextNode(text);
export const getData = element => {
	const data = {};
	element.find('[name]').each(function(){
		const item = $(this);
		data[item.attr('name')] = item.val();
	});
	return data;
};
export const formatSpaces = str => {
	str = str.trim();
	while (str.indexOf('  ') !== -1) str = str.replace('  ', ' ');
	return str;
};
export const noAccent = str => str.normalize('NFD').replace(/[\u0300-\u036f]/g, "");
export const parseDate = date => {
	date = date.trim().split('/');
	if (date.length !== 3) return false;
	let d = parseInt(date[0]);
	let m = parseInt(date[1]) - 1;
	let y = parseInt(date[2]);
	date = new Date(y, m, d);
	if (date.getDate() !== d || date.getMonth() !== m || date.getFullYear() !== y) return false;
	m ++;
	y = y.toString();
	return `${'0'.repeat(4-y.length)+y}-${m<10?'0'+m:m}-${d<10?'0'+d:d}`;
};
export const parseTime = time => {
	time = time.trim().split(':');
	if (time.length !== 2) return false;
	let h = parseInt(time[0]);
	let m = parseInt(time[1]);
	if (!h && h !== 0) return false;
	if (!m && m !== 0) return false;
	if (h >= 24 || h < 0 || m >= 60 || m < 0) return false;
	return `${h<10?'0'+h:h}:${m<10?'0'+m:m}:00`;
};
export const dateSQL = date => {
	const y = date.getFullYear();
	const M = date.getMonth() + 1;
	const d = date.getDate();
	return y+`-${M<10?'0'+M:M}-${d<10?'0'+d:d}`;
};
export const timeSQL = date => {
	const h = date.getHours();
	const m = date.getMinutes();
	const s = date.getSeconds();
	return `${h<10?'0'+h:h}:${m<10?'0'+m:m}:${s<10?'0'+s:s}`;
};
export const dateTimeSQL = date => {
	return dateSQL(date) + ' ' + timeSQL(date);
};
export const replaceAll = (str, a, b) => {
	while (str.indexOf(a) !== -1) {
		str = str.replace(a, b);
	}
	return str;
};
export const dateTimeBR = date => {
	if (!(date instanceof Date)) date = new Date(date);
	const y = date.getFullYear();
	const M = date.getMonth() + 1;
	const d = date.getDate();
	const h = date.getHours();
	const m = date.getMinutes();
	return `${d>9?d:'0'+d}/${M>9?M:'0'+M}/${y} ${h>9?h:'0'+h}:${m>9?m:'0'+m}`;
};