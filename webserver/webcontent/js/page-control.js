import Loading from './loading.js';

const Y_ALIGN = 0.45;
const messages = [];
const msg_y0 = 20;
const msg_delta = 46;
const msgRiseDelay = 500;
const msgMoveDelay = 250;
const msgFadeDelay = 250;
const msgTimeAlive = 5000;
const readyHandlers = [];
const menuStruct = {':map': {}, ':root': []};

let ready = false;
let body = null;
let leftbar = null;
let workspace = null;
let goBackButton = null;
let menuOptions = null;
let workspaceWidth = null;
let workspaceHeight = null;
let currentMenuOption = null;
let forms = {};

const $new = tagName => $(document.createElement(tagName));
const parsePx = x => parseInt(x.toString().replace('px', ''));
const parseW = x => parseInt(x.toString().replace('w', ''));
const domTxt = str => document.createTextNode(str);

const measureWorkspace = () => {
	let width = parsePx(workspace.css('width'));
	let height = parsePx(workspace.css('height'));
	if (width !== workspaceWidth || height !== workspaceHeight) {
		workspaceWidth = width;
		workspaceHeight = height;
		return true;
	}
	return false;
};

const centerElements = () => {
	workspace.children().each((i, e) => {
		centerElement($(e).children());
	});
};

const handleResize = () => {
	measureWorkspace();
	centerElements();
};

const centerElement = element => {
	let width = parsePx(element.css('width'));
	let height = parsePx(element.css('height'));
	element.css({
		top: Math.floor((workspaceHeight - height)*Y_ALIGN) + 'px',
		left: Math.floor((workspaceWidth - width)*0.5) + 'px',
	});
};

const createTab = content => {
	const tab = $new('div').addClass('workspace-tab').append(content);
	tab.css({opacity: 0});
	workspace.append(tab);
	setTimeout(() => {
		centerElement(tab.children());
		tab.css({opacity: 1});
	}, 0);
	return tab;
};

const deleteTab = tab => {
	tab.remove();
};

const handleMenuOptionClick = option => {
	if (option.onclick) {
		option.onclick();
	}
	if (option.length) {
		openMenuOption(option);
	}
};

const openMenuOption = option => {
	currentMenuOption = option;
	menuOptions.html('');
	if (option.parent) {
		menuOptions.append(goBackButton);
	} else {
		goBackButton.remove();
	}
	option.forEach(item => {
		menuOptions.append(item.element);
	});
};

const compressHTML = str => {
	let res = '';
	str.split('\n').forEach(line => res += line.trim());
	return res;
};

$(document).ready(() => {

	body = $('body');
	leftbar = $('#leftbar');
	workspace = $('#workspace');
	menuOptions = $('#leftbar .menu-options');
	goBackButton = $new('div').addClass('menu-option goback')
	goBackButton.append($new('div').addClass('fas fa-chevron-left'));
	goBackButton.append(domTxt('Voltar'));
	goBackButton[0].addEventListener('click', _=>openMenuOption(currentMenuOption.parent));
	openMenuOption(menuStruct[':root']);

	const handlePinwrapClick = e => {
		const cell = $(e).closest('.form-cell');
		const input = cell.find('input');
		cell.toggleClass('switch-on');
		if (input.length) {
			input.val(cell.hasClass('switch-on'));
			input.trigger('change');
		}
	}
	workspace.on('click', '.pin-wrap', function(){
		handlePinwrapClick(this);
	});
	workspace.on('keydown', '.pin-wrap', function(e) {
		const key = e.key.toLowerCase();
		if (key === ' ' || key === '\n' || key === 'enter') handlePinwrapClick(this);
	});

	handleResize();
	$(window).bind('resize', handleResize);

	ready = true;
	while (readyHandlers.length) readyHandlers.splice(0, 1)[0]();
});

export const init = handler => {
	if (ready === true && readyHandlers.length === 0) {
		handler();
	} else {
		readyHandlers.push(handler);
	}
};

export const hideLeftBar = () => {
	body.addClass('leftbar-hidden');
	handleResize();
};

export const showLeftBar = () => {
	body.removeClass('leftbar-hidden');
	handleResize();
};

export const setUserName = name => {
	$('#username').html(name ? domTxt(name) : '');
};

export const createPage = content => {
	const page = $new('div').addClass('tab-page').append(content).wrap('<div></div>');
	createTab(page.parent().addClass('tab-page-wrap'));
	return page;
};

export const currentPage = () => {
	return workspace.find('.tab-page').last();
};

export const killPage = page => {
	deleteTab(page.closest('div.workspace-tab'));
};

export const ocupyWorkspace = promise => {
	const loading = new Loading();
	loading.start();
	const canvas = $(loading.canvas).css({position: 'absolute'});
	const tab = createTab(canvas);
	tab.css({opacity: 0}).animate({opacity: 1}, 250);
	const end = () => deleteTab(tab);
	promise.then(end).catch(end);
};

export const setForms = formMap => {
	forms = formMap;
};

export const openForm = (formName, args) => {
	const form = forms[formName];
	if (!form) {
		throw `Form ${formName} not found`;
	}
	const page = createPage(form.html);
	ocupyWorkspace(new Promise(loaded => {
		try {
			form.init(args || {}, loaded, page);
		} catch (e) {
			console.error(e);
		}
	}));
	return page;
};

export const addMenuOption = ({id, title, parentId, onclick}) => {
	const map  = menuStruct[':map'];
	const root = menuStruct[':root'];
	const parent = !parentId ? root : map[parentId];
	const option = [];
	const element = $new('div').addClass('menu-option').append(domTxt(title));
	element[0].addEventListener('click', _=>handleMenuOptionClick(option));
	option.title = title;
	option.parent = parent;
	option.onclick = onclick;
	option.element = element;
	map[id] = option;
	parent.push(option);
};

export const updateMenuOptionTitle = (id, newTitle) => {
	const map = menuStruct[':map'];
	const option = map[id];
	option.title = newTitle;
	option.element.html(domTxt(newTitle));
};

export const initSelectionTable = (table, args) => {
	const {url, data, success, error, cols, toHTMLRow} = args;
	const head = $new('div').addClass('selection-table-head');
	const body = $new('div').addClass('selection-table-body');
	head.append('<div class="selection-table-row"></div>')
	table.append([head, body]);
	let resultList = [];
	let fullWidth = null;
	let widths = null;
	const calcWidths = () => {
		fullWidth = parsePx(body.css('width'));
		let totalP = 0;
		let totalW = 0;
		let vp = [];
		let vw = [];
		cols.forEach((item, i) => {
			let {width} = item;
			vp[i] = 0;
			vw[i] = 0;
			if (width.indexOf('px') !== -1) {
				const val = parsePx(width);
				totalP += val;
				vp[i] = val;
			} else if (width.indexOf('w') !== -1) {
				const val = parseW(width);
				totalW += val;
				vw[i] = val;
			}
		});
		widths = new Array(cols.length).fill(0);
		let mul = (fullWidth - totalP)/totalW;
		cols.forEach((col, i) => {
			widths[i] = vp[i] + vw[i]*mul;
		});
	};
	const formatHeader = () => {
		head.children().children().each((i, e) => {
			$(e).css({width: widths[i] + 'px'});
		});
	};
	const getElement = item => {
		let element = item.element;
		if (!element) {
			const temp = $new('div');
			temp.html(compressHTML(toHTMLRow(item)));
			element = item.element = temp.children();
			element.children().each((i, cell) => {
				$(cell).css({width: widths[i]});
			})
		}
		return element;
	};
	const placeElements = array => {
		body.html('');
		array.forEach(item => body.append(getElement(item)));
	};
	cols.forEach(item => {
		const element = $new('div').addClass('selection-table-cell');
		element.html('<div></div>');
		element.children().append(domTxt(item.title));
		head.children().append(element);
	});
	const setList = array => {
		calcWidths(table);
		formatHeader();
		placeElements(array.slice(0, 5));
		resultList = array
	};
	$.get({url, data, error,
		success: array => {
			setList(array);
			success();
		}
	});
	return {
		applyFilter: filter => {
			const elements = filter(resultList);
			if (elements) {
				placeElements(elements);
			}
		},
		setList
	}
};

export const initPaginationTable = (element, args) => {
	const {url, data, cols, itemToRow, success, error} = args;
	const widths = [];
	const headerCells = [];
	const pageSize = 9;
	let index = 0;
	let head = null;
	let body = null;
	let prevButton = null;
	let nextButton = null;
	let textInfo = null;
	let resultList = [];
	const createHTMLContent = () => {
		let html =
			'<div class="table-content">' +
				'<div class="head">' +
					'<div class="row"></div>' +
				'</div>' +
				'<div class="body">' +
				'</div>' +
			'</div>' +
			'<div class="pages">' +
				'<div class="button disabled">' +
					'<span class="fas fa-angle-left"></span>' +
				'</div>' +
				'<div class="text-info"></div>' +
				'<div class="button disabled">' +
					'<span class="fas fa-angle-right"></span>' +
				'</div>' +
			'</div>';
		element.html(html);
		const row = element.find('.head .row');
		cols.forEach(({title}) => {
			const cell = $new('div').addClass('cell');
			const content = $new('div').addClass('content');
			row.append(cell);
			cell.append(content);
			content.append(domTxt(title));
			headerCells.push(cell);
		});
		head = element.find('.head');
		body = element.find('.body');
		textInfo = element.find('.text-info');
		prevButton = element.find('.button').first();
		nextButton = element.find('.button').last();
		nextButton.bind('click', () => {
			if (index + pageSize < resultList.length) {
				index += pageSize;
				updateCurrentPage();
			}
		});
		prevButton.bind('click', () => {
			if (index > 0) {
				index -= pageSize;
				updateCurrentPage();
			}
		});
	};
	const sizeHeader = () => {
		headerCells.forEach((cell, col) => {
			cell.css({ width: widths[col] + 'px' });
		});
	};
	const calcWidths = () => {
		let tableWidth = parsePx(element.find('.table-content').css('width'));
		let sumPixels = 0;
		let sumWeight = 0;
		let pixels = [];
		let weight = [];
		cols.forEach((col, i) => {
			const str = col.width;
			let pVal = 0;
			let wVal = 0;
			if (str.indexOf('px') !== -1) {
				pVal = parsePx(str);
			} else if (str.indexOf('w') !== -1) {
				wVal = parseInt(str.replace('w', ''));
			}
			sumPixels += pixels[i] = pVal;
			sumWeight += weight[i] = wVal;
		});
		const mul = (tableWidth - sumPixels)/sumWeight;
		cols.forEach((col, i) => {
			widths[i] = pixels[i] + weight[i]*mul;
		});
	};
	const getDOMRow = item => {
		let row = item.row;
		if (!row) {
			row = itemToRow(item);
			row.find('.cell').each((i, cell) => {
				$(cell).css({width: widths[i]});
			});
		}
		return row;
	};
	const updateCurrentPage = () => {
		body.html('');
		const n = Math.min(resultList.length - index, pageSize);
		for (let i=0; i<n; ++i) {
			const item = resultList[index + i];
			body.append(getDOMRow(item));
		}
		const a = index + 1;
		const b = index + n;
		const c = resultList.length;
		textInfo.html(`${a} - ${b} de ${c}`);
		if (a > 1) {
			prevButton.removeClass('disabled');
		} else {
			prevButton.addClass('disabled');
		}
		if (b < c) {
			nextButton.removeClass('disabled');
		} else {
			nextButton.addClass('disabled');
		}
	};
	createHTMLContent();
	calcWidths();
	sizeHeader();
	if (!url) {
		updateCurrentPage();
	} else {
		$.get({
			url, data,
			success: array => {
				resultList = array;
				updateCurrentPage();
				if (success) success();
			},
			error: err => {
				updateCurrentPage();
				if (error) error(err);
			}
		});
	}
	return {
		reload: ({data}) => new Promise((done, fail) => {
			$.get({
				url, data,
				success: array => {
					resultList = array;
					updateCurrentPage();
					done();
				},
				error: err => {
					fail();
				}
			});
		})
	};
};

export const sendMsg = (text, type) => {
	const element = $new('div').addClass('wrap-msg');
	const content = $new('div').addClass('msg-content');
	const closeButton = $new('div').addClass('msg-close-button');
	content.append(domTxt(text));
	closeButton.append('<span class="fas fa-times"></span>');
	element.append(content).append(closeButton);
	if (type === 'error') {
		element.addClass('error-msg');
	}
	for (let i=0; i<messages.length; ++i) {
		const {element, state} = messages[i];
		if (state === 'fading') break;
		element.finish().animate({bottom: `+=${msg_delta}px`}, msgMoveDelay);
	}
	const msgObj = {
		element,
		state: 'rising',
	};
	element.css({
		left: '20px',
		opacity: 0,
		bottom: msg_y0 + 'px'
	});
	element.animate({
		opacity: 1,
	}, msgRiseDelay, () => {
		msgObj.state = 'await'
	});
	messages.splice(0, 0, msgObj);
	workspace.append(element);
	const die = () => {
		if (msgObj.state === 'fading') return;
		msgObj.state = 'fading';
		msgObj.element.animate({
			opacity: 0
		}, () => {
			const index = messages.indexOf(msgObj);
			for (let i=index; i<messages.length; ++i) {
				messages[i].element.finish().animate({ bottom: `-=${ msg_delta }px` }, msgMoveDelay);
			}
			messages.splice(index, 1);
			msgObj.element.remove();
		});
	};
	setTimeout(die, msgTimeAlive);
	closeButton.bind('click', die);
	return text;
};

export const errorMsg = text => {
	sendMsg(text, 'error');
};