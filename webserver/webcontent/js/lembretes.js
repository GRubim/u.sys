import {
	openForm
} from './page-control.js';

import {
	dateTimeSQL
} from './util.js';

const url = '/lembrete/ate';
const interval = 30000;

let timeoutArray = [];
let checkPoint = null;
let ajaxCall = null;
let timeout = null;
let running = false;
let preventIdMaps = null;
let updateHandler = null;
let total = null;
let lastArray = [];

const countLastArray = now => {
	const prev = total;
	total = 0;
	lastArray.forEach(obj => total += new Date(obj.horario) <= now);
	if (total !== prev && updateHandler) {
		updateHandler(total);
	}
};
const resetPrevents = () => {
	preventIdMaps = [{}, {}];
};
const preventId = id => {
	preventIdMaps[0][id] = true;
};
const idAllowed = id => {
	const [map1, map2] = preventIdMaps;
	return !map1[id] && !map1[id];
};
const preventsAdvance = () => {
	preventIdMaps[1] = preventIdMaps[0];
	preventIdMaps[0] = {};
};
const cancelLembretes = () => {
	timeoutArray.forEach(timeout => clearTimeout(timeout));
	timeoutArray = [];
};
const notificar = id => {
	if (!idAllowed(id)) return;
	openForm('lembrete/notificar', {id});
	preventId(id);
	if (updateHandler) updateHandler(++total);
	countLastArray(new Date());
};
const makeCall = ({success, error}) => $.ajax({
	url,
	method: 'GET',
	data: {max: dateTimeSQL(new Date(new Date()*1 + interval*100))},
	success: success,
	error
});
const makeUpdateCall = () => {
	const next = () => {
		ajaxCall = null;
		timeout = setTimeout(() => {
			timeout = null;
			makeUpdateCall();
		}, interval);
	};
	ajaxCall = makeCall({
		error: next,
		success: array => {
			cancelLembretes();
			const now = new Date();
			array.forEach(obj => {
				const {id, horario} = obj;
				const time = new Date(horario);
				if (time > now) {
					const delay = time - now;
					const timeout = setTimeout(() => {
						notificar(id);
					}, delay);
					timeoutArray.push(timeout);
				} else if (time > checkPoint) {
					notificar(id);
				}
			});
			lastArray = array;
			checkPoint = now;
			countLastArray(now);
			preventsAdvance();
			next();
		}
	});
};
const start = () => {
	if (running) return;
	running = true;
	checkPoint = new Date();
	total = null;
	resetPrevents();
	makeUpdateCall();
};
const stop = () => {
	if (!running) return;
	running = false;
	if (timeout) clearTimeout(timeout);
	if (ajaxCall) ajaxCall.abort();
	cancelLembretes();
	lastArray = [];
	total = null;
};
const count = () => new Promise(done => makeCall({
	success: array => {
		lastArray = array;
		countLastArray(new Date());
	},
	error: done
}));
const onupdate = handler => updateHandler = handler;

export default {start, stop, onupdate, count};
