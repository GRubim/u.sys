import * as Util from './util.js';
import * as PageControl from './page-control.js';

import {
	ocupyWorkspace,
	killPage,
	openForm,
	setForms,
	sendMsg,
	errorMsg,
	initSelectionTable,
	initPaginationTable
} from './page-control.js';
import {
	syncAjax,
	$new,
	domTxt,
	getData,
	noAccent,
	formatSpaces,
	parseTime,
	parseDate
} from './util.js';
import Lembretes from './lembretes.js';

const forms = {
/*{{TARGET}}*/
};
setForms(forms);
export default forms;