const fs = require('fs');
const loadFileStr = fileName => {
	try {
		return fs.readFileSync(fileName).toString();
	} catch(err) {
		return null;
	}
};
const js = loadFileStr(__dirname + '/update.js');
if (js) {
	const str = JSON.stringify({id: 1, js});
	fs.writeFileSync(__dirname + '/../update.json', str);
	fs.writeFileSync(__dirname + '/../last-update.json', JSON.stringify({id: 0, js: ''}));
	console.log('done');
} else {
	console.log('fail');
}
