const query = "SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0; SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0; SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION'; CREATE TABLE IF NOT EXISTS `usys`.`AgendamentoProva` ( `id` INT NOT NULL AUTO_INCREMENT, `semestre` INT NOT NULL, `disciplina` VARCHAR(45) NOT NULL, `data` DATE NOT NULL, `horario` TIME NULL, `idMatricula` INT NOT NULL, `idUsuario` INT NOT NULL, PRIMARY KEY (`id`), UNIQUE INDEX `id_UNIQUE` (`id` ASC), INDEX `fk_AgendamentoProva_Matricula1_idx` (`idMatricula` ASC), INDEX `fk_AgendamentoProva_Usuario1_idx` (`idUsuario` ASC), CONSTRAINT `fk_AgendamentoProva_Matricula1` FOREIGN KEY (`idMatricula`) REFERENCES `usys`.`Matricula` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION, CONSTRAINT `fk_AgendamentoProva_Usuario1` FOREIGN KEY (`idUsuario`) REFERENCES `usys`.`Usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION) ENGINE = InnoDB; SET SQL_MODE=@OLD_SQL_MODE; SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS; SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;";
const fs = require('fs');
const exec = require('child_process').execSync;
fs.writeFileSync('temp.sql', query);
exec('mysql -u root -padmin123 < temp.sql');
exec('del /S /Q temp.sql');
updateDone();
