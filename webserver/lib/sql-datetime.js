const dateSQL = date => {
	const y = date.getFullYear();
	const M = date.getMonth() + 1;
	const d = date.getDate();
	return y+`-${M<10?'0'+M:M}-${d<10?'0'+d:d}`;
};

const timeSQL = date => {
	const h = date.getHours();
	const m = date.getMinutes();
	const s = date.getSeconds();
	return `${h<10?'0'+h:h}:${m<10?'0'+m:m}:${s<10?'0'+s:s}`;
};

const dateTimeSQL = date => {
	return dateSQL(date) + ' ' + timeSQL(date);
};

module.exports.now = () => {
	return dateTimeSQL(new Date());
};

module.exports.dateNow = () => {
	return dateSQL(new Date());
};

module.exports.timeNow = () => {
	return timeSQL(new Date());
};
