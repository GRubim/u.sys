const mariadb = require('mariadb');
const pool = mariadb.createPool({
	host: 'localhost', 
	user: 'root', 
	password: 'admin123',
	database: 'usys',
	connectionLimit: 20
});
module.exports = filename => new Promise((done, fail) => {
	let open = false;
	pool.getConnection()
		.then(conn => {
			open = true;
			const obj = {
				conn,
				query: (a, b, c) => conn.query(a, b, c),
				end: () => {
					open = false;
					conn.end();
				}
			};
			setTimeout(() => {
				if (open) {
					console.error('Connection not closed from ' + filename);
					conn.end();
				}
			}, 5000);
			done(obj);
		})
		.catch(error => fail(error));
});
