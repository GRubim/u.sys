const parseCookie = cookie => {
	const result = {};
	if (!cookie) {
		return result;
	}
	const attributes = cookie.split("; ");
	attributes.forEach(attr => {
		const [name, value] = attr.split("=");
		result[name] = value;
	});
	return result;
};
class SessionManager {
	constructor(args) {
		this.last_id = 0;
		this.size = 0;
		this.maxSize = args.maxSize || 100;
		this.maxAge = args.maxAge || 1000*60*60*24;
		this.sessionMap = {};
	}
	destroy(session) {
		const {sessionId} = session;
		const {sessionMap} = this;
		if (!sessionMap[sessionId]) {
			return this;
		}
		-- this.size;
		delete sessionMap[sessionId];
		return this;
	}
	renew(session) {
		const {maxAge} = this;
		clearTimeout(session.death);
		session.death = setTimeout(() => {
			this.destroy(session);
		}, maxAge);
		return this;
	}
	create() {
		const {size, maxSize, maxAge, sessionMap} = this;
		if (size >= maxSize) {
			return null;
		}
		let sessionId = ++ this.last_id + "-";
		for (let i=0; i<32; ++i) {
			sessionId += Math.floor(Math.random()*16).toString(16);
		}
		const death = setTimeout(() => {
			this.destroy(session);
		}, maxAge);
		const session = {
			sessionId, death,
			destroy: () => this.destroy(session),
			renew: () => this.renew(session)
		};
		sessionMap[session.sessionId] = session;
		++ this.size;
		return session;
	}
	find(sessionId) {
		return this.sessionMap[sessionId] || null;
	}
}
module.exports = args => {
	const env = new SessionManager(args);
	return (req, res, next) => {
		let session = null;
		let sessionId = null;
		const cookie = req.headers.cookie;
		if (cookie) {
			sessionId = parseCookie(cookie).JSESSIONID;
		}
		if (sessionId !== null) {
			session = env.find(sessionId);
		}
		if (session === null) {
			session = env.create();
			if (session !== null) {
				res.setHeader("Set-Cookie", "JSESSIONID=" + session.sessionId);
			}
		} else {
			session.renew();
		}
		if (session !== null) {
			res.session = req.session = session;
		}
		next();
	};
};
