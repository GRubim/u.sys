const sha1 = require("sha1");
module.exports.hash = (password) => {
	let salt = "";
	for (let i=0; i<16; ++i) {
		salt += Math.floor(Math.random()*16).toString(16);
	}
	return {salt, hash: sha1(salt + password)};
};
module.exports.check = (password, hash, salt) => {
	return sha1(salt + password) === hash;
};
