// Modo desenvolvimento
let DEV = false;
process.argv.forEach(arg => {
	arg = arg.trim().toLowerCase();
	if (arg === 'dev') DEV = true;
});

// Dependências nativas
const fs = require('fs');
const os = require('os');
const dns = require('dns');

// Dependências npm
const mime = require('mime-types');
const bodyParser = require('body-parser');
const express = require('express');

// Dependências locais
const session = require('./lib/session');

// Constantes de configuração
const PORT = 80;

const loadFile = fileName => {
	try {
		return fs.readFileSync(fileName);
	} catch(err) {
		return null;
	}
};
const loadFileStr = fileName => {
	try {
		return fs.readFileSync(fileName).toString();
	} catch(err) {
		return null;
	}
};
const loadFileJson = fileName => {
	try {
		return JSON.parse(fs.readFileSync(fileName).toString());
	} catch(err) {
		return null;
	}
};
const compressHTML = str => {
	let res = '';
	str.split('\n').forEach(line => res += line.trim());
	return res;
};
const checkUpdate = () => {
	const updatePath = __dirname + '/update.json';
	const lastUpdatePath = __dirname + '/last-update.json';
	let update = loadFileJson(updatePath);
	if (!update) {
		return;
	}
	let lastUpdate = loadFileJson(lastUpdatePath);
	if (!lastUpdate) {
		lastUpdate = {id: 0, js: ''};
	}
	if (update.id <= lastUpdate.id) {
		return;
	}
	let concluded = false;
	console.log('Atualização encontrada');
	try {
		let f;
		eval(`f = updateDone => {${ update.js }};`);
		f(() => concluded = true);
	} catch (error) {
		console.log(error);
	}
	if (concluded) {
		fs.writeFileSync(lastUpdatePath, JSON.stringify(update));
		console.log('Atualização concluída');
	} else {
		console.log('Atualização incompleta');
	}
};

checkUpdate();
const app = express();

app.use(bodyParser.urlencoded({extended: false}));
app.use(session({}));
app.use((req, res, next) => {
	if (DEV && !req.session.user) {
		req.session.user = {
			id: 1,
			nome: 'Administrador',
			login: 'admin',
			hash: 'bdc9632a513d3065a83b0b10dc0b2f533eaf2f5a',
			salt: '1bd4fac6385d00e9'
		};
	}
	req.notLogged = () => {
		if (req.session.user) return false;
		res.error(403);
		return true;
	};
	res.sendJson = obj => {
		const json = JSON.stringify(obj);
		res.setHeader('Content-Type', mime.lookup('file.json'));
		res.write(json);
		res.end();
	};
	res.error = function(code) {
		if (arguments.length > 1) console.log(arguments[1]);
		res.statusCode = code;
		res.end();
	};
	next();
});

// Execução dos scripts de rotas
fs.readdirSync(__dirname + '/src').forEach(name => {
	const routing = require(`./src/${ name }/routing`);
	routing(app);
});

// Roteamento webcontent
app.get('/index.html', (req, res) => {
	res.redirect('/');
});
app.get('/', (req, res) => {
	res.setHeader('Content-Type', 'text/html');
	res.write(loadFile(`${__dirname}/webcontent/index.html`));
	res.end();
});

// Compilação dos formulários
const addTab = (fileString, n) => {
	const tab = '\t'.repeat(n);
	return tab + fileString.split('\n').join('\n' + tab);
};
const getForms = () => {
	const formsJs = loadFileStr(__dirname + '/webcontent/js/-forms.js');
	const root = __dirname + '/webcontent/forms/';
	let content = '';
	const exploreDir = path => {
		const currPath = root + path;
		const html = loadFileStr(currPath + '/index.html');
		if (html) {
			const script = loadFileStr(currPath + '/script.js');
			let formName = path;
			if (content) content += ',';
			while (formName[0] === '/') formName = formName.substr(1);
			content += '\t' + JSON.stringify(formName) + ': {\n';
			content += '\t\thtml: ' + JSON.stringify(compressHTML(html)) + ',\n';
			content += '\t\tinit: (args, loaded, page) => {\n';
			content += addTab(script, 3) + '\n';
			content += '\t\t}\n';
			content += '\t}\n';
		}
		fs.readdirSync(currPath).forEach(name => {
			const newPath = path + '/' + name;
			if (fs.lstatSync(root + newPath).isDirectory()) {
				exploreDir(newPath);
			}
		});
	};
	exploreDir('');
	return formsJs.replace('/*{{TARGET}}*/', content);
};

if (DEV) {
	app.get('/js/forms.js', (req, res) => {
		res.setHeader('Content-Type', mime.lookup('forms.js'));
		res.write(getForms());
		res.end();
	});
}

// Auxiliar para testes de roteamento
app.get('/test', (req, res) => {
	res.setHeader('Content-Type', 'plain/html');
	res.end('[[OK]]');
});

// Roteamento de arquivos diretos no webcontent
app.use((req, res) => {
	const {path} = req;
	const filePath = `${__dirname}/webcontent${path}`;
	const file = loadFile(filePath);
	if (file === null) {
		res.statusCode = 404;
		res.end();
	} else {
		res.statusCode = 200;
		res.setHeader('Content-Type', mime.lookup(path));
		res.write(file);
		res.end();
	}
});

app.listen(PORT);
console.log('Server started - ' + new Date());
console.log('PORT: ' + PORT);

dns.lookup(os.hostname(), (err, add, fam) => {
	console.log('IP: ' + add);
});
