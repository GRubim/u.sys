CREATE TABLE IF NOT EXISTS AgendamentoAva (
	id INT NOT NULL AUTO_INCREMENT,
	idMatricula INT NOT NULL,
	data DATETIME NOT NULL,
	PRIMARY KEY (id),
	UNIQUE INDEX id_UNIQUE (id ASC),
	INDEX fk_AgendamentoAva_Matricula1_idx (idMatricula ASC),
	CONSTRAINT fk_AgendamentoAva_Matricula1
		FOREIGN KEY (idMatricula)
		REFERENCES Matricula (id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
);

CREATE TABLE IF NOT EXISTS ComentarioAva (
	id INT NOT NULL AUTO_INCREMENT,
	data DATETIME NOT NULL,
	texto TEXT(1023) NOT NULL,
	idUsuario INT NOT NULL,
	idAgendamento INT NOT NULL,
	PRIMARY KEY (id),
	UNIQUE INDEX id_UNIQUE (id ASC),
	INDEX fk_ComentarioAva_Usuario1_idx (idUsuario ASC),
	INDEX fk_ComentarioAva_AgendamentoAva1_idx (idAgendamento ASC),
	CONSTRAINT fk_ComentarioAva_Usuario1
		FOREIGN KEY (idUsuario)
		REFERENCES Usuario (id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT fk_ComentarioAva_AgendamentoAva1
		FOREIGN KEY (idAgendamento)
		REFERENCES AgendamentoAva (id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
);