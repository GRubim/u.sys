const connect = require('../lib/db-conn');
const query1 = `
	INSERT INTO Curso (
		id, nome
	) VALUES
		(1, 'Administração'),
		(2, 'Análise e Desenvolvimento de Sistemas'),
		(3, 'Artes Visuais'),
		(4, 'Ciências Contábeis'),
		(5, 'Ciências Econômicas'),
		(6, 'Comércio Exterior'),
		(7, 'Educação Física - Bacharel'),
		(8, 'Educação Física - Licenciatura'),
		(9, 'Empreendedorismo'),
		(10, 'Filosofia'),
		(11, 'Geografia'),
		(12, 'Gestão Ambiental'),
		(13, 'Gestão Comercial'),
		(14, 'Gestão da Produção Industrial'),
		(15, 'Gestão de Turismo'),
		(16, 'Gestão Financeira'),
		(17, 'Gestão Hospitalar'),
		(18, 'Gestão Pública'),
		(19, 'História'),
		(20, 'Letras - Português'),
		(21, 'Letras - Português e Espanhol'),
		(22, 'Letras - Português e Inglês'),
		(23, 'Logística'),
		(24, 'Marketing'),
		(25, 'Marketing Digital'),
		(26, 'Matemática'),
		(27, 'Pedagogia'),
		(28, 'Processos Gerenciais'),
		(29, 'Recursos Humanos'),
		(30, 'Segurança Pública'),
		(31, 'Serviços Jurídicos, Cartorários e Notariais'),
		(32, 'Sociologia'),
		(33, 'Teologia'),
		(34, 'Formação Pedagógica'),
		(35, 'Segunda Licenciatura');
`;
const query2 = `
	INSERT INTO OfertaCurso (
		idCurso, idModalidade
	) VALUES
		(1, 1),
		(1, 2),
		(2, 1),
		(2, 2),
		(3, 1),
		(4, 1),
		(4, 2),
		(5, 1),
		(5, 2),
		(6, 1),
		(6, 2),
		(7, 1),
		(8, 1),
		(9, 1),
		(9, 2),
		(10, 1),
		(10, 2),
		(11, 1),
		(11, 2),
		(12, 1),
		(12, 2),
		(13, 1),
		(13, 2),
		(14, 1),
		(14, 2),
		(15, 1),
		(15, 2),
		(16, 1),
		(16, 2),
		(17, 1),
		(17, 2),
		(18, 1),
		(18, 2),
		(19, 1),
		(19, 2),
		(20, 1),
		(20, 2),
		(21, 1),
		(21, 2),
		(22, 1),
		(22, 2),
		(23, 1),
		(23, 2),
		(24, 1),
		(24, 2),
		(25, 1),
		(25, 2),
		(26, 1),
		(26, 2),
		(27, 1),
		(27, 2),
		(28, 1),
		(28, 2),
		(29, 1),
		(29, 2),
		(30, 1),
		(30, 2),
		(31, 1),
		(31, 2),
		(32, 1),
		(32, 2),
		(33, 1),
		(33, 2),
		(34, 2),
		(35, 2);
`;
let conn;
connect(__filename)
	.then(res => (conn = res).query(query1))
	.then(res1 => {
		console.log({res1});
		return conn.query(query2);
	})
	.then(res2 => {
		console.log({res2});
		conn.end();
	})
	.catch(error => {
		if (conn) conn.end();
		console.log({error});
	});