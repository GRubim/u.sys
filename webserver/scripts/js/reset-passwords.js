const fs = require('fs');
const mariadb = require('mariadb');
const database = 'usys';
const pool = mariadb.createPool({
	host: 'localhost', 
	user: 'root', 
	password: 'admin123',
	database,
	connectionLimit: 20
});
const hash_pwd = require('../../lib/hash-pwd.js');
pool.getConnection()
	.then(conn => {
		const {hash, salt} = hash_pwd.hash('admin123');
		conn.query('UPDATE Usuario SET hash = ?, salt = ? WHERE 1 = 1;', [hash, salt])
			.then(() => {
				conn.end();
				pool.end();
			});
	});