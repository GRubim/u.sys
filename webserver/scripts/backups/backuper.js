const fs = require('fs');
const mariadb = require('mariadb');
const database = 'usys';
const pool = mariadb.createPool({
	host: 'localhost', 
	user: 'root', 
	password: 'admin123',
	database,
	connectionLimit: 20
});
let conn = null;
let tables = [
	
	// 1
	'aluno',
	'curso',
	'modalidade',
	'siteparceiro',
	'usuario',
	
	// 2
	'ofertacurso',
	'comentario',
	'lembrete',
	
	// 3
	'matricula',
	
	// 4
	'agendamentoprova',
	'agendamentoava',
	
	// 5
	'comentarioava'

];
let path = `backup-${(new Date()*1).toString(16)}.foo`;
const file = fs.createWriteStream(path, {flags: 'a'});
const nextTable = (conn, index) => new Promise((done, fail) => {
	let tableName = tables[index];
	if (!tableName) return done();
	conn.query('SELECT * FROM ' + tableName + ';')
		.then(array => {
			array.forEach((item, i) => {
				item.tableName = tableName;
				file.write(JSON.stringify(item) + '\n');
			});
			return nextTable(conn, index + 1);
		})
		.then(done)
		.catch(fail);
});
pool.getConnection()
	.then(res => nextTable(conn = res, 0))
	.then(array => {
		conn.end();
		pool.end();
		console.log('Pronto!');
		setTimeout(() => console.log('Bye'), 1000000);
	})
	.catch(err => {
		if (conn) conn.end();
		console.error(err);
		setTimeout(() => console.log('Bye'), 1000000);
	});