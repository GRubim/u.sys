const FS = require('fs');
const mariadb = require('mariadb');
const exec = require('child_process').execSync;
const pool = mariadb.createPool({
	host: 'localhost', 
	user: 'root', 
	password: 'admin123',
	database: 'usys',
	connectionLimit: 20
});

if (process.argv.length !== 3) {
	console.log('Wrong number of arguments');
	process.exit(1);
}
const filename = process.argv[2];
const total_bytes = FS.statSync(filename).size;
let reader;
const queue = [];
let buffer = '';
const flushQueue = () => new Promise((done, fail) => {
	if (queue.length === 0) return done();
	const line = queue.splice(0, 1)[0];
	runLine(line)
		.then(() => flushQueue())
		.then(done)
		.catch(fail);
});
const flushBuffer = () => {
	let index;
	while ((index = buffer.indexOf('\n')) !== -1) {
		let line = buffer.substr(0, index).replace('\r', '');
		buffer = buffer.substr(index + 1);
		queue.push(line);
	}
};
let conn;
let stopped = false;
const stop = () => {
	if (stopped === true) return;
	stopped = true;
	if (conn) conn.end();
	pool.end();
	reader.close();
	console.log('Pronto!');
	setTimeout(() => console.log('Bye'), 1000000);
};
const handleData = str => {
	buffer += str;
	reader.pause();
	flushBuffer();
	flushQueue()
		.then(() => reader.resume())
		.catch(err => {
			console.error(err);
			stop();
		});
};
const handleClose = () => {
	if (stopped === true) return;
	buffer = buffer.replace('\r', '');
	if (buffer === '') return stop();
	buffer += '\n';
	flushBuffer();
	flushQueue(() => {
		stop();
	}).catch(err => console.error(err));
};

const startReading = () => {
	reader = FS.createReadStream(filename, {
		encoding: 'utf-8',
		highWaterMark: 256
	});
	reader.on('data', handleData);
	reader.on('close', handleClose);
};

const toSQLDate = date => {
	let y = date.getFullYear();
	let M = date.getMonth() + 1;
	let d = date.getDate();
	return `${y}-${M>9?M:'0'+M}-${d>9?d:'0'+d}`;
};
const toSQLTime = date => {
	let h = date.getHours();
	let m = date.getMinutes();
	let s = date.getSeconds();
	return `${h>9?h:'0'+h}:${m>9?m:'0'+m}:${s>9?s:'0'+s}`;
};
const toSQLDatetime = date => {
	return toSQLDate(date) + ' ' + toSQLTime(date);
};

const addAluno = obj => {
	const {id, nome, telefone, email} = obj;
	return conn.query(`
		INSERT INTO Aluno (
			id, nome, telefone, email
		) VALUES (?, ?, ?, ?);
	`, [id, nome, telefone, email]);
};
const addCurso = obj => {
	const {id, nome} = obj;
	return conn.query(`
		INSERT INTO Curso (
			id, nome
		) VALUES (?, ?);
	`, [id, nome]);
};
const addModalidade = obj => {
	const {id, nome} = obj;
	return conn.query(`
		INSERT INTO Modalidade (
			id, nome
		) VALUES (?, ?);
	`, [id, nome]);
};
const addSiteParceiro = obj => {
	const {id, nome} = obj;
	return conn.query(`
		INSERT INTO SiteParceiro (
			id, nome
		) VALUES (?, ?);
	`, [id, nome]);
};
const addUsuario = obj => {
	const {id, nome, login, hash, salt} = obj;
	return conn.query(`
		INSERT INTO Usuario (
			id, nome, login, hash, salt
		) VALUES (?, ?, ?, ?, ?);
	`, [id, nome, login, hash, salt]);
};
const addOfertaCurso = obj => {
	const {id, idModalidade, idCurso} = obj;
	return conn.query(`
		INSERT INTO OfertaCurso (
			id, idModalidade, idCurso
		) VALUES (?, ?, ?);
	`, [id, idModalidade, idCurso]);
};
const addComentario = obj => {
	let {id, idAluno, idUsuario, texto, horario} = obj;
	horario = new Date(horario);
	horario = toSQLDatetime(horario);
	return conn.query(`
		INSERT INTO Comentario (
			id, idAluno, idUsuario, texto, horario
		) VALUES (?, ?, ?, ?, ?);
	`, [id, idAluno, idUsuario, texto, horario]);
};
const addLembrete = obj => {
	let {id, texto, horario, idAluno, idUsuario, idUsuarioDst} = obj;
	horario = new Date(horario);
	horario = toSQLDatetime(horario);
	return conn.query(`
		INSERT INTO Lembrete (
			id, texto, horario, idAluno, idUsuario, idUsuarioDst
		) VALUES (?, ?, ?, ?, ?, ?);
	`, [id, texto, horario, idAluno, idUsuario, idUsuarioDst]);
};
const addMatricula = obj => {
	let {
		id,
		idAluno,
		idOfertaCurso,
		idUsuario,
		idSiteParceiro,
		dataMatriculado,
		estado
	} = obj;
	if (dataMatriculado) {
		dataMatriculado = new Date(dataMatriculado);
		dataMatriculado = toSQLDate(dataMatriculado);
	}
	return conn.query(`
		INSERT INTO Matricula (
			id,
			idAluno,
			idOfertaCurso,
			idUsuario,
			idSiteParceiro,
			dataMatriculado,
			estado
		) VALUES (?, ?, ?, ?, ?, ${dataMatriculado?'?':'NULL'}, ?);
	`, [
		id,
		idAluno,
		idOfertaCurso,
		idUsuario,
		idSiteParceiro,
		...(dataMatriculado?[dataMatriculado]:[]),
		estado
	]);
};
const addAgendamentoProva = obj => {
	let { id, semestre, disciplina, data, horario, idMatricula, idUsuario } = obj;
	data = new Date(data);
	data = toSQLDate(data);
	if (horario) {
		horario = new Date('1990-01-01 ' + horario);
		horario = toSQLTime(horario);
	}
	return conn.query(`
		INSERT INTO AgendamentoProva (
			id, semestre, disciplina, data, horario, idMatricula, idUsuario
		) VALUES (?, ?, ?, ?, ${horario?'?':'NULL'}, ?, ?);
	`, [
		id, semestre, disciplina, data, ...(horario?[horario]:[]), idMatricula, idUsuario
	]);
};
const addAgendamentoAva = obj => {
	let { id, idMatricula, data } = obj;
	data = new Date(data);
	data = toSQLDatetime(data);
	return conn.query(`
		INSERT INTO AgendamentoAva (
			id, idMatricula, data
		) VALUES (?, ?, ?);
	`, [id, idMatricula, data]);
};
const addComentarioAva = obj => {
	let {id, data, texto, idUsuario, idAgendamento} = obj;
	data = new Date(data);
	data = toSQLDatetime(data);
	return conn.query(`
		INSERT INTO AgendamentoAva (
			id, data, texto, idUsuario, idAgendamento
		) VALUES (?, ?, ?);
	`, [id, data, texto, idUsuario, idAgendamento]);
};
let lastTotal = '';
let lastTable = '';
const runLine = line => {
	const obj = JSON.parse(line);
	const tableName = obj.tableName.toLowerCase();
	total = Math.floor((reader.bytesRead/total_bytes)*200);
	if (total !== lastTotal || lastTable !== tableName) {
		let str = (total/2).toString();
		if (str.indexOf('.') === -1) str += '.0';
		console.log('Progresso: ' + '0'.repeat(Math.max(0, 4 - str.length)) + str + '% - table: ' + tableName);
		lastTable = tableName;
		lastTotal = total;
	}
	if (tableName === 'aluno') return addAluno(obj);
	if (tableName === 'curso') return addCurso(obj);
	if (tableName === 'modalidade') return addModalidade(obj);
	if (tableName === 'siteparceiro') return addSiteParceiro(obj);
	if (tableName === 'usuario') return addUsuario(obj);
	
	if (tableName === 'ofertacurso') return addOfertaCurso(obj);
	if (tableName === 'comentario') return addComentario(obj);
	if (tableName === 'lembrete') return addLembrete(obj);
	
	if (tableName === 'matricula') return addMatricula(obj);
	
	if (tableName === 'agendamentoprova') return addAgendamentoProva(obj);
	if (tableName === 'addagendamentoava') return addAgendamentoAva(obj);
	
	if (tableName === 'addcomentarioava') return addComentarioAva(obj);

	return Promise.resolve();
};

console.log('Limpando base de dados...');
const clear_db = `
DELETE FROM usys.ComentarioAva WHERE 1 = 1;
DELETE FROM usys.AgendamentoAva WHERE 1 = 1;
DELETE FROM usys.AgendamentoProva WHERE 1 = 1;
DELETE FROM usys.Matricula WHERE 1 = 1;
DELETE FROM usys.Lembrete WHERE 1 = 1;
DELETE FROM usys.Comentario WHERE 1 = 1;
DELETE FROM usys.OfertaCurso WHERE 1 = 1;
DELETE FROM usys.Usuario WHERE 1 = 1;
DELETE FROM usys.SiteParceiro WHERE 1 = 1;
DELETE FROM usys.Modalidade WHERE 1 = 1;
DELETE FROM usys.Curso WHERE 1 = 1;
DELETE FROM usys.Aluno WHERE 1 = 1;`;
FS.writeFileSync('temp.sql', clear_db);
exec('mysql -u root -padmin123 < temp.sql');
FS.unlinkSync('temp.sql');

console.log('Restaurando dados...');
pool.getConnection().then(res => {
	conn = res;
	startReading();
});