DELETE FROM usys.ComentarioAva WHERE 1 = 1;

DELETE FROM usys.AgendamentoAva WHERE 1 = 1;
DELETE FROM usys.AgendamentoProva WHERE 1 = 1;

DELETE FROM usys.Matricula WHERE 1 = 1;

DELETE FROM usys.Lembrete WHERE 1 = 1;
DELETE FROM usys.Comentario WHERE 1 = 1;
DELETE FROM usys.OfertaCurso WHERE 1 = 1;

DELETE FROM usys.Usuario WHERE 1 = 1;
DELETE FROM usys.SiteParceiro WHERE 1 = 1;
DELETE FROM usys.Modalidade WHERE 1 = 1;
DELETE FROM usys.Curso WHERE 1 = 1;
DELETE FROM usys.Aluno WHERE 1 = 1;