SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

CREATE SCHEMA IF NOT EXISTS usys DEFAULT CHARACTER SET utf8;
USE usys;

CREATE TABLE IF NOT EXISTS usys.Usuario (
	id INT NOT NULL AUTO_INCREMENT,
	nome VARCHAR(45) NOT NULL,
	login VARCHAR(24) NOT NULL,
	hash VARCHAR(40) NOT NULL,
	salt VARCHAR(16) NOT NULL,
	PRIMARY KEY (id),
	UNIQUE INDEX id_UNIQUE (id ASC),
	UNIQUE INDEX login_UNIQUE (login ASC))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS usys.Aluno (
	id INT NOT NULL AUTO_INCREMENT,
	nome VARCHAR(64) NOT NULL,
	telefone VARCHAR(20) NOT NULL,
	email VARCHAR(45) NULL,
	PRIMARY KEY (id),
	UNIQUE INDEX id_UNIQUE (id ASC))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS usys.Curso (
	id INT NOT NULL AUTO_INCREMENT,
	nome VARCHAR(75) NOT NULL,
	PRIMARY KEY (id),
	UNIQUE INDEX id_UNIQUE (id ASC),
	UNIQUE INDEX nome_UNIQUE (nome ASC))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS usys.Modalidade (
	id INT NOT NULL AUTO_INCREMENT,
	nome VARCHAR(45) NOT NULL,
	PRIMARY KEY (id),
	UNIQUE INDEX id_UNIQUE (id ASC),
	UNIQUE INDEX nome_UNIQUE (nome ASC))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS usys.OfertaCurso (
	id INT NOT NULL AUTO_INCREMENT,
	idModalidade INT NOT NULL,
	idCurso INT NOT NULL,
	PRIMARY KEY (id),
	UNIQUE INDEX id_UNIQUE (id ASC),
	INDEX fk_OfertaCurso_Modalidade_idx (idModalidade ASC),
	INDEX fk_OfertaCurso_Curso1_idx (idCurso ASC),
	CONSTRAINT fk_OfertaCurso_Modalidade
		FOREIGN KEY (idModalidade)
		REFERENCES usys.Modalidade (id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT fk_OfertaCurso_Curso1
		FOREIGN KEY (idCurso)
		REFERENCES usys.Curso (id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS usys.SiteParceiro (
	id INT NOT NULL AUTO_INCREMENT,
	nome VARCHAR(45) NOT NULL,
	PRIMARY KEY (id),
	UNIQUE INDEX id_UNIQUE (id ASC),
	UNIQUE INDEX nome_UNIQUE (nome ASC))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS usys.Matricula (
	id INT NOT NULL AUTO_INCREMENT,
	idAluno INT NOT NULL,
	idOfertaCurso INT NOT NULL,
	idUsuario INT NULL,
	idSiteParceiro INT NULL,
	dataMatriculado DATE NULL,
	estado INT NOT NULL,
	PRIMARY KEY (id),
	UNIQUE INDEX id_UNIQUE (id ASC),
	INDEX fk_Matricula_Aluno1_idx (idAluno ASC),
	INDEX fk_Matricula_OfertaCurso1_idx (idOfertaCurso ASC),
	INDEX fk_Matricula_Usuario1_idx (idUsuario ASC),
	INDEX fk_Matricula_SiteParceiro1_idx (idSiteParceiro ASC),
	CONSTRAINT fk_Matricula_Aluno1
		FOREIGN KEY (idAluno)
		REFERENCES usys.Aluno (id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT fk_Matricula_OfertaCurso1
		FOREIGN KEY (idOfertaCurso)
		REFERENCES usys.OfertaCurso (id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT fk_Matricula_Usuario1
		FOREIGN KEY (idUsuario)
		REFERENCES usys.Usuario (id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT fk_Matricula_SiteParceiro1
		FOREIGN KEY (idSiteParceiro)
		REFERENCES usys.SiteParceiro (id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS usys.Comentario (
	id INT NOT NULL AUTO_INCREMENT,
	idAluno INT NOT NULL,
	idUsuario INT NOT NULL,
	texto TEXT(1023) NOT NULL,
	horario DATETIME NOT NULL,
	PRIMARY KEY (id),
	UNIQUE INDEX id_UNIQUE (id ASC),
	INDEX fk_Comentario_Usuario1_idx (idUsuario ASC),
	INDEX fk_Comentario_Aluno1_idx (idAluno ASC),
	CONSTRAINT fk_Comentario_Usuario1
		FOREIGN KEY (idUsuario)
		REFERENCES usys.Usuario (id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT fk_Comentario_Aluno1
		FOREIGN KEY (idAluno)
		REFERENCES usys.Aluno (id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS usys.Lembrete (
	id INT NOT NULL AUTO_INCREMENT,
	texto TEXT(1023) NOT NULL,
	horario DATETIME NOT NULL,
	idAluno INT NOT NULL,
	idUsuario INT NOT NULL,
	idUsuarioDst INT NOT NULL,
	PRIMARY KEY (id),
	UNIQUE INDEX data_UNIQUE (id ASC),
	INDEX fk_Lembrete_Aluno1_idx (idAluno ASC),
	INDEX fk_Lembrete_Usuario1_idx (idUsuario ASC),
	INDEX fk_Lembrete_Usuario2_idx (idUsuarioDst ASC),
	CONSTRAINT fk_Lembrete_Aluno1
		FOREIGN KEY (idAluno)
		REFERENCES usys.Aluno (id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT fk_Lembrete_Usuario1
		FOREIGN KEY (idUsuario)
		REFERENCES usys.Usuario (id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT fk_Lembrete_Usuario2
		FOREIGN KEY (idUsuarioDst)
		REFERENCES usys.Usuario (id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS usys.AgendamentoProva (
	id INT NOT NULL AUTO_INCREMENT,
	semestre INT NOT NULL,
	disciplina VARCHAR(45) NOT NULL,
	data DATE NOT NULL,
	horario TIME NULL,
	idMatricula INT NOT NULL,
	idUsuario INT NOT NULL,
	PRIMARY KEY (id),
	UNIQUE INDEX id_UNIQUE (id ASC),
	INDEX fk_AgendamentoProva_Matricula1_idx (idMatricula ASC),
	INDEX fk_AgendamentoProva_Usuario1_idx (idUsuario ASC),
	CONSTRAINT fk_AgendamentoProva_Matricula1
		FOREIGN KEY (idMatricula)
		REFERENCES usys.Matricula (id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT fk_AgendamentoProva_Usuario1
		FOREIGN KEY (idUsuario)
		REFERENCES usys.Usuario (id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS usys.AgendamentoAva (
	id INT NOT NULL AUTO_INCREMENT,
	idMatricula INT NOT NULL,
	data DATETIME NOT NULL,
	PRIMARY KEY (id),
	UNIQUE INDEX id_UNIQUE (id ASC),
	INDEX fk_AgendamentoAva_Matricula1_idx (idMatricula ASC),
	CONSTRAINT fk_AgendamentoAva_Matricula1
		FOREIGN KEY (idMatricula)
		REFERENCES usys.Matricula (id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS usys.ComentarioAva (
	id INT NOT NULL AUTO_INCREMENT,
	data DATETIME NOT NULL,
	texto TEXT(1023) NOT NULL,
	idUsuario INT NOT NULL,
	idAgendamento INT NOT NULL,
	PRIMARY KEY (id),
	UNIQUE INDEX id_UNIQUE (id ASC),
	INDEX fk_ComentarioAva_Usuario1_idx (idUsuario ASC),
	INDEX fk_ComentarioAva_AgendamentoAva1_idx (idAgendamento ASC),
	CONSTRAINT fk_ComentarioAva_Usuario1
		FOREIGN KEY (idUsuario)
		REFERENCES usys.Usuario (id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT fk_ComentarioAva_AgendamentoAva1
		FOREIGN KEY (idAgendamento)
		REFERENCES usys.AgendamentoAva (id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

USE usys;

INSERT IGNORE INTO Usuario (
	nome,
	login,
	hash,
	salt
) VALUES (
	'Administrador',
	'admin',
	'bdc9632a513d3065a83b0b10dc0b2f533eaf2f5a',
	'1bd4fac6385d00e9'
);

INSERT IGNORE INTO Modalidade (
	id, nome
) VALUES (
	1, 'Semi presencial'
), (
	2, '100% on-line'
);