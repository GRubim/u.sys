USE usys;

INSERT INTO Usuario (
  nome,
  login,
  hash,
  salt
) VALUES (
  'Administrador',
  'admin',
  'bdc9632a513d3065a83b0b10dc0b2f533eaf2f5a',
  '1bd4fac6385d00e9'
);

INSERT INTO Modalidade (
  id, nome
) VALUES (
  1, 'Semi presencial'
), (
  2, '100% on-line'
);
