#include <stdio.h>
#include <stdlib.h>

int main(int n_args, const char* args[]) {
	if (n_args < 2) {
		puts("No input files");
		return 1;
	}
	int i;
	for (i=1; i<n_args; ++i) {
		char line[1024];
		sprintf(line, "mysql -u root -padmin123 < %s", args[i]);
		system(line);
	}
	return 0;
}