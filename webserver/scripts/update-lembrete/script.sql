DROP TABLE Lembrete;
CREATE TABLE Lembrete (
	id INT NOT NULL AUTO_INCREMENT,
	texto TEXT(1023) NOT NULL,
	horario DATETIME NOT NULL,
	idAluno INT NOT NULL,
	idUsuario INT NOT NULL,
	idUsuarioDst INT NOT NULL,
	PRIMARY KEY (id),
	UNIQUE INDEX data_UNIQUE (id ASC),
	INDEX fk_Lembrete_Aluno1_idx (idAluno ASC),
	INDEX fk_Lembrete_Usuario1_idx (idUsuario ASC),
	INDEX fk_Lembrete_Usuario2_idx (idUsuarioDst ASC),
	CONSTRAINT fk_Lembrete_Aluno1
		FOREIGN KEY (idAluno)
		REFERENCES usys.Aluno (id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT fk_Lembrete_Usuario1
		FOREIGN KEY (idUsuario)
		REFERENCES usys.Usuario (id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT fk_Lembrete_Usuario2
		FOREIGN KEY (idUsuarioDst)
		REFERENCES usys.Usuario (id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
);