const dao = require('./dao');
module.exports = app => {
	
	app.post('/addMatricula', (req, res) => {
		if (req.notLogged()) return;
		const {estado, idAluno, idOfertaCurso, idUsuario, idSiteParceiro} = req.body;
		if (!estado || !idAluno || !idOfertaCurso || (!idUsuario && !idSiteParceiro)) {
			return res.sendJson(false);
		}
		dao.add({estado, idAluno, idOfertaCurso, idUsuario, idSiteParceiro})
			.then(id => res.sendJson(id))
			.catch(err => res.error(500, err));
	});
	
	app.get('/listMatriculas', (req, res) => {
		if (req.notLogged()) return;
		const {idAluno, estados} = req.query;
		if (!idAluno) return res.sendJson(false);
		dao.list(idAluno, estados)
			.then(array => res.sendJson(array))
			.catch(err => res.error(500, err));
	});

	app.get('/matricula/interessados', (req, res) => {
		if (req.notLogged()) return;
		dao.interessados({...req.query})
			.then(array => res.sendJson(array))
			.catch(err => res.error(500, err));
	});
	
	app.get('/getMatriculaById', (req, res) => {
		if (req.notLogged()) return;
		const {id} = req.query;
		if (!id) return res.sendJson(false);
		dao.get(id)
			.then(matricula => res.sendJson(matricula))
			.catch(err => res.error(500, err));
	});
	
	app.post('/updateMatricula', (req, res) => {
		if (req.notLogged()) return;
		const {id, estado, idOfertaCurso} = req.body;
		if (!id || !estado || !idOfertaCurso) return res.sendJson(false);
		dao.update({id, estado, idOfertaCurso})
			.then(updated => res.sendJson(updated))
			.catch(err => res.error(500, err));
	});

	app.get('/matricula/listPorColaborador', (req, res) => {
		if (req.notLogged()) return;
		const {idUsuario, idSiteParceiro, ini, fim} = req.query;
		dao.listPorColaborador(idUsuario, idSiteParceiro, ini, fim)
			.then(array => res.sendJson(array))
			.catch(err => res.error(500, err));
	});

	return app;
};
