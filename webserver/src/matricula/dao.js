const daoAVA = require('../agendamentoAva/dao.js');
const connect = require('../../lib/db-conn');
const sqlDateTime = require('../../lib/sql-datetime');
const ESTADO_MATRICULADO = 1;
const ESTADO_TRANCADO = 2;
const ESTADO_CANCELADO = 3;
const ESTADO_INTERESSADO = 4;
const ESTADO_INTERESSE_INATIVO = 5;

const getIdCurso = (conn, idOfertaCurso) => new Promise((done, fail) => {
	conn.query(`
		SELECT
			curso.id
		FROM OfertaCurso oferta
		INNER JOIN Curso curso ON curso.id = oferta.idCurso
		WHERE oferta.id = ?
	`, [idOfertaCurso])
		.then(array => done({conn, idCurso: array.length ? array[0].id : null}))
		.catch(error => fail({conn, error}));
});

const cursoBate = (conn, idMatricula, idCurso) => new Promise((done, fail) => {
	conn.query(`
		SELECT
			oferta.idCurso
		FROM Matricula mat
		INNER JOIN OfertaCurso oferta ON oferta.id = mat.idOfertaCurso
		WHERE mat.id = ?;
	`, [idMatricula])
		.then(array => done({conn, bate: array.length !== 0 && array[0].idCurso === idCurso}))
		.catch(error => fail({conn, error}));
});

const get = (conn, idMatricula) => new Promise((done, fail) => {
	conn.query(`
		SELECT
			mat.*,
			curso.id   AS idCurso,
			curso.nome AS nomeCurso,
			modal.nome AS modalidade,
			user.nome  AS nomeUsuario,
			site.nome  AS nomeSite,
			aluno.nome AS nomeAluno
		FROM Matricula mat
		INNER JOIN OfertaCurso  oferta ON oferta.id = mat.idOfertaCurso
		INNER JOIN Curso        curso  ON curso.id  = oferta.idCurso
		INNER JOIN Modalidade   modal  ON modal.id  = oferta.idModalidade
		LEFT  JOIN Usuario      user   ON user.id   = mat.idUsuario
		LEFT  JOIN SiteParceiro site   ON site.id   = mat.idSiteParceiro
		INNER JOIN Aluno        aluno  ON aluno.id  = mat.idAluno
		WHERE mat.id = ?;
	`, [idMatricula])
		.then(array => done({conn, matricula: array[0] || null}))
		.catch(error => fail({conn, error}));
});

const get_std = (conn, idMatricula) => new Promise((done, fail) => {
	get(conn, idMatricula)
		.then(({matricula}) => done(matricula))
		.catch(({error}) => fail(error));
});

const getByAlunoCurso = (conn, idAluno, idCurso) => new Promise((done, fail) => {
	conn.query(`
		SELECT mat.*
		FROM Matricula mat
		INNER JOIN OfertaCurso oferta ON oferta.id = mat.idOfertaCurso
		INNER JOIN Curso       curso  ON curso.id  = oferta.idCurso
		WHERE mat.idAluno = ? AND oferta.idCurso = ?;
	`, [idAluno, idCurso])
		.then(array => done({conn, matricula: array[0] || null}))
		.catch(error => fail({conn, error}));
});

const add = (conn, matricula) => new Promise((done, fail) => {
	let {estado, idAluno, idOfertaCurso, idSiteParceiro, idUsuario} = matricula;
	const query = `
		INSERT INTO Matricula (
			dataMatriculado, estado, idAluno, idOfertaCurso, idSiteParceiro, idUsuario
		) VALUES (
			${ estado == ESTADO_MATRICULADO ? '?,' : 'NULL,' }
			?, ?, ?, ${ idSiteParceiro ? '?, NULL' : 'NULL, ?' }
		);
	`;
	const values = estado == ESTADO_MATRICULADO ? [sqlDateTime.dateNow()] : [];
	values.push(estado, idAluno, idOfertaCurso, idSiteParceiro || idUsuario);
	conn.query(query, values).then(res => {
		done({conn, id: res.insertId});
	}).catch(error => {
		fail({conn, error});
	});
});

const update = (conn, matricula) => new Promise((done, fail) => {
	const {id, idOfertaCurso, estado} = matricula;
	conn.query(`
		UPDATE Matricula
		SET
			idOfertaCurso = ?,
			estado = ?,
			dataMatriculado = (CASE
				WHEN (dataMatriculado IS NULL) AND (? = 1) THEN ?
				ELSE dataMatriculado
			END)
		WHERE id = ?;
	`, [idOfertaCurso, estado, estado, sqlDateTime.dateNow(), id])
		.then(res => done({conn, updated: res.affectedRows !== 0}))
		.catch(error => fail({conn, error}));
});

const update_std = (conn, matricula) => new Promise((done, fail) => {
	update(conn, matricula)
		.then(({updated}) => done(updated))
		.catch(({error}) => fail(error));
});

const updateCheckAVA_std = (conn, matricula) => new Promise((done, fail) => {
	let antes, depois = matricula.estado;
	get_std(conn, matricula.id)
		.then(obj => {
			antes = obj.estado;
			return update_std(conn, matricula);
		})
		.then(updated => {
			if (!updated) {
				return done(false);
			}
			if (antes != depois && depois == ESTADO_MATRICULADO) {
				return daoAVA.addUnique(conn, matricula.id);
			}
			done(updated);
		})
		.then(done)
		.catch(fail);
});

const updateCheckAVA = (conn, matricula) => new Promise((done, fail) => {
	updateCheckAVA_std(conn, matricula)
		.then(updated => done({conn, updated}))
		.catch(error => fail({conn, error}));
});

const list = (conn, idAluno, estados) => new Promise((done, fail) => {
	conn.query(`
		SELECT
			mat.id,
			mat.estado,
			curso.nome AS nomeCurso,
			modal.nome AS modalidade
		FROM MATRICULA mat
		INNER JOIN OfertaCurso oferta ON oferta.id = mat.idOfertaCurso
		INNER JOIN Curso       curso  ON curso.id  = oferta.idCurso
		INNER JOIN Modalidade  modal  ON modal.id  = oferta.idModalidade
		WHERE mat.idAluno = ?
		ORDER BY mat.estado, nomeCurso;
	`, [idAluno])
		.then(array => done({conn, array}))
		.catch(error => fail({conn, error}));
});

const interessados = (conn, args) => new Promise((done, fail) => {
	const values = [ESTADO_INTERESSADO];
	const {idUsuario, idSiteParceiro} = args;
	if (idUsuario) values.push(idUsuario);
	if (idSiteParceiro) values.push(idSiteParceiro);
	conn.query(`
		SELECT	
			aluno.id AS idAluno,
			aluno.nome AS nomeAluno,
			curso.nome AS nomeCurso,
			sitep.nome AS nomeSite,
			usuar.nome AS nomeUsuario
		FROM Matricula matri
		INNER JOIN Aluno        aluno ON aluno.id = matri.idAluno
		INNER JOIN OfertaCurso  ofert ON ofert.id = matri.idOfertaCurso
		INNER JOIN Curso        curso ON curso.id = ofert.idCurso
		LEFT  JOIN SiteParceiro sitep ON sitep.id = matri.idSiteParceiro
		LEFT  JOIN Usuario      usuar ON usuar.id = matri.idUsuario
		WHERE matri.estado = ?
		${ idUsuario ? ' AND idUsuario = ?' : '' }
		${ idSiteParceiro ? ' AND idSiteParceiro = ?' : '' }
		ORDER BY matri.id;
	`, values)
		.then(done)
		.catch(fail);
});

const listPorColaborador = (conn, idUsuario, idSiteParceiro, ini, fim) => {
	return conn.query(`
		SELECT
			mat.id AS id,
			alu.id AS idAluno,
			alu.nome AS nomeAluno,
			cur.nome AS nomeCurso,
			mat.dataMatriculado AS data
		FROM Matricula mat
		INNER JOIN OfertaCurso ofe ON ofe.id = mat.idOfertaCurso
		INNER JOIN Curso cur ON cur.id = ofe.idCurso
		INNER JOIN Aluno alu ON alu.id = mat.idAluno
		WHERE
			mat.${ idUsuario ? 'idUsuario' : 'idSiteParceiro' } = ?
			AND dataMatriculado >= ? AND dataMatriculado <= ?;
	`, [idUsuario || idSiteParceiro, ini, fim]);
};

module.exports.list = (idAluno, estados) => new Promise((done, fail) => {
	connect(__filename)
		.then(conn => list(conn, idAluno, estados))
		.then(({conn, array}) => {
			conn.end();
			done(array);
		})
		.catch(({conn, error}) => {
			if (conn) conn.end();
			fail(error);
		});
});

module.exports.interessados = args => new Promise((done, fail) => {
	let conn;
	connect(__filename)
		.then(res => interessados(conn = res, args))
		.then(array => {
			conn.end();
			done(array);
		})
		.catch(error => {
			if (conn) conn.end();
			fail(error);
		})
});

module.exports.add = matricula => new Promise((done, fail) => {
	connect(__filename)
		.then(conn => getIdCurso(conn, matricula.idOfertaCurso))
		.then(({conn, idCurso}) => {
			if (!idCurso) {
				conn.end();
				fail('Curso inválido');
				return;
			}
			return getByAlunoCurso(conn, matricula.idAluno, idCurso);
		})
		.then(res => {
			const {conn} = res;
			if (res.matricula) {
				conn.end();
				fail('Matrícula já existente');
				return;
			}
			return add(conn, matricula);
		})
		.then(({conn, id}) => {
			conn.end();
			done(id);
		})
		.catch(({conn, error}) => {
			if (conn) conn.end();
			fail(error);
		})
});

module.exports.get = idMatricula => new Promise((done, fail) => {
	connect(__filename)
		.then(conn => get(conn, idMatricula))
		.then(({conn, matricula}) => {
			conn.end();
			done(matricula);
		})
		.catch(({conn, error}) => {
			if (conn) conn.end();
			fail(error);
		})
});

module.exports.update = matricula => new Promise((done, fail) => {
	const {id, estado, idOfertaCurso} = matricula;
	connect(__filename)
		.then(conn => getIdCurso(conn, idOfertaCurso))
		.then(({conn, idCurso}) => {
			if (!idCurso) {
				conn.end();
				fail('Curso inválido');
				return;
			}
			return cursoBate(conn, id, idCurso);
		})
		.then(({conn, bate}) => {
			if (!bate) {
				conn.end();
				fail('Proibido alterar o curso');
				return;
			}
			return updateCheckAVA(conn, matricula);
		})
		.then(({conn, updated}) => {
			conn.end();
			done(updated);
		})
		.catch(({conn, error}) => {
			if (conn) conn.end();
			fail(error);
		});
});

module.exports.listPorColaborador = (idUsuario, idSiteParceiro, ini, fim) => new Promise((done, fail) => {
	let conn;
	connect(__filename)
		.then(res => listPorColaborador(conn = res, idUsuario, idSiteParceiro, ini, fim))
		.then(array => {
			conn.end();
			done(array);
		})
		.catch(error => {
			if (conn) conn.end();
			fail(error);
		});
});