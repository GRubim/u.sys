const connect = require('../../lib/db-conn');

const listUsuarios = conn => conn.query(`
	SELECT
		id AS idUsuario,
		nome
	FROM Usuario
	ORDER BY nome;
`);

const listSitesParceiros = conn => conn.query(`
	SELECT
		id AS idSiteParceiro,
		nome
	FROM SiteParceiro
	ORDER BY nome;
`);

module.exports.list = search => new Promise((done, fail) => {
	let conn, usuarios, sites;
	connect(__filename)
		.then(res => listUsuarios(conn = res))
		.then(array => {
			usuarios = array;
			return listSitesParceiros(conn);
		})
		.then(array => {
			conn.end();
			done(array.concat(usuarios));
		})
		.catch(error => {
			if (conn) conn.end();
			fail(error);
		});
});
