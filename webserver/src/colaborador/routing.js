const dao = require('./dao');

module.exports = app => {
	app.get('/colaborador/list', (req, res) => {
		if (req.notLogged()) return;
		dao.list()
			.then(array => res.sendJson(array))
			.catch(err => res.error(500, err));
	});
};
