const connect = require('../../lib/db-conn');

const add = (conn, lembrete) => new Promise((done, fail) => {
	const {
		texto,
		horario,
		idAluno,
		idUsuario,
		idUsuarioDst
	} = lembrete;
	conn.query(`
		INSERT INTO Lembrete (
			texto,
			horario,
			idAluno,
			idUsuario,
			idUsuarioDst
		) VALUES (?, ?, ?, ?, ?);
	`, [
		texto,
		horario,
		idAluno,
		idUsuario,
		idUsuarioDst || idUsuario
	])
		.then(res => done(res.insertId))
		.catch(fail);
});

const get = (conn, id, idUsuario) => new Promise((done, fail) => {
	conn.query(`
		SELECT lembr.*, aluno.nome AS nomeAluno, usr.nome AS nomeUsuario
		FROM Lembrete lembr
		INNER JOIN Aluno aluno ON aluno.id = lembr.idAluno
		INNER JOIN Usuario usr ON usr.id = lembr.idUsuario
		WHERE lembr.id = ? AND (lembr.idUsuario = ? OR lembr.idUsuarioDst = ?);
	`, [id, idUsuario, idUsuario])
		.then(array => done(array[0] || null))
		.catch(fail);
});

const remove = (conn, id, idUsuario) => new Promise((done, fail) => {
	conn.query(`
		DELETE FROM Lembrete
		WHERE id = ? AND (idUsuario = ? OR idUsuarioDst = ?);
	`, [id, idUsuario, idUsuario])
		.then(res => done(res.affectedRows !== 0))
		.catch(fail);
});

const ate = (conn, idUsuario, max) => conn.query(`
	SELECT
		l.*,
		a.nome AS nomeAluno
	FROM Lembrete l
	INNER JOIN Aluno   a ON a.id = l.idAluno
	INNER JOIN Usuario u ON u.id = l.idUsuario
	WHERE idUsuarioDst = ? AND horario <= ?;
`, [idUsuario, max]);

const list = (conn, idUsuario) => conn.query(`
	SELECT *
	FROM Lembrete
	WHERE idUsuarioDst = ?
	ORDER BY horario;
`, [idUsuario]);

const update = (conn, lembrete, idUsuario) => new Promise((done, fail) => {
	const {id, texto, horario} = lembrete;
	conn.query(`
		UPDATE Lembrete
		SET
			texto = ?,
			horario = ?
		WHERE id = ? AND (idUsuario = ? OR idUsuarioDst = ?);
	`, [texto, horario, id, idUsuario, idUsuario])
		.then(res => done(res.affectedRows !== 0))
		.catch(fail);
});

module.exports.add = lembrete => new Promise((done, fail) => {
	let conn;
	connect(__filename)
		.then(res => add(conn = res, lembrete))
		.then(id => {
			conn.end();
			done(id);
		})
		.catch(err => {
			if (!conn) conn.end();
			fail(err);
		});
});

module.exports.get = (id, idUsuario) => new Promise((done, fail) => {
	let conn;
	connect(__filename)
		.then(res => get(conn = res, id, idUsuario))
		.then(res => {
			conn.end();
			done(res);
		})
		.catch(err => {
			if (conn) conn.end();
			fail(err);
		});
});

module.exports.remove = (id, idUsuario) => new Promise((done, fail) => {
	let conn;
	connect(__filename)
		.then(res => remove(conn = res, id, idUsuario))
		.then(res => {
			conn.end();
			done(res);
		})
		.catch(err => {
			if (conn) conn.end();
			fail(err);
		});
});

module.exports.ate = (idUsuario, max) => new Promise((done, fail) => {
	let conn;
	connect(__filename)
		.then(res => ate(conn = res, idUsuario, max))
		.then(array => {
			conn.end();
			done(array);
		})
		.catch(err => {
			if (conn) conn.end();
			fail(err);
		});
});

module.exports.update = (lembrete, idUsuario) => new Promise((done, fail) => {
	let conn;
	connect(__filename)
		.then(res => update(conn = res, lembrete, idUsuario))
		.then(res => {
			conn.end();
			done(res);
		})
		.catch(err => {
			if (conn) conn.end();
			fail(err);
		});
});

module.exports.list = idUsuario => new Promise((done, fail) => {
	let conn;
	connect(__filename)
		.then(res => list(conn = res, idUsuario))
		.then(array => {
			conn.end();
			done(array);
		})
		.catch(err => {
			if (conn) conn.end();
			fail(err);
		});
});