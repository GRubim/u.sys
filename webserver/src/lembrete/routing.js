const dao = require('./dao');

module.exports = app => {

	app.post('/lembrete/add', (req, res) => {
		if (req.notLogged()) return;
		const lembrete = {...req.body};
		lembrete.idUsuario = req.session.user.id;
		dao.add(lembrete)
			.then(id => res.sendJson(id))
			.catch(err => res.error(500, err));
	});

	app.get('/lembrete/get', (req, res) => {
		if (req.notLogged()) return;
		const {id} = req.query;
		const idUsuario = req.session.user.id;
		dao.get(id, idUsuario)
			.then(lembrete => res.sendJson(lembrete))
			.catch(err => res.error(500, err));
	});

	app.post('/lembrete/remove', (req, res) => {
		if (req.notLogged()) return;
		const {id} = req.body;
		const idUsuario = req.session.user.id;
		dao.remove(id, idUsuario)
			.then(removed => res.sendJson(removed))
			.catch(err => res.error(500, err));
	});

	app.get('/lembrete/ate', (req, res) => {
		if (req.notLogged()) return;
		const {max} = req.query;
		const idUsuario = req.session.user.id;
		dao.ate(idUsuario, max)
			.then(array => res.sendJson(array))
			.catch(err => res.error(500, err));
	});

	app.post('/lembrete/update', (req, res) => {
		if (req.notLogged()) return;
		const {id, texto, horario} = req.body;
		const idUsuario = req.session.user.id;
		dao.update({id, texto, horario}, idUsuario)
			.then(updated => res.sendJson(updated))
			.catch(err => res.error(500, err));
	});

	app.get('/lembrete/list', (req, res) => {
		if (req.notLogged()) return;
		const idUsuario = req.session.user.id;
		dao.list(idUsuario)
			.then(array => res.sendJson(array))
			.catch(err => res.error(500, err));
	});

};
