const ESTADO_MATRICULADO = 1;
const ESTADO_TRANCADO = 2;
const ESTADO_CANCELADO = 3;
const ESTADO_INTERESSADO = 4;
const ESTADO_INTERESSE_INATIVO = 5;

const connect = require('../../lib/db-conn.js');
const {now} = require('../../lib/sql-datetime.js');

const add = (conn, agendamento) => new Promise((done, fail) => {
	const {idMatricula} = agendamento;
	conn.query('INSERT INTO AgendamentoAva (idMatricula, data) VALUES (?, ?);', [
		idMatricula, now()
	])
		.then(res => done(res.insertId || null))
		.catch(fail);
});

const get = (conn, id) => new Promise((done, fail) => {
	conn.query(`
		SELECT
			ava.id,
			ava.data,
			aluno.nome AS nomeAluno,
			curso.nome AS nomeCurso,
			modal.nome AS modalidade
		FROM AgendamentoAva ava
		INNER JOIN Matricula mat ON mat.id = ava.idMatricula
		INNER JOIN OfertaCurso oferta ON oferta.id = mat.idOfertaCurso
		INNER JOIN Curso curso ON curso.id = oferta.idCurso
		INNER JOIN Modalidade modal ON modal.id = oferta.idModalidade
		INNER JOIN Aluno aluno ON aluno.id = mat.idAluno
		WHERE ava.id = ?;
	`, [id])
		.then(res => done(res[0] || null))
		.catch(fail);
});

const list = conn => conn.query(`
	SELECT
		ava.id,
		ava.data,
		aluno.nome AS nomeAluno,
		curso.nome AS nomeCurso
	FROM AgendamentoAva ava
	INNER JOIN Matricula mat ON mat.id = ava.idMatricula
	INNER JOIN Aluno aluno ON aluno.id = mat.idAluno
	INNER JOIN OfertaCurso oc ON oc.id = mat.idOfertaCurso
	INNER JOIN Curso curso ON curso.id = oc.idCurso
	ORDER BY ava.data;
`);

const listByAluno = (conn, idAluno) => conn.query(`
	SELECT
		ava.id,
		ava.data,
		aluno.nome AS nomeAluno,
		curso.nome AS nomeCurso
	FROM AgendamentoAva ava
	INNER JOIN Matricula mat ON mat.id = ava.idMatricula
	INNER JOIN Aluno aluno ON aluno.id = mat.idAluno
	INNER JOIN OfertaCurso oc ON oc.id = mat.idOfertaCurso
	INNER JOIN Curso curso ON curso.id = oc.idCurso
	WHERE aluno.id = ?;
`, [idAluno]);

const remove = (conn, id) => new Promise((done, fail) => {
	conn.query('DELETE FROM ComentarioAva WHERE idAgendamento = ?;', [id])
		.then(() => conn.query('DELETE FROM AgendamentoAva WHERE id = ?;', [id]))
		.then(res => done(true))
		.catch(fail);
});

const getId = (conn, idMatricula) => new Promise((done, fail) => {
	conn.query('SELECT id FROM AgendamentoAva WHERE idMatricula = ?;', [idMatricula])
		.then(array => done(array.length !== 0 && array[0].id || null))
		.catch(fail);
});

const getEstadoMatricula = (conn, idMatricula) => new Promise((done, fail) => {
	conn.query('SELECT estado FROM Matricula WHERE id = ?;', [idMatricula])
		.then(array => {
			if (!array.length) {
				fail(`Matrícula com id ${id} não encontrada`);
			} else {
				done(array[0].estado);
			}
		})
		.catch(fail);
});

const podeAgendar = (conn, idMatricula) => new Promise((done, fail) => {
	getEstadoMatricula(conn, idMatricula)
		.then(estado => {
			if (estado != ESTADO_MATRICULADO) {
				done(false);
			} else {
				return getId(conn, idMatricula);
			}
		})
		.then(id => done(!id))
		.catch(fail);
});

module.exports.add = matricula => new Promise((done, fail) => {
	let conn = null;
	connect(__filename)
		.then(res => add(conn = res, matricula))
		.then(id => {
			conn.end();
			done(id);
		})
		.catch(error => {
			if (conn !== null) conn.end();
			fail(error);
		});
});

module.exports.list = config => new Promise((done, fail) => {
	let idAluno = null;
	if (config) {
		idAluno = config.idAluno;
	}
	let conn = null;
	connect(__filename)
		.then(res => {
			conn = res;
			if (idAluno) {
				return listByAluno(conn, idAluno);
			}
			return list(conn);
		})
		.then(array => {
			conn.end();
			done(array);
		})
		.catch(error => {
			if (conn !== null) conn.end();
			fail(error);
		});
});

module.exports.remove = id => new Promise((done, fail) => {
	let conn = null;
	connect(__filename)
		.then(res => remove(conn = res, id))
		.then(res => {
			conn.end();
			done(res);
		})
		.catch(error => {
			if (conn !== null) conn.end();
			fail(error);
		});
});

module.exports.exists = idMatricula => new Promise((done, fail) => {
	let conn = null;
	connect(__filename)
		.then(res => getId(conn = res, idMatricula))
		.then(id => {
			conn.end();
			done(id !== null);
		})
		.catch(error => {
			if (conn !== null) conn.end();
			fail(error);
		});
});

module.exports.get = id => new Promise((done, fail) => {
	let conn = null;
	connect(__filename)
		.then(res => get(conn = res, id))
		.then(res => {
			conn.end();
			done(res);
		})
		.catch(error => {
			if (conn !== null) conn.end();
			fail(error);
		});
});

module.exports.podeAgendar = idMatricula => new Promise((done, fail) => {
	let conn = null;
	connect(__filename)
		.then(res => podeAgendar(conn = res, idMatricula))
		.then(res => {
			conn.end();
			done(res);
		})
		.catch(error => {
			if (conn === null) conn.end();
			fail(error);
		});
});

module.exports.addUnique = (conn, idMatricula) => new Promise((done, fail) => {
	podeAgendar(conn, idMatricula)
		.then(pode => {
			if (pode) {
				return add(conn, {idMatricula});
			}
			done(null);
		})
		.then(done)
		.catch(fail);
});
