const dao = require('./dao.js');

module.exports = app => {
	
	app.post('/agendamentoAva/add', (req, res) => {
		if (req.notLogged()) return;
		const {idMatricula} = req.body;
		dao.add({idMatricula})
			.then(id => res.sendJson(id))
			.catch(err => res.error(500, err));
	});

	app.post('/agendamentoAva/remove', (req, res) => {
		if (req.notLogged()) return;
		const id = req.body.id;
		dao.remove(id)
			.then(removed => res.sendJson(removed))
			.catch(err => res.error(500, err));
	});

	app.get('/agendamentoAva/list', (req, res) => {
		if (req.notLogged()) return;
		dao.list(req.query)
			.then(array => res.sendJson(array))
			.catch(err => res.error(500, err));
	});

	app.get('/agendamentoAva/podeAgendar', (req, res) => {
		if (req.notLogged()) return;
		dao.podeAgendar(req.query.idMatricula)
			.then(pode => res.sendJson(pode))
			.catch(err => res.error(500, err));
	});

	app.get('/agendamentoAva/get', (req, res) => {
		if (req.notLogged()) return;
		dao.get(req.query.id)
			.then(obj => res.sendJson(obj))
			.catch(err => res.error(500, err));
	});

};