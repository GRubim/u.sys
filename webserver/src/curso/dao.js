const connect = require('../../lib/db-conn');

const listOfertaCurso = (conn, idCurso) => new Promise((done, fail) => {
	conn.query(`
		SELECT
			oferta.id           AS id,
			oferta.idCurso      AS idCurso,
			oferta.idModalidade AS idModalidade,
			curso.nome          AS nome,
			modalidade.nome     AS modalidade
		FROM OfertaCurso oferta
		INNER JOIN Curso      curso      ON curso.id      = oferta.idCurso
		INNER JOIN Modalidade modalidade ON modalidade.id = oferta.idModalidade
		${ idCurso ? 'WHERE idCurso = ?' : '' }
		ORDER BY nome;
	`, idCurso ? [idCurso] : [])
		.then(array => done({conn, array}))
		.catch(error => fail({conn, error}));
});

const list = conn => conn.query('SELECT * FROM Curso;');

const listByIdAluno = (conn, idAluno) => conn.query(`
	SELECT DISTINCT
		curso.id,
		curso.nome,
		modal.nome AS modalidade,
		matri.id   AS idMatricula
	FROM Curso curso
	INNER JOIN OfertaCurso ofert ON curso.id = ofert.idCurso
	INNER JOIN Modalidade  modal ON modal.id = ofert.idModalidade
	INNER JOIN Matricula   matri ON ofert.id = matri.idOfertaCurso
	WHERE matri.idAluno = ? AND matri.estado = 1;
`, [idAluno]);

const isOfertado = (conn, idCurso, idModalidade) => new Promise((done, fail) => {
	conn.query(`
		SELECT TOP 1
			id
		FROM OfertaCurso
		WHERE
			idCurso = ? AND idModalidade = ?;
	`, [idCurso, idModalidade])
		.then(array => done({conn, res: array.length !== 0}))
		.catch(error => fail({conn, error}));
});

const addCurso = (conn, nome) => new Promise((done, fail) => {
	conn.query('INSERT INTO Curso (nome) VALUES (?);', [nome])
		.then(res => done({conn, id: res.insertId}))
		.catch(error => fail({conn, error}));
});

const addOfertaCurso = (conn, idCurso, idModalidade) => new Promise((done, fail) => {
	conn.query(`
		INSERT INTO OfertaCurso (
			idCurso, idModalidade
		) VALUES (?, ?);
	`, [idCurso, idModalidade])
		.then(res => done({conn, id: res.insertId}))
		.catch(error => fail({conn, error}));
});

const addOfertas = (conn, idCurso, modalidades) => new Promise((done, fail) => {
	let query = 'INSERT INTO OfertaCurso (idCurso, idModalidade) VALUES ';
	const values = [];
	modalidades.forEach((idModalidade, i) => {
		query += i ? ', (?, ?)' : '(?, ?)';
		values.push(idCurso, idModalidade);
	});
	conn.query(query + ';', values)
		.then(res => done({conn, res: res.affectedRows}))
		.catch(error => fail({conn, error}));
});

const get = (conn, id) => new Promise((done, fail) => {
	conn.query('SELECT * FROM Curso WHERE id = ?;', [id])
		.then(array => done(array[0] || null))
		.catch(fail);
});

const update = (conn, curso) => new Promise((done, fail) => {
	const {id, nome} = curso;
	conn.query(`
		UPDATE Curso
		SET nome = ?
		WHERE id = ?;
	`, [nome, id])
		.then(res => done(res.affectedRows !== 0))
		.catch(fail);
});

module.exports.addCurso = (nome, modalidades) => new Promise((done, fail) => {
	let idCurso;
	connect(__filename)
		.then(conn => addCurso(conn, nome))
		.then(({conn, id}) => {
			idCurso = id;
			if (idCurso) return addOfertas(conn, idCurso, modalidades);
			conn.end();
			fail('Curso não cadastrado');
		})
		.then(({conn, res}) => {
			conn.end();
			if (res < modalidades.length) {
				fail('Falha ao registrar modalidades');
			} else {
				done(idCurso);
			}
		})
		.catch(({conn, error}) => {
			if (conn) conn.end();
			fail(error);
		});
});

module.exports.listOfertaCurso = idCurso => new Promise((done, fail) => {
	connect(__filename)
		.then(conn => listOfertaCurso(conn, idCurso))
		.then(({conn, array}) => {
			conn.end();
			done(array);
		})
		.catch(({conn, error}) => {
			if (conn) conn.end();
			fail(error);
		});
});

module.exports.isOfertado = (idCurso, idModalidade) => new Promise((done, fail) => {
	connect(__filename)
		.then(conn => isOfertado(conn, idCurso, idModalidade))
		.then(({conn, res}) => {
			conn.end();
			done(res);
		})
		.catch(({conn, error}) => {
			if (conn) conn.end();
			fail(error);
		});
});

module.exports.list = () => new Promise((done, fail) => {
	let conn;
	connect(__filename)
		.then(res => list(conn = res))
		.then(array => {
			conn.end();
			done(array);
		})
		.catch(error => {
			if (conn) conn.end();
			fail(error);
		});
});

module.exports.get = id => new Promise((done, fail) => {
	let conn;
	connect(__filename)
		.then(res => get(conn = res, id))
		.then(curso => {
			conn.end();
			done(curso);
		})
		.catch(error => {
			if (conn) conn.end();
			fail(error);
		});
});

module.exports.update = curso => new Promise((done, fail) => {
	let conn;
	connect(__filename)
		.then(res => update(conn = res, curso))
		.then(updated => {
			conn.end();
			done(updated);
		})
		.catch(error => {
			if (conn) conn.end();
			fail(error);
		});
});

module.exports.listByIdAluno = idAluno => new Promise((done, fail) => {
	let conn = null;
	connect(__filename)
		.then(res => listByIdAluno(conn = res, idAluno))
		.then(array => {
			conn.end();
			done(array);
		})
		.catch(err => {
			if (conn !== null) conn.end();
			fail(err);
		});
});