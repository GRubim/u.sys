const dao = require('./dao');

module.exports = app => {
	
	app.get('/listOfertaCurso', (req, res) => {
		if (req.notLogged()) return;
		const {idCurso} = req.query;
		dao.listOfertaCurso(idCurso)
			.then(array => res.sendJson(array))
			.catch(err => res.error(500, err));
	});

	app.get('/curso/list', (req, res) => {
		if (req.notLogged()) return;
		dao.list()
			.then(array => res.sendJson(array))
			.catch(err => res.error(500, err));
	});
	
	app.post('/addCurso', (req, res) => {
		if (req.notLogged()) return;
		const {nome, modalidades} = req.body;
		if (!nome || !modalidades) return res.error(500, 'Bad arguments');
		let array = JSON.parse(`[${ modalidades }]`);
		dao.addCurso(nome, array)
			.then(id => res.sendJson(id))
			.catch(err => res.error(500, err));
	});

	app.get('/curso/get', (req, res) => {
		if (req.notLogged()) return;
		dao.get(req.query.id)
			.then(array => res.sendJson(array))
			.catch(err => res.error(500, err));
	});

	app.post('/curso/update', (req, res) => {
		if (req.notLogged()) return;
		const {id, nome} = req.body;
		dao.update({id, nome})
			.then(array => res.sendJson(array))
			.catch(err => res.error(500, err));
	});

	app.get('/curso/listByIdAluno', (req, res) => {
		if (req.notLogged()) return;
		dao.listByIdAluno(req.query.idAluno)
			.then(array => res.sendJson(array))
			.catch(err => res.err(500, err));
	});

};
