const dao = require('./dao');
module.exports = app => {
	app.post('/prova/add', (req, res) => {
		if (req.notLogged()) return;
		const idUsuario = req.session.user.id;
		dao.add(req.body, idUsuario)
			.then(id => res.sendJson(id))
			.catch(err => res.error(500, err));
	});
	app.get('/prova/list', (req, res) => {
		if (req.notLogged()) return;
		let {d0, t0, d1, t1} = req.query;
		if (!t0) t0 = '00:00:00';
		if (!t1) t1 = '23:59:59';
		dao.list(d0, t0, d1, t1)
			.then(array => res.sendJson(array))
			.catch(err => res.error(500, err));
	});
	app.get('/prova/get', (req, res) => {
		if (req.notLogged()) return;
		dao.get(req.query.id)
			.then(obj => res.sendJson(obj))
			.catch(err => res.error(500, err));
	});
	app.post('/prova/remove', (req, res) => {
		if (req.notLogged()) return;
		dao.remove(req.body.id)
			.then(obj => res.sendJson(obj))
			.catch(err => res.error(500, err));
	});
	app.post('/prova/update', (req, res) => {
		if (req.notLogged()) return;
		dao.update(req.body)
			.then(obj => res.sendJson(obj))
			.catch(err => res.error(500, err));
	});
};