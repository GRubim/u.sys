const connect = require('../../lib/db-conn');

const add = (conn, prova, idUsuario) => new Promise((done, fail) => {
	const {
		semestre,
		disciplina,
		data,
		horario,
		idMatricula
	} = prova;
	const query = `
		INSERT INTO AgendamentoProva (
			idMatricula,
			idUsuario,
			semestre,
			disciplina,
			data,
			horario
		) VALUES (?, ?, ?, ?, ?, ${
			horario ? '?' : 'NULL'
		});
	`;
	const values = [
		idMatricula,
		idUsuario,
		semestre,
		disciplina,
		data
	];
	if (horario) {
		values.push(horario);
	}
	conn.query(query, values)
		.then(res => done(res.insertId))
		.catch(fail);
});

const list = (conn, d0, t0, d1, t1) => conn.query(`
	SELECT
		prova.id,
		prova.semestre,
		prova.disciplina,
		prova.data,
		prova.horario,
		curso.nome AS nomeCurso,
		aluno.nome AS nomeAluno
	FROM
		AgendamentoProva prova
	INNER JOIN Matricula   matri ON matri.id = prova.idMatricula
	INNER JOIN OfertaCurso ofert ON ofert.id = matri.idOfertaCurso
	INNER JOIN Curso       curso ON curso.id = ofert.idCurso
	INNER JOIN Aluno       aluno ON aluno.id = matri.idAluno
	WHERE
		NOT (
			(prova.horario IS NOT NULL) AND (
				   (prova.data < ?)
				OR (prova.data > ?)
				OR (prova.data = ?) AND (prova.horario < ?)
				OR (prova.data = ?) AND (prova.horario > ?)
			) OR (prova.horario IS NULL) AND (
				prova.data < ? OR prova.data > ?
			)
		);
`, [d0, d1, d0, t0, d1, t1, d0, d1]);

const get = (conn, id) => new Promise((done, fail) => {
	conn.query(`
		SELECT
			prova.semestre,
			prova.disciplina,
			prova.data,
			prova.horario,
			aluno.nome AS nomeAluno,
			aluno.id   AS idAluno,
			matri.id   AS idMatricula
		FROM AgendamentoProva prova
		INNER JOIN Matricula matri ON matri.id = prova.idMatricula
		INNER JOIN Aluno aluno ON aluno.id = matri.idAluno
		WHERE prova.id = ?;
	`, [id])
		.then(res => done(res[0] || null))
		.catch(fail);
});

const update = (conn, prova) => new Promise((done, fail) => {
	const {
		id,
		semestre,
		disciplina,
		data,
		horario,
		idMatricula
	} = prova;
	const query = `
		UPDATE AgendamentoProva SET
			semestre = ?,
			disciplina = ?,
			data = ?,
			horario = ${ horario ? '?' : 'NULL' },
			idMatricula = ?
		WHERE id = ?;`;
	const values = [semestre, disciplina, data];
	if (horario) {
		values.push(horario);
	}
	values.push(idMatricula, id);
	conn.query(query, values)
		.then(res => done(res.affectedRows !== 0))
		.catch(fail);
});

const remove = (conn, id) => new Promise((done, fail) => {
	conn.query(`DELETE FROM AgendamentoProva WHERE id = ?;`, [id])
		.then(res => done(res.affectedRows !== 0))
		.catch(fail);
});

module.exports.add = (prova, idUsuario) => new Promise((done, fail) => {
	let conn = null;
	connect(__filename)
		.then(res => add(conn = res, prova, idUsuario))
		.then(id => {
			conn.end();
			done(idUsuario);
		})
		.catch(err => {
			if (conn !== null) conn.end();
			fail(err);
		});
});

module.exports.list = (d0, t0, d1, t1) => new Promise((done, fail) => {
	let conn = null;
	connect(__filename)
		.then(res => list(conn = res, d0, t0, d1, t1))
		.then(array => {
			conn.end();
			done(array);
		})
		.catch(err => {
			if (conn !== null) conn.end();
			fail(err);
		});
});

module.exports.get = id => new Promise((done, fail) => {
	let conn = null;
	connect(__filename)
		.then(res => get(conn = res, id))
		.then(res => {
			conn.end();
			done(res);
		})
		.catch(err => {
			if (conn !== null) conn.end();
			fail(err);
		});
});

module.exports.update = prova => new Promise((done, fail) => {
	let conn = null;
	connect(__filename)
		.then(res => update(conn = res, prova))
		.then(res => {
			conn.end();
			done(res);
		})
		.catch(err => {
			if (conn !== null) conn.end();
			fail(err);
		});
});

module.exports.remove = idProva => new Promise((done, fail) => {
	let conn = null;
	connect(__filename)
		.then(res => remove(conn = res, idProva))
		.then(res => {
			conn.end();
			done(res);
		})
		.catch(err => {
			if (conn !== null) conn.end();
			fail(err);
		});
});