const daoAluno = require('./dao');

module.exports = app => {

	app.post('/aluno/add', (req, res) => {
		if (req.notLogged()) return;
		const {nome, telefone, email} = req.body;
		if (!nome || !telefone) return res.sendJson(false);
		daoAluno.add({nome, telefone, email})
			.then(id => res.sendJson(id))
			.catch(err => res.error(500, err));
	});

	app.get('/aluno/list', (req, res) => {
		if (req.notLogged()) return;
		daoAluno.list()
			.then(array => res.sendJson(array))
			.catch(err => res.error(500, err));
	});

	app.get('/aluno/get', (req, res) => {
		if (req.notLogged()) return;
		daoAluno.get(req.query.id)
			.then(aluno => res.sendJson(aluno))
			.catch(err => res.error(500, err));
	});

	app.post('/aluno/update', (req, res) => {
		if (req.notLogged()) return;
		let {id, nome, telefone, email} = req.body;
		if (!nome || !telefone) {
			return res.error(500);
		}
		daoAluno.update({id, nome, telefone, email})
			.then(updated => res.sendJson(updated))
			.catch(err => res.error(500, err));
	});

	app.post('/aluno/remove', (req, res) => {
		if (req.notLogged()) return;
		daoAluno.remove(req.body.id)
			.then(aluno => res.sendJson(true))
			.catch(err => res.error(500, err));
	});

};
