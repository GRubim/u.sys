const connect = require('../../lib/db-conn');

const add = (conn, aluno) => new Promise((done, fail) => {
	const {nome, telefone, email} = aluno;
	conn.query(`
		INSERT INTO Aluno (
			nome, telefone, email
		) VALUES (?, ?, ?);
	`, [nome, telefone, email])
		.then(res => done(res.insertId))
		.catch(fail);
});

const list = conn => conn.query('SELECT * FROM Aluno ORDER BY nome;');

const get = (conn, id) => new Promise((done, fail) => {
	conn.query('SELECT * FROM Aluno WHERE id = ?;', [id])
		.then(res => done(res[0] || null))
		.catch(fail);
});

const update = (conn, aluno) => new Promise((done, fail) => {
	const {id, nome, email, telefone} = aluno;
	conn.query(`
		UPDATE Aluno
		SET
			nome = ?, email = ?, telefone = ?
		WHERE id = ?;
	`, [nome, email, telefone, id])
		.then(res => done(res.affectedRows !== 0))
		.catch(fail);
});

const removeMatriculas = (conn, id) => conn.query('DELETE FROM Matricula WHERE idAluno = ?;', [id]);
const removeComentarios = (conn, id) => conn.query('DELETE FROM Comentario WHERE idAluno = ?;', [id]);
const remove = (conn, id) => new Promise((done, fail) => {
	conn.query('DELETE FROM Aluno WHERE id = ?;', [id])
		.then(res => done(res.affectedRows !== 0))
		.catch(fail);
});

module.exports.add = aluno => new Promise((done, fail) => {
	let conn = null;
	connect(__filename)
		.then(res => add(conn = res, aluno))
		.then(id => {
			conn.end();
			done(id);
		})
		.catch(error => {
			if (conn) conn.end();
			fail(error);
		});
});

module.exports.list = search => new Promise((done, fail) => {
	let conn = null;
	connect(__filename)
		.then(res => list(conn = res))
		.then(array => {
			conn.end();
			done(array);
		})
		.catch(error => {
			if (conn) conn.end();
			fail(error);
		})
});

module.exports.get = id => new Promise((done, fail) => {
	let conn = null;
	connect(__filename)
		.then(res => get(conn = res, id))
		.then(aluno => {
			conn.end();
			done(aluno);
		})
		.catch(error => {
			if (conn) conn.end();
			fail(error);
		})
});

module.exports.update = aluno => new Promise((done, fail) => {
	let conn = null;
	connect(__filename)
		.then(res => update(conn = res, aluno))
		.then(updated => {
			conn.end();
			done(updated);
		})
		.catch(error => {
			if (conn) conn.end();
			fail(error);
		});
});

module.exports.remove = id => new Promise((done, fail) => {
	let conn = null;
	connect(__filename)
		.then(res => removeMatriculas(conn = res, id))
		.then(() => removeComentarios(conn, id))
		.then(() => remove(conn, id))
		.then(removed => {
			conn.end();
			if (removed) {
				done();
			} else {
				fail('Falha ao remover aluno');
			}
		})
		.catch(error => {
			if (conn) conn.end();
			fail(error);
		});
});
