const connect = require('../../lib/db-conn');

const add = (conn, nome) => new Promise((done, fail) => {
	conn.query(`
		INSERT INTO SiteParceiro (nome) VALUES (?);
	`, [nome])
		.then(res => done({conn, id: res.insertId}))
		.catch(error => fail({conn, error}));
});

module.exports.add = nome => new Promise((done, fail) => {
	connect(__filename)
		.then(conn => add(conn, nome))
		.then(({conn, id}) => {
			conn.end();
			done(id);
		})
		.catch(({conn, code, error}) => {
			if (conn) conn.end();
			fail({code, error});
		});
});

module.exports.list = search => new Promise((done, fail) => {
	connect(__filename)
		.then(conn => {
			let query = `SELECT * FROM SiteParceiro ORDER BY nome;`;
			conn.query(query, [])
				.then(res => {
					conn.end();
					done(res);
				})
				.catch(error => {
					conn.end();
					fail({code: 'DB_ERROR', error});
				});
		})
		.catch(({conn, code, error}) => {
			if (conn) conn.end();
			fail({code, error});
		});
});
