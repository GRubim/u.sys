const daoSiteParceiro = require('./dao');
module.exports = app => {
	app.get('/listSiteParceiro', (req, res) => {
		if (req.notLogged()) return;
		daoSiteParceiro.list()
			.then(array => res.sendJson(array))
			.catch(err => res.error(500, err));
	});
	app.post('/addSiteParceiro', (req, res) => {
		if (req.notLogged()) return;
		const nome = (req.body.nome || '').trim();
		if (!nome) return res.error(500, 'Bad arguments');
		daoSiteParceiro.add(nome)
			.then(id => res.sendJson(id))
			.catch(err => res.error(500, err));
	});
};
