const hashPwd = require('../../lib/hash-pwd');
const daoUsuario = require('./dao');

module.exports = app => {
	
	app.get('/current_user', (req, res) => {
		res.sendJson(req.session.user || false);
	});
	
	app.post('/login', (req, res) => {
		const {login, senha} = req.body;
		if (!login || !senha) return res.sendJson(false);
		daoUsuario.auth(login, senha)
			.then(obj => {
				if (obj.usuario) req.session.user = obj.usuario;
				res.sendJson(obj);
			})
			.catch(err => res.error(500, err));
	});
	
	app.post('/logout', (req, res) => {
		if (req.session.user) req.session.user = null;
		res.sendJson(true);
	});
	
	app.post('/usuario/add', (req, res) => {
		if (req.notLogged()) return;
		const {nome, login, senha} = req.body;
		if (!nome || !login) {
			return res.sendJson(false);
		}
		const {hash, salt} = hashPwd.hash(senha);
		daoUsuario.add({nome, login, hash, salt})
			.then(id => res.sendJson(id))
			.catch(err => res.error(500, err));
	});
	
	app.get('/usuario/list', (req, res) => {
		if (req.notLogged()) return;
		daoUsuario.list()
			.then(array => res.sendJson(array))
			.catch(err => res.error(500, err));
	});

};
