const connect = require('../../lib/db-conn');
const hashPwd = require('../../lib/hash-pwd');

const nomeExists = (conn, nome) => new Promise((done, fail) => {
	conn.query('SELECT id FROM Usuario WHERE nome = ?;', [nome])
		.then(res => done({conn, exists: res.length > 0}))
		.catch(error => fail({conn, error}));
});

const getByLogin = (conn, login) => new Promise((done, fail) => {
	conn.query('SELECT * FROM Usuario WHERE login = ?;', [login])
		.then(res => done({conn, usuario: res[0] || null}))
		.catch(error => fail({conn, error}));
});

const list = conn => new Promise((done, fail) => {
	conn.query('SELECT id, nome, login FROM Usuario ORDER BY nome')
		.then(array => done({conn, array}))
		.catch(error => fail({conn, error}));
});

const add = (conn, usuario) => new Promise((done, fail) => {
	const {nome, login, hash, salt} = usuario;
	conn.query(`
		INSERT INTO Usuario (
			nome, login, hash, salt
		) VALUES (?, ?, ?, ?);
	`, [nome, login, hash, salt])
		.then(res => done({conn, id: res.insertId}))
		.catch(error => fail({conn, error}));
});

module.exports.nomeExists = nome => new Promise((done, fail) => {
	connect(__filename)
		.then(conn => nomeExists(conn, nome))
		.then(({conn, exists}) => {
			conn.end();
			done(exists);
		})
		.catch(({conn, error}) => {
			if (conn) conn.end();
			fail(error);
		});
});

module.exports.loginExists = login => new Promise((done, fail) => {
	connect(__filename)
		.then(conn => getByLogin(conn, login))
		.then(({conn, usuario}) => {
			conn.end();
			done(usuario !== null);
		})
		.catch(({conn, error}) => {
			if (conn) conn.end();
			fail(error);
		});
});

module.exports.list = login => new Promise((done, fail) => {
	connect(__filename)
		.then(list)
		.then(({conn, array}) => {
			conn.end();
			done(array);
		})
		.catch(({conn, error}) => {
			if (conn) conn.end();
			fail(error);
		});
});

module.exports.add = usuario => new Promise((done, fail) => {
	const {nome, login, hash, salt} = usuario;
	connect(__filename)
		.then(conn => nomeExists(conn, nome))
		.then(({conn, exists}) => {
			if (!exists) return getByLogin(conn, login)
			conn.end();
			fail('Nome de usuário repetido');
		})
		.then(res => {
			if (!res.usuario) return add(res.conn, usuario);
			conn.end();
			fail('Login repetido');
		})
		.then(({conn, id}) => {
			conn.end();
			done(id);
		})
		.catch(({conn, error}) => {
			if (conn) conn.end();
			fail(error);
		});
});

module.exports.auth = (login, senha) => new Promise((done, fail) => {
	connect(__filename)
		.then(conn => getByLogin(conn, login))
		.then(({conn, usuario}) => {
			conn.end();
			let found = false;
			let authenticated = false;
			if (usuario) {
				found = true;
				const {hash, salt} = usuario;
				if (hashPwd.check(senha, hash, salt)) {
					authenticated = true;
				} else {
					usuario = null;
				}
			}
			done({found, authenticated, usuario});
		})
		.catch(({conn, error}) => {
			if (conn) conn.end();
			fail(error);
		});
});
