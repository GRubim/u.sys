const connect = require('../../lib/db-conn');
const {now} = require('../../lib/sql-datetime');

const add = (conn, comentario) => new Promise((done, fail) => {
	const {texto, idUsuario, idAgendamento} = comentario;
	conn.query(`
		INSERT INTO ComentarioAva (
			data,
			texto,
			idUsuario,
			idAgendamento
		) VALUES (?, ?, ?, ?);
	`, [now(), texto, idUsuario, idAgendamento])
		.then(res => done(res.insertId || null))
		.catch(fail);
});

const update = (conn, comentario, idUsuario) => new Promise((done, fail) => {
	const {id, texto} = comentario;
	conn.query(`
		UPDATE ComentarioAva
		SET texto = ?
		WHERE id = ? AND idUsuario = ?;
	`, [texto, id, idUsuario])
		.then(res => done(res.affectedRows !== 0))
		.catch(fail);
});

const list = (conn, idAgendamento) => conn.query(`
	SELECT
		cmm.id,
		cmm.data AS horario,
		cmm.texto,
		cmm.idUsuario,
		usr.nome AS usuario
	FROM ComentarioAva cmm
	INNER JOIN Usuario usr ON usr.id = cmm.idUsuario
	WHERE idAgendamento = ?;
`, [idAgendamento]);

const get = (conn, id) => new Promise((done, fail) => {
	conn.query(`
		SELECT
			texto
		FROM ComentarioAva com
		WHERE id = ?;
	`, [id])
		.then(array => done(array[0] || null))
		.catch(fail);
});

module.exports.add = comentario => new Promise((done, fail) => {
	let conn;
	connect(__filename)
		.then(res => add(conn = res, comentario))
		.then(id => {
			conn.end();
			done(id);
		})
		.catch(err => {
			if (conn) conn.end();
			fail(err);
		});
});

module.exports.update = (comentario, idUsuario) => new Promise((done, fail) => {
	let conn;
	connect(__filename)
		.then(res => update(conn = res, comentario, idUsuario))
		.then(updated => {
			conn.end();
			done(updated);
		})
		.catch(err => {
			if (conn) conn.end();
			fail(err);
		});
});

module.exports.list = idAgendamento => new Promise((done, fail) => {
	let conn;
	connect(__filename)
		.then(res => list(conn = res, idAgendamento))
		.then(array => {
			conn.end();
			done(array);
		})
		.catch(err => {
			if (conn) conn.end();
			fail(err);
		});
});

module.exports.get = id => new Promise((done, fail) => {
	let conn;
	connect(__filename)
		.then(res => get(conn = res, id))
		.then(res => {
			conn.end();
			done(res);
		})
		.catch(err => {
			if (conn) conn.end();
			fail(err);
		});
});