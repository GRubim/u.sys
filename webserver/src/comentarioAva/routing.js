const dao = require('./dao');

module.exports = app => {

	app.post('/comentarioAva/add', (req, res) => {
		const idUsuario = req.session.user.id;
		const comentario = {...req.body, idUsuario};
		dao.add(comentario)
			.then(id => res.sendJson(id))
			.catch(err => res.error(500, err));
	});

	app.post('/comentarioAva/update', (req, res) => {
		const comentario = {...req.body};
		const idUsuario = req.session.user.id;
		dao.update(comentario, idUsuario)
			.then(updated => res.sendJson(updated))
			.catch(err => res.error(500, err));
	});

	app.get('/comentarioAva/list', (req, res) => {
		dao.list(req.query.idAgendamento)
			.then(array => res.sendJson(array))
			.catch(err => res.error(500, err));
	});

	app.get('/comentarioAva/get', (req, res) => {
		dao.get(req.query.id)
			.then(comentario => res.sendJson(comentario))
			.catch(err => res.error(500, err));
	});

};
