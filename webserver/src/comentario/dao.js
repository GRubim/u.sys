const connect = require('../../lib/db-conn');
const sqlDateTime = require('../../lib/sql-datetime');

const add = (conn, comentario) => new Promise((done, fail) => {
	const {idAluno, idUsuario, texto} = comentario;
	conn.query(`
		INSERT INTO Comentario (
			idAluno, idUsuario, texto, horario
		) VALUES (?, ?, ?, ?);
	`, [idAluno, idUsuario, texto, sqlDateTime.now()])
		.then(res => done(res.insertId))
		.catch(error => fail(error));
});

const get = (conn, id) => new Promise((done, fail) => {
	conn.query(`
		SELECT
			texto
		FROM Comentario com
		WHERE id = ?;
	`, [id])
		.then(array => done(array[0] || null))
		.catch(fail);
});

const update = (conn, comentario) => new Promise((done, fail) => {
	const {id, texto} = comentario;
	conn.query(`
		UPDATE Comentario
		SET texto = ?
		WHERE id = ?;
	`, [texto, id])
		.then(res => done(res.affectedRows !== 0))
		.catch(fail);
});

const list = (conn, idAluno) => new Promise((done, fail) => {
	conn.query(`
		SELECT
			com.id AS id,
			com.idUsuario AS idUsuario,
			com.texto AS texto,
			com.horario AS horario,
			user.nome AS usuario
		FROM Comentario com
		INNER JOIN Usuario user ON user.id = com.idUsuario
		WHERE idAluno = ?
		ORDER BY com.id DESC;
	`, [idAluno])
		.then(done)
		.catch(fail);
});

module.exports.add = comentario => new Promise((done, fail) => {
	let conn;
	connect(__filename)
		.then(res => add(conn = res, comentario))
		.then(id => {
			conn.end();
			done(id);
		})
		.catch(error => {
			if (conn) conn.end();
			fail(error);
		});
});

module.exports.update = comentario => new Promise((done, fail) => {
	let conn;
	connect(__filename)
		.then(res => update(conn = res, comentario))
		.then(updated => {
			conn.end();
			done(updated);
		})
		.catch(error => {
			if (conn) conn.end();
			fail(error);
		});
});

module.exports.list = idAluno => new Promise((done, fail) => {
	let conn;
	connect(__filename)
		.then(res => list(conn = res, idAluno))
		.then(array => {
			conn.end();
			done(array);
		})
		.catch(error => {
			if (conn) conn.end();
			fail(error);
		});
});

module.exports.get = id => new Promise((done, fail) => {
	let conn;
	connect(__filename)
		.then(res => get(conn = res, id))
		.then(comentario => {
			conn.end();
			done(comentario);
		})
		.catch(error => {
			if (conn) conn.end();
			fail(error);
		});
});
