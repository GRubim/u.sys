const dao = require('./dao');

module.exports = app => {

	app.get('/comentario/list', (req, res) => {
		if (req.notLogged()) return;
		dao.list(req.query.idAluno)
			.then(array => res.sendJson(array))
			.catch(err => res.error(500, err));
	});

	app.get('/comentario/get', (req, res) => {
		if (req.notLogged()) return;
		dao.get(req.query.id)
			.then(comentario => res.sendJson(comentario))
			.catch(err => res.error(500, err));
	});

	app.post('/comentario/add', (req, res) => {
		if (req.notLogged()) return;
		const {idAluno, texto} = req.body;
		const idUsuario = req.session.user.id;
		dao.add({idAluno, idUsuario, texto})
			.then(id => res.sendJson(id))
			.catch(err => res.error(500, err));
	});

	app.post('/comentario/update', (req, res) => {
		if (req.notLogged()) return;
		const {id, texto} = req.body;
		dao.update({id, texto})
			.then(updated => res.sendJson(updated))
			.catch(err => res.error(500, err));
	});

};
