const http = require('http');
const dns = require('dns');
const os = require('os');
const exec = require('child_process').exec;
const checkIp = ip => {
	http.get({
		hostname: ip,
		port: 80,
		path: '/test'
	}, res => {
		res.on('data', data => {
			if (data.toString().indexOf('[[OK]]') !== -1) {
				exec('start http://' + ip, () => {
					process.exit(0);
				});
			}
		});
	}).on('error', () => {});
}
dns.lookup(os.hostname(), (err, add, fam) => {
	let array = add.split('.');
	for (let i=0; i<256; ++i) {
		array[3] = i;
		checkIp(array.join('.'));
	}
});
